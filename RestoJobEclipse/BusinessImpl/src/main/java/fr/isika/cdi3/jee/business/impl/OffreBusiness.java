package fr.isika.cdi3.jee.business.impl;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.IOffreBusiness;
import fr.isika.cdi3.jee.dataapi.IOffreDao;
import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoOffre;

@Remote(IOffreBusiness.class)
@Stateless
public class OffreBusiness implements IOffreBusiness{

@EJB
private IOffreDao proxyOffre;
	
	@Override
	public List<DtoOffre> GetAll(String recherche, String gastronomie, String bistronomie, String patisserie,
			String traiteur, String boulangerie, String cuisineRegionale, String grandService, String petitService,
			String fruitsMer, String cuisineMesure, String experienceCulinaire, String troisHuit,
			String cuisineThematique, String cuisineModerne, String CDI, String CDD, String vacation, String cuisine,
			String salle) {
		// TODO Auto-generated method stub
		return proxyOffre.GetAll(recherche,gastronomie,bistronomie,patisserie,traiteur,boulangerie,cuisineRegionale,
				grandService,petitService,fruitsMer, cuisineMesure, experienceCulinaire, troisHuit, cuisineThematique, cuisineModerne, 
				CDI, CDD, vacation, cuisine, salle);
	}

	@Override
	public DtoOffre GetById(int IdOffre) {
		return proxyOffre.GetById(IdOffre);
		
	}

	@Override
	public void createOffer(DtoOffre dtoOffre) {
		proxyOffre.createOffer(dtoOffre);
		
	}

	public Long nbOffresPostees(int idEntreprise) {
		return proxyOffre.nbOffresPostees(idEntreprise);
	}

	@Override
	public void bookmark(DtoOffre dtoOffreBookmark, DtoCoopteurProfil dtoCoopteurProfil) {
		proxyOffre.bookmark(dtoOffreBookmark, dtoCoopteurProfil);
		
	}

	@Override
	public List<DtoOffre> GetByEntreprise(int idEntreprise) {
		return proxyOffre.GetByEntreprise(idEntreprise);
	}
}

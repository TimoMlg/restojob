package fr.isika.cdi3.jee.business.impl;
import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import fr.isika.cdi3.jee.business.api.IPersonneBusiness;
import fr.isika.cdi3.jee.dataapi.IPersonneDao;
import fr.isika.cdi3.jee.dto.DtoPersonne;

@Remote(IPersonneBusiness.class)
@Stateless
public class PersonneBusiness implements  IPersonneBusiness{

	@EJB
	private IPersonneDao proxyPersonne;
	

	@Override
	public DtoPersonne SeConnecter(String mail, String motPasse) throws Exception
	{
		return proxyPersonne.getConnexion(mail, motPasse);
	}
}

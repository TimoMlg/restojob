package fr.isika.cdi3.jee.business.impl;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.IEntrepriseBusiness;
import fr.isika.cdi3.jee.dataapi.IEntrepriseDao;
import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoPersonne;

@Remote(IEntrepriseBusiness.class)
@Stateless
public class EntrepriseBusiness implements IEntrepriseBusiness {


	@EJB
	private IEntrepriseDao proxyBusinessEnt;

	public void creerUnCompteEntreprise(DtoEntreprise entDto, DtoPersonne persDto) {

		proxyBusinessEnt.creerCompteEntreprise(entDto, persDto);
	}
	 

	@Override
	public DtoEntreprise getById(int idEntreprise) {
		
		return proxyBusinessEnt.getById(idEntreprise);
	}
}

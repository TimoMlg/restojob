package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dto.DtoImage;
import fr.isika.cdi3.jee.business.api.IImageBusiness;
import fr.isika.cdi3.jee.dataapi.IImageDao;

@Remote(IImageBusiness.class)
@Stateless
public class ImageBusiness implements IImageBusiness {
  @EJB
  private IImageDao proxy;
  @Override
  public List<DtoImage> getByPoste(int idPoste) {
    return proxy.getByPoste(idPoste);
  }

  @Override
  public DtoImage getById(int id) {
    return proxy.getById(id);
  }

}

package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.ITypeContratBusiness;
import fr.isika.cdi3.jee.dataapi.ITypeContratDao;
import fr.isika.cdi3.jee.dto.DtoTypeContrat;

@Remote(ITypeContratBusiness.class)
@Stateless
public class TypeContratBusiness implements ITypeContratBusiness {
  @EJB
  private ITypeContratDao proxy;
  @Override
  public List<DtoTypeContrat> getAll() {    
    return proxy.getAll();
  }
  public DtoTypeContrat getById(int idTC) {
	  return proxy.getById(idTC);
			 
  }

}

package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.ISuiviCooptationBusiness;
import fr.isika.cdi3.jee.business.api.ISuiviRecommandationBusiness;
import fr.isika.cdi3.jee.dataapi.ISuiviRecommandationDAO;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

@Remote(ISuiviRecommandationBusiness.class)
@Stateless
public class SuiviRecommandationBusiness implements ISuiviRecommandationBusiness {

	@EJB
	private ISuiviRecommandationDAO proxySuiviRecommandation;
	@Override
	public List<DtoSuiviCooptation> getRecommandation(int idCoopteur) {
		return proxySuiviRecommandation.getRecommandation(idCoopteur);
	}

}

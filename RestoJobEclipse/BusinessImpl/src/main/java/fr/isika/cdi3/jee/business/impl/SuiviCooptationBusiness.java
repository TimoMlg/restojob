package fr.isika.cdi3.jee.business.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.ISuiviCooptationBusiness;
import fr.isika.cdi3.jee.dataapi.ICoopteurProfilDao;
import fr.isika.cdi3.jee.dataapi.IPersonneDao;
import fr.isika.cdi3.jee.dataapi.ISuiviCooptationDao;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

@Remote(ISuiviCooptationBusiness.class)
@Stateless
public class SuiviCooptationBusiness implements ISuiviCooptationBusiness {

  @EJB
  private ISuiviCooptationDao proxySuiviCooptation;

  @EJB
  private ICoopteurProfilDao proxyCoopteurProfil;
  @EJB
  private IPersonneDao proxyPersonne;

  @Override
  public String addSuiviCooptation(LocalDateTime dateCreation, int idCoopteur, int idEtatCooptation, int idOffre,
      String mail) {
    if (proxySuiviCooptation.countLimiteCooptation(idCoopteur, idOffre)) {
      return "Vous avez atteint votre limite de cooptation sur cette offre.";
    }
    if (proxyPersonne.isEnregistre(mail)) {
      int idProfil = proxyPersonne.getIdByMail(mail);
      if (proxyCoopteurProfil.isCoopteurProfil(idProfil) == false) {
        return "Cette adresse e-mail n'est pas associée à un compte de type particulier.";
      }
      if (proxyCoopteurProfil.checkVeutRecommandation(idProfil)) {
        if (proxySuiviCooptation.isDejaCoopte(idOffre, mail)) {
          return "Le profil associé à cette adresse e-mail à déjà été coopté sur cette offre.";
        }
        proxySuiviCooptation.addSuiviCooptation(dateCreation, idCoopteur, idEtatCooptation, idOffre, idProfil);
        return "Un suivi de cooptation à été engagé. L'utilisateur à été notifié.";
      }
      return "L'utilisateur concerné ne souhaite pas être recommandé";
    }
    return "Un e-mail automatisé à été envoyé à l'adresse renseignée.";
  }

  @Override
  public void updateEtatSuiviCooptationValid(DtoSuiviCooptation dtosc) {
    proxySuiviCooptation.updateEtatSuiviCooptationValid(dtosc);
  }

  @Override
  public void updateEtatSuiviCooptationRefus(DtoSuiviCooptation sc) {
    proxySuiviCooptation.updateEtatSuiviCooptationRefus(sc);

  }

  @Override
  public DtoSuiviCooptation updateEtatSuiviCooptationSelection(DtoSuiviCooptation sc) {
    return proxySuiviCooptation.updateEtatSuiviCooptationSelection(sc);

  }

  @Override
  public DtoSuiviCooptation updateEtatSuiviCooptationRecrutement(DtoSuiviCooptation sc) {
    return proxySuiviCooptation.updateEtatSuiviCooptationRecrutement(sc);

  }

  @Override
  public DtoSuiviCooptation updateEtatSuiviCooptationPeriodeEssai(DtoSuiviCooptation sc) {
    return proxySuiviCooptation.updateEtatSuiviCooptationPeriodeEssai(sc);

  }

  public Long nbProfilsRecrutes(int idEntreprise) {
    return proxySuiviCooptation.nbProfilsRecrutes(idEntreprise);
  }

  @Override
  public List<DtoSuiviCooptation> getByOffre(int idOffre) {
    return proxySuiviCooptation.getByOffre(idOffre);
  }

  public Long nbValidationRecrutementCoopteur(int idCoopteur) {
    return proxySuiviCooptation.nbValidationRecrutementCoopteur(idCoopteur);
  }

  @Override
  public List<DtoSuiviCooptation> entretiensByCoopteur(int idParticulier) {
    return proxySuiviCooptation.entretiensByCoopteur(idParticulier);
  }

  @Override
  public List<DtoSuiviCooptation> recrutementsByCoopteurs(int idParticulier) {
    return proxySuiviCooptation.recrutementsByCoopteur(idParticulier);
  }

  @Override
  public List<DtoSuiviCooptation> selectionsByCoopteurs(int idParticulier) {
    return proxySuiviCooptation.selectionsByCoopteur(idParticulier);
  }

  @Override
  public long getSuiviSelonEtat(int idPersonne, int idEtat) {
    return proxySuiviCooptation.getSuiviSelonEtat(idPersonne, idEtat);
  }

  @Override
  public DtoSuiviCooptation updateEtatSuiviCooptationEntretien(DtoSuiviCooptation dtosc) {
    return proxySuiviCooptation.updateEtatSuiviCooptationEntretien(dtosc);
  }

}

package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.IEntretienBusiness;
import fr.isika.cdi3.jee.dataapi.IEntretienDao;
import fr.isika.cdi3.jee.dto.DtoEntretien;

@Remote(IEntretienBusiness.class)
@Stateless
public class EntretienBusiness implements IEntretienBusiness {

	@EJB
	private IEntretienDao proxyEntretien;

	@Override
	public List<DtoEntretien> datesEntretien(int idSuiviCooptation) {
		return proxyEntretien.datesEntretien(idSuiviCooptation);
	}

}

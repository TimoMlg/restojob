package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.IPosteBusiness;
import fr.isika.cdi3.jee.dataapi.IPosteDao;
import fr.isika.cdi3.jee.dto.DtoPoste;

@Remote(IPosteBusiness.class)
@Stateless
public class PosteBusiness implements IPosteBusiness{

	@EJB
	private IPosteDao proxy;
	@Override
	public List<DtoPoste> getAll() {
		
		return proxy.getAll();
	}
	@Override
	public DtoPoste getById(int idPoste) {
		
		return proxy.getById(idPoste);
	}

}

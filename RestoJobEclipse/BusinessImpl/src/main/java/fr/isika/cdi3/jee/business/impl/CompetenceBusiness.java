package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.ICompetenceBusiness;
import fr.isika.cdi3.jee.dataapi.ICompetenceDao;
import fr.isika.cdi3.jee.dto.DtoCompetence;

@Remote(ICompetenceBusiness.class)
@Stateless
public class CompetenceBusiness implements ICompetenceBusiness {

  @EJB
  private ICompetenceDao proxy;

  @Override
  public List<DtoCompetence> getAll() {

    return proxy.getAll();
  }

  @Override
  public DtoCompetence getById(int idCompetence) {

    return proxy.getById(idCompetence);
  }

}

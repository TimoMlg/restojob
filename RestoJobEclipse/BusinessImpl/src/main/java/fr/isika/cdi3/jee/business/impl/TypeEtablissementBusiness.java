package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.ITypeEtablissementBusiness;
import fr.isika.cdi3.jee.dataapi.ITypeEtablissementDao;
import fr.isika.cdi3.jee.dto.DtoTypeEtablissement;

@Remote(ITypeEtablissementBusiness.class)
@Stateless
public class TypeEtablissementBusiness implements ITypeEtablissementBusiness {
  @EJB
  private ITypeEtablissementDao proxy;
  @Override
  public List<DtoTypeEtablissement> getAll() {
    return proxy.getAll();
  }

}

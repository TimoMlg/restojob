package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.ICiviliteBusiness;
import fr.isika.cdi3.jee.dataapi.ICiviliteDao;
import fr.isika.cdi3.jee.dto.DtoCivilite;

@Remote(ICiviliteBusiness.class)
@Stateless
public class CiviliteBusiness implements ICiviliteBusiness {
  @EJB
  private ICiviliteDao proxy;
  
  @Override
  public List<DtoCivilite> getAll() {
    return proxy.getAll();
  }

}

package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.ITempsTravailBusiness;
import fr.isika.cdi3.jee.dataapi.ITempsTravailDao;
import fr.isika.cdi3.jee.dto.DtoTempsTravail;

@Remote(ITempsTravailBusiness.class)
@Stateless
public class TempsTravailBusiness implements ITempsTravailBusiness
{
	@EJB
	private ITempsTravailDao proxy;
	@Override
	public List<DtoTempsTravail> getAll() {
		
		return proxy.getAll();
	}
	@Override
	public DtoTempsTravail getById(int idTT) {
		
		return proxy.getById(idTT);
	}

}

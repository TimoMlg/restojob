package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.ICoopteurProfilBusiness;
import fr.isika.cdi3.jee.dataapi.ICoopteurProfilDao;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoOffre;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

@Remote(ICoopteurProfilBusiness.class)
@Stateless
public class CoopteurProfilBusiness implements ICoopteurProfilBusiness {

	@EJB
	ICoopteurProfilDao proxyCoopteurProfil;

	@Override
	public void creerCompteParticulier(DtoCoopteurProfil particulier) {
		proxyCoopteurProfil.creerCompteParticulier(particulier);
	}

	@Override
	public DtoCoopteurProfil getById(int idDtoPersonne) {
		return proxyCoopteurProfil.getById(idDtoPersonne);
	}

	@Override
	public long recommandationsReussies(int idParticulier) {
		return proxyCoopteurProfil.recommandationsReussies(idParticulier);
	}

	@Override
	public long personnesRecommandees(int idParticulier) {
		return proxyCoopteurProfil.personnesRecommandees(idParticulier);
	}

	@Override
	public long cooptationsEnCours(int idParticulier) {
		return proxyCoopteurProfil.cooptationsEnCours(idParticulier);
	}

	@Override
	public List<DtoCoopteurProfil> profilsAssocies(int idParticulier) {
		return proxyCoopteurProfil.profilsAssocies(idParticulier);
	}

	@Override
	public List<DtoOffre> listeOffresFav(int idParticulier) {
		return proxyCoopteurProfil.listeOffresFav(idParticulier);
	}

	@Override
	public List<DtoOffre> listeRecommandationsReussies(int idParticulier) {
		return proxyCoopteurProfil.listeRecommandationsReussies(idParticulier);
	}
}

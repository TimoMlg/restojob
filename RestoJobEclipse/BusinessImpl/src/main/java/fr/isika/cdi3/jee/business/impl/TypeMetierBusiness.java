package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.ITypeMetierBusiness;
import fr.isika.cdi3.jee.dataapi.ITypeMetierDao;
import fr.isika.cdi3.jee.dto.DtoTypeMetier;

@Remote(ITypeMetierBusiness.class)
@Stateless
public class TypeMetierBusiness implements ITypeMetierBusiness {
  @EJB
  private ITypeMetierDao proxy;
  @Override
  public List<DtoTypeMetier> getAll() {
    return proxy.getAll();
  }

}

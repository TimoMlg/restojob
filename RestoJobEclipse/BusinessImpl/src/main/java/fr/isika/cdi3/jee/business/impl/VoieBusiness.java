package fr.isika.cdi3.jee.business.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.business.api.IVoieBusiness;
import fr.isika.cdi3.jee.dataapi.IVoieDao;
import fr.isika.cdi3.jee.dto.DtoVoie;

@Remote(IVoieBusiness.class)
@Stateless
public class VoieBusiness implements IVoieBusiness {
  @EJB
  private IVoieDao proxy;
  @Override
  public List<DtoVoie> getAll() {
    return proxy.getall();
  }

}

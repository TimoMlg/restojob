package fr.isika.cdi3.jee.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import fr.isika.cdi3.jee.business.api.ISuiviCooptationBusiness;
import fr.isika.cdi3.jee.business.api.ISuiviRecommandationBusiness;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

@ManagedBean
@SessionScoped
public class ManagedBeanSuiviRecommandation {

	@EJB
	private ISuiviRecommandationBusiness proxySuiviRecommandation;
	private List<DtoSuiviCooptation> recommandations;
	private int nombreRecommandations;
	
	@ManagedProperty(value="#{managedBeanPersonne}")
	private ManagedBeanPersonne mbeanP;
	
	public ManagedBeanPersonne getMbeanP() {
		return mbeanP;
	}
	public void setMbeanP(ManagedBeanPersonne mbeanP) {
		this.mbeanP = mbeanP;
	}
	@PostConstruct
	public void initPage()
	{
		if(mbeanP.getDtoPersonne() != null && mbeanP.getDtoPersonne().getClass() == DtoCoopteurProfil.class) {
		this.getRecommandation(mbeanP.getDtoPersonne().getId());
		this.getNombreRecommandations();
		}
	}
	public void getRecommandation(int idCoopteur)
	{
		recommandations = proxySuiviRecommandation.getRecommandation(idCoopteur);
		nombreRecommandations = recommandations.size();
	}
	
	
	/**
	 * Getters, setters
	 * @return
	 */
	
	
	
	
	public int getNombreRecommandations() {
		return nombreRecommandations;
	}
	public void setNombreRecommandations(int nombreRecommandations) {
		this.nombreRecommandations = nombreRecommandations;
	}
	
	public ISuiviRecommandationBusiness getProxySuiviRecommandation() {
		return proxySuiviRecommandation;
	}
	public void setProxySuiviRecommandation(ISuiviRecommandationBusiness proxySuiviRecommandation) {
		this.proxySuiviRecommandation = proxySuiviRecommandation;
	}
	public List<DtoSuiviCooptation> getRecommandations() {
		return recommandations;
	}
	public void setRecommandations(List<DtoSuiviCooptation> recommandations) {
		this.recommandations = recommandations;
	}
	
}

package fr.isika.cdi3.jee.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import fr.isika.cdi3.jee.business.api.ICompetenceBusiness;
import fr.isika.cdi3.jee.business.api.IOffreBusiness;
import fr.isika.cdi3.jee.business.api.ISuiviCooptationBusiness;
import fr.isika.cdi3.jee.dto.DtoCompetence;

@ManagedBean
@ViewScoped
public class ManagedBeanStatistique {

  @EJB
  private IOffreBusiness proxyBusinessOffre;
  @EJB
  private ISuiviCooptationBusiness proxySuiviCooptation;
  @EJB
  private ICompetenceBusiness proxyCompeteneces;

  private long nbOffre;
  private long nbProfilsRecrutes;
  private long selections;
  private long entretiens;
  private long recrutements;
  private String pourcentageRecrutement;
  private List<DtoCompetence> competences = new ArrayList<>();

  // pour stats
  private Map<Integer, Integer> competencesNombre = new HashMap<>();

  @ManagedProperty(value = "#{managedBeanPersonne}")
  private ManagedBeanPersonne mbp;

  @PostConstruct
  public void init() {
    if (mbp.getDtoent() == null) {
      try {
        FacesContext.getCurrentInstance().getExternalContext().redirect("Accueil.xhtml?faces-redirect=true");
      } catch (IOException e) {
        e.printStackTrace();
      }
    } else {
      long recrutement = nbProfilsRecrutes(mbp.getDtoent().getId());
      long offres = nbOffresPostées(mbp.getDtoent().getId());
      float pourcent = 0;
      if (offres != 0) {
        pourcent = ((float) recrutement / offres) * 100;
        pourcent = Math.round(pourcent);
      }
      setNbOffre(offres);
      setNbProfilsRecrutes(recrutement);
      setPourcentageRecrutement(Float.toString(pourcent));
//      competences = proxyCompeteneces.getByEntreprise(mbp.getDtoent().getId());
//      System.out.println(competences);
//      for (DtoCompetence comp : getCompetences()) {
//        if (competencesNombre.containsKey(comp.getId())) {
//          competencesNombre.put(comp.getId(), (competencesNombre.get(comp.getId()) + 1));
//        } else {
//          competencesNombre.put(comp.getId(), 1);
//        }
//      }
//      for (Integer idCompt : competencesNombre.keySet()) {
//        System.out.println("id : " + idCompt + " : " + competencesNombre.get(idCompt));
//      }
    }
  }

  public Long nbOffresPostées(int idEntreprise) {
    return proxyBusinessOffre.nbOffresPostees(idEntreprise);
  }

  public Long nbProfilsRecrutes(int idEntreprise) {
    return proxySuiviCooptation.nbProfilsRecrutes(idEntreprise);
  }

  public Long nbValidationRecrutementCoopteur(int idCoopteur) {
    return proxySuiviCooptation.nbValidationRecrutementCoopteur(idCoopteur);
  }

  public Long nbSelection(int idPersonne) {
    // return proxySuiviCooptation.
    return null;
  }

  public long getNbOffre() {
    return nbOffre;
  }

  public void setNbOffre(long nbOffre) {
    this.nbOffre = nbOffre;
  }

  public long getNbProfilsRecrutes() {
    return nbProfilsRecrutes;
  }

  public void setNbProfilsRecrutes(long nbProfilsRecrutes) {
    this.nbProfilsRecrutes = nbProfilsRecrutes;
  }

  public String getPourcentageRecrutement() {
    return pourcentageRecrutement;
  }

  public void setPourcentageRecrutement(String pourcentageRecrutement) {
    this.pourcentageRecrutement = pourcentageRecrutement;
  }

  public List<DtoCompetence> getCompetences() {
    return competences;
  }

  public void setCompetences(List<DtoCompetence> competences) {
    this.competences = competences;
  }

  public long getSelections() {
    return selections;
  }

  public void setSelections(long selections) {
    this.selections = selections;
  }

  public long getEntretiens() {
    return entretiens;
  }

  public void setEntretiens(long entretiens) {
    this.entretiens = entretiens;
  }

  public long getRecrutements() {
    return recrutements;
  }

  public void setRecrutements(long recrutements) {
    this.recrutements = recrutements;
  }

  public IOffreBusiness getProxyBusinessOffre() {
    return proxyBusinessOffre;
  }

  public void setProxyBusinessOffre(IOffreBusiness proxyBusinessOffre) {
    this.proxyBusinessOffre = proxyBusinessOffre;
  }

  public ISuiviCooptationBusiness getProxySuiviCooptation() {
    return proxySuiviCooptation;
  }

  public void setProxySuiviCooptation(ISuiviCooptationBusiness proxySuiviCooptation) {
    this.proxySuiviCooptation = proxySuiviCooptation;
  }

  public Map<Integer, Integer> getCompetencesNombre() {
    return competencesNombre;
  }

  public void setCompetencesNombre(Map<Integer, Integer> competencesNombre) {
    this.competencesNombre = competencesNombre;
  }

  public ICompetenceBusiness getProxyCompeteneces() {
    return proxyCompeteneces;
  }

  public void setProxyCompeteneces(ICompetenceBusiness proxyCompeteneces) {
    this.proxyCompeteneces = proxyCompeteneces;
  }

  public ManagedBeanPersonne getMbp() {
    return mbp;
  }

  public void setMbp(ManagedBeanPersonne mbp) {
    this.mbp = mbp;
  }

}

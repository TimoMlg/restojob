package fr.isika.cdi3.jee.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import fr.isika.cdi3.jee.business.api.ICompetenceBusiness;
import fr.isika.cdi3.jee.business.api.ICoopteurProfilBusiness;
import fr.isika.cdi3.jee.business.api.IEntrepriseBusiness;
import fr.isika.cdi3.jee.business.api.IOffreBusiness;
import fr.isika.cdi3.jee.business.api.IPosteBusiness;
import fr.isika.cdi3.jee.business.api.ITempsTravailBusiness;
import fr.isika.cdi3.jee.business.api.ITypeContratBusiness;
import fr.isika.cdi3.jee.business.api.ITypeEtablissementBusiness;
import fr.isika.cdi3.jee.business.api.ITypeMetierBusiness;
import fr.isika.cdi3.jee.dto.DtoCompetence;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoEtatOffre;
import fr.isika.cdi3.jee.dto.DtoExperience;
import fr.isika.cdi3.jee.dto.DtoOffre;
import fr.isika.cdi3.jee.dto.DtoPoste;
import fr.isika.cdi3.jee.dto.DtoSalaire;
import fr.isika.cdi3.jee.dto.DtoTempsTravail;
import fr.isika.cdi3.jee.dto.DtoTypeContrat;
import fr.isika.cdi3.jee.dto.DtoTypeEtablissement;
import fr.isika.cdi3.jee.dto.DtoTypeMetier;

@ManagedBean
@SessionScoped
public class ManagedBeanOffre {

  private DtoOffre offreDto = new DtoOffre();
  private DtoOffre offreDtoCreation = new DtoOffre();

  @ManagedProperty(value = "#{managedBeanPersonne}")
  private ManagedBeanPersonne mbeanP;

  public ManagedBeanPersonne getMbeanP() {
    return mbeanP;
  }

  public void setMbeanP(ManagedBeanPersonne mbeanP) {
    this.mbeanP = mbeanP;
  }

  @EJB
  private IOffreBusiness proxyOffre;
  @EJB
  private ITypeMetierBusiness proxyTypeMetier;
  @EJB
  private ITypeContratBusiness proxyTypeContrat;
  @EJB
  private ITypeEtablissementBusiness proxyTE;
  @EJB
  private ITempsTravailBusiness proxyTempsTravail;
  @EJB
  private IPosteBusiness proxyPoste;
  @EJB
  private ICompetenceBusiness proxyCompetence;
  @EJB
  private IEntrepriseBusiness proxyEntreprise;
  @EJB
  private ICoopteurProfilBusiness proxyCoopteurProfil;

  private String recherche = "";
  private String gastronomie = "";
  private String bistronomie = "";
  private String patisserie = "";
  private String traiteur = "";
  private String boulangerie = "";
  private String grandService = "";
  private String petitService = "";
  private String fruitsMer = "";
  private String cuisineMesure = "";
  private String troisHuit = "";
  private String cuisineThematique = "";
  private String cuisineModerne = "";
  private String CDI = "";
  private String CDD = "";
  private String vacation = "";
  private String cuisine = "";
  private String salle = "";
  private String experienceCulinaire = "";
  private String cuisineRegionale = "";

  private String description;
  private String remuneration;
  private String titrePoste;
  private String datePubli;
  private String typeContrat;
  private String localisation;
  private String nomEntreprise;
  private String image;
  private String experience;
  private String prime;

  private boolean filled;

  public String getPrime() {
    return prime;
  }

  public void setPrime(String prime) {
    this.prime = prime;
  }

  public String getExperience() {
    return experience;
  }

  public void setExperience(String experience) {
    this.experience = experience;
  }

  private int nombreOffres;

  private int selectedPoste;
  private int selectedTC;
  private int selectedTT;
  private int selectedCom;

  @ManagedProperty(value = "#{managedBeanPersonne}")
  private ManagedBeanPersonne mbp;

  private int idOffre;

  public int getIdOffre() {
    return idOffre;
  }

  public void setIdOffre(int idOffre) {
    this.idOffre = idOffre;
  }

  public int getNombreOffres() {
    return nombreOffres;
  }

  public void setNombreOffres(int nombreOffres) {
    this.nombreOffres = nombreOffres;
  }

  private List<DtoOffre> listeOffre;

  private List<DtoCompetence> competences;

  private List<Integer> tmIds = new ArrayList<>();
  private List<Integer> tcIds = new ArrayList<>();
  private List<Integer> teIds = new ArrayList<>();

  private List<DtoTypeMetier> listeTypeMetier = new ArrayList<>();
  private List<DtoTypeContrat> listeTypeContrat = new ArrayList<>();
  private List<DtoTypeEtablissement> listeTE = new ArrayList<>();
  private List<DtoTypeContrat> listeTypeContratCreation = new ArrayList<>();
  private List<DtoTempsTravail> listeTempsTravail = new ArrayList<>();
  private List<DtoPoste> listePoste = new ArrayList<>();
  private List<DtoCompetence> listeCompetence = new ArrayList<>();

  public ManagedBeanPersonne getMbp() {
    return mbp;
  }

  public void setMbp(ManagedBeanPersonne mbp) {
    this.mbp = mbp;
  }

  public List<DtoCompetence> getListeCompetence() {
    return listeCompetence;
  }

  public void setListeCompetence(List<DtoCompetence> listeCompetence) {
    this.listeCompetence = listeCompetence;
  }

  public List<DtoPoste> getListePoste() {
    return listePoste;
  }

  public void setListePoste(List<DtoPoste> listePoste) {
    this.listePoste = listePoste;
  }

  public List<DtoTempsTravail> getListeTempsTravail() {
    return listeTempsTravail;
  }

  public void setListeTempsTravail(List<DtoTempsTravail> listeTempsTravail) {
    this.listeTempsTravail = listeTempsTravail;
  }

  public DtoOffre getOffreDtoCreation() {
    return offreDtoCreation;
  }

  public void setOffreDtoCreation(DtoOffre offreDtoCreation) {
    this.offreDtoCreation = offreDtoCreation;
  }

  public List<DtoTypeContrat> getListeTypeContratCreation() {
    return listeTypeContratCreation;
  }

  public void setListeTypeContratCreation(List<DtoTypeContrat> listeTypeContratCreation) {
    this.listeTypeContratCreation = listeTypeContratCreation;
  }

  public List<Integer> getTcIds() {
    return tcIds;
  }

  public void setTcIds(List<Integer> tcIds) {
    this.tcIds = tcIds;
  }

  public List<DtoTypeContrat> getListeTypeContrat() {
    return listeTypeContrat;
  }

  public void setListeTypeContrat(List<DtoTypeContrat> listeTypeContrat) {
    this.listeTypeContrat = listeTypeContrat;
  }

  public String getPageEspaceEntreprise() {
    return pageEspaceEntreprise;
  }

  public void setPageEspaceEntreprise(String pageEspaceEntreprise) {
    this.pageEspaceEntreprise = pageEspaceEntreprise;
  }

  public List<DtoOffre> getListeOffre() {
    return listeOffre;
  }

  public void setListeOffre(List<DtoOffre> listeOffre) {
    this.listeOffre = listeOffre;
  }

  public List<DtoTypeMetier> getListeTypeMetier() {
    return listeTypeMetier;
  }

  public void setListeTypeMetier(List<DtoTypeMetier> listeTypeMetier) {
    this.listeTypeMetier = listeTypeMetier;
  }

  public List<DtoCompetence> getCompetences() {
    return competences;
  }

  public void setCompetences(List<DtoCompetence> competences) {
    this.competences = competences;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getRemuneration() {
    return remuneration;
  }

  public void setRemuneration(String remuneration) {
    this.remuneration = remuneration;
  }

  public String getTitrePoste() {
    return titrePoste;
  }

  public void setTitrePoste(String titrePoste) {
    this.titrePoste = titrePoste;
  }

  public String getDatePubli() {
    return datePubli;
  }

  public void setDatePubli(String datePubli) {
    this.datePubli = datePubli;
  }

  public String getTypeContrat() {
    return typeContrat;
  }

  public void setTypeContrat(String typeContrat) {
    this.typeContrat = typeContrat;
  }

  public String getLocalisation() {
    return localisation;
  }

  public void setLocalisation(String localisation) {
    this.localisation = localisation;
  }

  public String getRecherche() {
    return recherche;
  }

  public void setRecherche(String recherche) {
    this.recherche = recherche;
  }

  public String getGastronomie() {
    return gastronomie;
  }

  public void setGastronomie(String gastronomie) {
    this.gastronomie = gastronomie;
  }

  public String getBistronomie() {
    return bistronomie;
  }

  public void setBistronomie(String bistronomie) {
    this.bistronomie = bistronomie;
  }

  public String getPatisserie() {
    return patisserie;
  }

  public void setPatisserie(String patisserie) {
    this.patisserie = patisserie;
  }

  public String getTraiteur() {
    return traiteur;
  }

  public void setTraiteur(String traiteur) {
    this.traiteur = traiteur;
  }

  public String getBoulangerie() {
    return boulangerie;
  }

  public void setBoulangerie(String boulangerie) {
    this.boulangerie = boulangerie;
  }

  public String getCuisineRegionale() {
    return cuisineRegionale;
  }

  public void setCuisineRegionale(String cuisineRegionale) {
    this.cuisineRegionale = cuisineRegionale;
  }

  public String getGrandService() {
    return grandService;
  }

  public void setGrandService(String grandService) {
    this.grandService = grandService;
  }

  public String getPetitService() {
    return petitService;
  }

  public void setPetitService(String petitService) {
    this.petitService = petitService;
  }

  public String getFruitsMer() {
    return fruitsMer;
  }

  public void setFruitsMer(String fruitsMer) {
    this.fruitsMer = fruitsMer;
  }

  public String getCuisineMesure() {
    return cuisineMesure;
  }

  public void setCuisineMesure(String cuisineMesure) {
    this.cuisineMesure = cuisineMesure;
  }

  public String getTroisHuit() {
    return troisHuit;
  }

  public void setTroisHuit(String troisHuit) {
    this.troisHuit = troisHuit;
  }

  public String getCuisineThematique() {
    return cuisineThematique;
  }

  public void setCuisineThematique(String cuisineThematique) {
    this.cuisineThematique = cuisineThematique;
  }

  public String getCuisineModerne() {
    return cuisineModerne;
  }

  public void setCuisineModerne(String cuisineModerne) {
    this.cuisineModerne = cuisineModerne;
  }

  public String getCDI() {
    return CDI;
  }

  public void setCDI(String cDI) {
    CDI = cDI;
  }

  public String getCDD() {
    return CDD;
  }

  public void setCDD(String cDD) {
    CDD = cDD;
  }

  public String getVacation() {
    return vacation;
  }

  public void setVacation(String vacation) {
    this.vacation = vacation;
  }

  public String getCuisine() {
    return cuisine;
  }

  public void setCuisine(String cuisine) {
    this.cuisine = cuisine;
  }

  public String getSalle() {
    return salle;
  }

  public void setSalle(String salle) {
    this.salle = salle;
  }

  public DtoOffre getOffreDto() {
    return offreDto;
  }

  public void setOffreDto(DtoOffre offreDto) {
    this.offreDto = offreDto;
  }

  public IOffreBusiness getProxyOffre() {
    return proxyOffre;
  }

  public void setProxyOffre(IOffreBusiness proxyOffre) {
    this.proxyOffre = proxyOffre;
  }

  public String getExperienceCulinaire() {
    return experienceCulinaire;
  }

  public void setExperienceCulinaire(String experienceCulinaire) {
    this.experienceCulinaire = experienceCulinaire;
  }

  public String getNomPage() {
    return nomPage;
  }

  public void setNomPage(String nomPage) {
    this.nomPage = nomPage;
  }

  public String getNomEntreprise() {
    return nomEntreprise;
  }

  public void setNomEntreprise(String nomEntreprise) {
    this.nomEntreprise = nomEntreprise;
  }

  public List<Integer> getTeIds() {
    return teIds;
  }

  public void setTeIds(List<Integer> teIds) {
    this.teIds = teIds;
  }

  public List<DtoTypeEtablissement> getListeTE() {
    return listeTE;
  }

  public void setListeTE(List<DtoTypeEtablissement> listeTE) {
    this.listeTE = listeTE;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public void rechercheMulti() {
    checkbox();
    listeOffre = proxyOffre.GetAll(recherche, gastronomie, bistronomie, patisserie, traiteur, boulangerie,
        cuisineRegionale, grandService, petitService, fruitsMer, cuisineMesure, experienceCulinaire, troisHuit,
        cuisineThematique, cuisineModerne, CDI, CDD, vacation, cuisine, salle);
  }

  String nomPage;

  public String GetById(int IdOffre) {
    offreDto = proxyOffre.GetById(IdOffre);
    description = offreDto.getDescription();
    remuneration = offreDto.getSalaire().getSalaireMin().toString() + "/"
        + offreDto.getSalaire().getSalaireMax().toString() + " €";
    titrePoste = offreDto.getPoste().getLibelle();
    datePubli = offreDto.getPublication().toString().substring(0, 10);
    typeContrat = offreDto.getTypeContrat().getLibelle();
    nomEntreprise = "Le poste à pourvoir est proposé par l'entreprise : " + offreDto.getEntreprise().getNom();
    localisation = offreDto.getEntreprise().getAdresseSiege().getNumero().toString() + " rue "
        + offreDto.getEntreprise().getAdresseSiege().getNomVoie() + " "
        + offreDto.getEntreprise().getAdresseSiege().getCodePostal().getLibelle() + " "
        + offreDto.getEntreprise().getAdresseSiege().getVille().getLibelle();
    competences = offreDto.getCompetences();
    image = offreDto.getImage().getUrl();
    experience = offreDto.getExperience().getAnneeMin().toString() + " an(s) requis.";
    int calculPrime = ((offreDto.getSalaire().getSalaireMax() + offreDto.getSalaire().getSalaireMin()) * 12) / 2;
    double result = 0.03 * calculPrime;
    prime = String.valueOf(result) + "€";

    return "Consultation_Offre.xhtml";
  }

  @PostConstruct
  public void initPage() {
    listeTypeMetier();
    listeTypeContrat();
    listeTypeEtablissement();
    rechercheMulti();
    listeTypeContratCreation();
    listeTempsTravail();
    listePoste();
    listeCompetence();
    mbp.getOffres();
    offreDtoCreation.setSalaire(new DtoSalaire());
    offreDtoCreation.setExperience(new DtoExperience());
    offreDtoCreation.setCompetences(new ArrayList<>());
    offreDtoCreation.setEtatOffre(new DtoEtatOffre(1));
    
    this.getNombreOffres();
  }

  public void listeTypeEtablissement() {
    listeTE = proxyTE.getAll();
  }

  public void listeTypeMetier() {
    listeTypeMetier = proxyTypeMetier.getAll();
  }

  public void listeTypeContrat() {
    listeTypeContrat = proxyTypeContrat.getAll();
  }

  public void listeTypeContratCreation() {
    listeTypeContratCreation = proxyTypeContrat.getAll();
  }

  public void listeTempsTravail() {
    listeTempsTravail = proxyTempsTravail.getAll();
  }

  public void listePoste() {
    listePoste = proxyPoste.getAll();
  }

  public void listeCompetence() {
    listeCompetence = proxyCompetence.getAll();
  }

  public void afficherOffre() {
    filled = true;
    offreDto = proxyOffre.GetById(idOffre);
    description = offreDto.getDescription();
    remuneration = offreDto.getSalaire().getSalaireMax().toString();
    titrePoste = offreDto.getPoste().getLibelle();
    datePubli = offreDto.getPublication().toString().substring(0, 10);
    typeContrat = offreDto.getTypeContrat().getLibelle();
    nomEntreprise = "Le poste à pourvoir est proposé par : " + offreDto.getEntreprise().getNom();
    localisation = offreDto.getEntreprise().getAdresseSiege().getVille().getLibelle();
    competences = offreDto.getCompetences();
    image = offreDto.getImage().getUrl();
  }

  public List<Integer> getTmIds() {
    return tmIds;
  }

  public void setTmIds(List<Integer> tmIds) {
    this.tmIds = tmIds;
  }

  public void trouverPoste() {
    offreDtoCreation.setPoste(proxyPoste.getById(selectedPoste));

  }

  public void trouverCompetence() {

    if (offreDtoCreation.getCompetences().size() < 5) {
      offreDtoCreation.getCompetences().add(proxyCompetence.getById(selectedCom));

    } else {
      System.out.println("vous pouvez choisir maximum  compétences, merci");
    }

  }

  public void trouverTempsTravail() {
    offreDtoCreation.setTempsTravail(proxyTempsTravail.getById(selectedTT));
  }

  public void trouverTypeContrat() {
    offreDtoCreation.setTypeContrat(proxyTypeContrat.getById(selectedTT));
  }

  private String pageEspaceEntreprise = "EspaceEntreprise.xhtml";

  public void createOffer() {
    trouverPoste();
    trouverTempsTravail();
    trouverTypeContrat();
    // offreDtoCreation.setEntreprise(proxyEntreprise.getById(mbp.getDtoPersonne().getId()));
    offreDtoCreation.setEntreprise(mbp.getDtoent());
    offreDtoCreation.setPublication(LocalDateTime.now());
    proxyOffre.createOffer(offreDtoCreation);
    offreDtoCreation = new DtoOffre();
    offreDtoCreation.setSalaire(new DtoSalaire());
    offreDtoCreation.setExperience(new DtoExperience());
    offreDtoCreation.setCompetences(new ArrayList<>());
    offreDtoCreation.setEtatOffre(new DtoEtatOffre(1));
    initPage();
    mbp.listeOffre();

  }

  public void checkbox() {
    salle = "";
    cuisine = "";
    for (Integer id : tmIds) {
      switch (id) {
      case 1:
        salle = "Métiers de la salle";
        break;
      case 2:
        cuisine = "Métiers de la cuisine";
        break;
      }
    }
    tmIds.clear();
    CDI = "";
    CDD = "";
    vacation = "";
    for (Integer id : tcIds) {
      switch (id) {
      case 1:
        CDI = "CDI";
        break;
      case 2:
        CDD = "CDD";
        break;
      case 3:
        vacation = "Vacation saisonnière";
        break;
      }
    }
    tcIds.clear();

    gastronomie = "";
    bistronomie = "";
    patisserie = "";
    traiteur = "";
    boulangerie = "";
    cuisineRegionale = "";
    grandService = "";
    petitService = "";
    fruitsMer = "";
    cuisineMesure = "";
    troisHuit = "";
    cuisineThematique = "";
    cuisineModerne = "";
    experienceCulinaire = "";
    for (Integer id : teIds) {
      switch (id) {
      case 1:
        gastronomie = "Gastronomie";
        break;
      case 2:
        bistronomie = "Bistronomie";
        break;
      case 3:
        patisserie = "Pâtisserie";
        break;
      case 4:
        traiteur = "Traiteur";
        break;
      case 5:
        boulangerie = "Boulangerie";
        break;
      case 6:
        cuisineRegionale = "Cuisine régionale";
        break;
      case 7:
        grandService = "Grand service";
        break;
      case 8:
        petitService = "Petit service";
        break;
      case 9:
        fruitsMer = "Fruits de mer et poissons";
        break;
      case 10:
        cuisineMesure = "Cuisine sur-mesure";
        break;
      case 11:
        experienceCulinaire = "Expérience culinaire";
        break;
      case 12:
        troisHuit = "Les 3 huit";
        break;
      case 13:
        cuisineThematique = "Cuisine thématique";
        break;
      case 14:
        cuisineModerne = "Cuisine moderne";
        break;
      }
    }
    teIds.clear();
  }

  public void setProxyTypeMetier(ITypeMetierBusiness proxyTypeMetier) {
    this.proxyTypeMetier = proxyTypeMetier;
  }

  public ITypeContratBusiness getProxyTypeContrat() {
    return proxyTypeContrat;
  }

  public void setProxyTypeContrat(ITypeContratBusiness proxyTypeContrat) {
    this.proxyTypeContrat = proxyTypeContrat;
  }

  public ITypeEtablissementBusiness getProxyTE() {
    return proxyTE;
  }

  public void setProxyTE(ITypeEtablissementBusiness proxyTE) {
    this.proxyTE = proxyTE;
  }

  public ITempsTravailBusiness getProxyTempsTravail() {
    return proxyTempsTravail;
  }

  public void setProxyTempsTravail(ITempsTravailBusiness proxyTempsTravail) {
    this.proxyTempsTravail = proxyTempsTravail;
  }

  public IPosteBusiness getProxyPoste() {
    return proxyPoste;
  }

  public void setProxyPoste(IPosteBusiness proxyPoste) {
    this.proxyPoste = proxyPoste;
  }

  public ICompetenceBusiness getProxyCompetence() {
    return proxyCompetence;
  }

  public void setProxyCompetence(ICompetenceBusiness proxyCompetence) {
    this.proxyCompetence = proxyCompetence;
  }

  public void bookMark(DtoOffre offre) {
    DtoCoopteurProfil dtoc = (DtoCoopteurProfil) mbeanP.getDtoPersonne();
    proxyOffre.bookmark(offre, dtoc);
  }

  public String getClassEtoile(DtoOffre offre) {
    try {
      List<DtoOffre> liste = proxyCoopteurProfil.listeOffresFav(mbeanP.getDtoPersonne().getId());
      for (DtoOffre dtoOffre : liste) {
        if (offre.getId() == dtoOffre.getId()) {
          return "favorite-button--on";
        }

      }
      return "";
    } catch (Exception e) {
      return "";
    }
  }

  public String getClassEtoile2(DtoOffre offre) {
    try {
      List<DtoOffre> liste = proxyCoopteurProfil.listeOffresFav(mbeanP.getDtoPersonne().getId());
      for (DtoOffre dtoOffre : liste) {
        if (offre.getId() == dtoOffre.getId()) {
          return "fas";
        }

      }
      return "far";
    } catch (Exception e) {
      return "far";
    }
  }

  public void setSelectedPoste(int selectedPoste) {
    this.selectedPoste = selectedPoste;
  }

  public int getSelectedTC() {
    return selectedTC;
  }

  public void setSelectedTC(int selectedTC) {
    this.selectedTC = selectedTC;
  }

  public int getSelectedTT() {
    return selectedTT;
  }

  public void setSelectedTT(int selectedTT) {
    this.selectedTT = selectedTT;
  }

  public int getSelectedCom() {
    return selectedCom;
  }

  public void setSelectedCom(int selectedCom) {
    this.selectedCom = selectedCom;
  }

  public boolean isFilled() {
    return filled;
  }

  public void setFilled(boolean filled) {
    this.filled = filled;
  }

  public IEntrepriseBusiness getProxyEntreprise() {
    return proxyEntreprise;
  }

  public void setProxyEntreprise(IEntrepriseBusiness proxyEntreprise) {
    this.proxyEntreprise = proxyEntreprise;
  }

  public ICoopteurProfilBusiness getProxyCoopteurProfil() {
    return proxyCoopteurProfil;
  }

  public void setProxyCoopteurProfil(ICoopteurProfilBusiness proxyCoopteurProfil) {
    this.proxyCoopteurProfil = proxyCoopteurProfil;
  }

  public ITypeMetierBusiness getProxyTypeMetier() {
    return proxyTypeMetier;
  }

  public int getSelectedPoste() {
    return selectedPoste;
  }

}
package fr.isika.cdi3.jee.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.isika.cdi3.jee.business.api.ISuiviCooptationBusiness;
import fr.isika.cdi3.jee.dto.DtoOffre;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

@ManagedBean(name = "managedBeanSuiviCooptation")
@ViewScoped
public class ManagedBeanSuiviCooptation {
  private Date dateChangementStatut;
  private int idCoopteur;
  private int idOffre;
  private int idProfil;
  private String mail;
  private String message;
  private DtoSuiviCooptation sc;
  private boolean filled;

  @EJB
  private ISuiviCooptationBusiness proxySuiviCooptation;

  @ManagedProperty(value = "#{managedBeanSuiviRecommandation}")
  private ManagedBeanSuiviRecommandation mbean;

  @ManagedProperty(value = "#{managedBeanPersonne}")
  private ManagedBeanPersonne mbeanP;

  private List<DtoOffre> offres = new ArrayList<>();

  @PostConstruct
  public void init() {

  }

  public void addSuiviCooptation(int idOffre, int idCoopteur) {
    // TODO Faire passer les paramètres du front pour l'appel de méthode
    message = proxySuiviCooptation.addSuiviCooptation(LocalDateTime.now(), idCoopteur, 1, idOffre, mail);
  }

  public void updateEtatSuiviCooptationValid(DtoSuiviCooptation dtosc) {
    proxySuiviCooptation.updateEtatSuiviCooptationValid(dtosc);
    mbean.initPage();
  }

  public void updateEtatSuiviCooptationRefus(DtoSuiviCooptation dtosc) {
    proxySuiviCooptation.updateEtatSuiviCooptationRefus(dtosc);
    mbean.initPage();
  }

  public void updateEtatSuiviCooptationSelection(DtoSuiviCooptation dtosc) {
    System.out.println(dtosc);
    DtoSuiviCooptation retour = proxySuiviCooptation.updateEtatSuiviCooptationSelection(dtosc);
    dtosc.getEtatCooptation().setLibelle(retour.getEtatCooptation().getLibelle());
  }

  public void updateEntretien(DtoSuiviCooptation sc) {
    sc = proxySuiviCooptation.updateEtatSuiviCooptationEntretien(sc);
  }

  public void updateEtatSuiviCooptationRecrutement(DtoSuiviCooptation dtosc) {
    dtosc = proxySuiviCooptation.updateEtatSuiviCooptationRecrutement(dtosc);
  }

  public void updateEtatSuiviCooptationPeriodeEssai(DtoSuiviCooptation dtosc) {
    dtosc = proxySuiviCooptation.updateEtatSuiviCooptationPeriodeEssai(dtosc);
  }

  public void afficherDetails(DtoSuiviCooptation suivi) {
    sc = suivi;
    filled = true;
  }

  public List<DtoSuiviCooptation> getByIdOffre(int idOffre) {
    return proxySuiviCooptation.getByOffre(idOffre);
  }

  public ISuiviCooptationBusiness getProxySuiviCooptation() {
    return proxySuiviCooptation;
  }

  public void setProxySuiviCooptation(ISuiviCooptationBusiness proxySuiviCooptation) {
    this.proxySuiviCooptation = proxySuiviCooptation;
  }

  public ManagedBeanSuiviRecommandation getMbean() {
    return mbean;
  }

  public void setMbean(ManagedBeanSuiviRecommandation mbean) {
    this.mbean = mbean;
  }

  public Date getDateChangementStatut() {
    return dateChangementStatut;
  }

  public void setDateChangementStatut(Date dateChangementStatut) {
    this.dateChangementStatut = dateChangementStatut;
  }

  public int getIdCoopteur() {
    return idCoopteur;
  }

  public void setIdCoopteur(int idCoopteur) {
    this.idCoopteur = idCoopteur;
  }

  public int getIdOffre() {
    return idOffre;
  }

  public void setIdOffre(int idOffre) {
    this.idOffre = idOffre;
  }

  public int getIdProfil() {
    return idProfil;
  }

  public void setIdProfil(int idProfil) {
    this.idProfil = idProfil;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public DtoSuiviCooptation getSc() {
    return sc;
  }

  public void setSc(DtoSuiviCooptation sc) {
    this.sc = sc;
  }

  public boolean getFilled() {
    return filled;
  }

  public void setFilled(boolean filled) {
    this.filled = filled;
  }

  public ManagedBeanPersonne getMbeanP() {
    return mbeanP;
  }

  public void setMbeanP(ManagedBeanPersonne mbeanP) {
    this.mbeanP = mbeanP;
  }

  public List<DtoOffre> getOffres() {
    offres = mbeanP.getOffres();
    for (DtoOffre dtoOffre : offres) {
      dtoOffre.setPersonnesRecommandees(getByIdOffre(dtoOffre.getId()));
    }
    return offres;
  }

  public void setOffres(List<DtoOffre> offres) {
    this.offres = offres;
  }

}

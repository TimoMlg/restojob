package fr.isika.cdi3.jee.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.isika.cdi3.jee.business.api.IEntrepriseBusiness;
import fr.isika.cdi3.jee.dto.DtoAdresse;
import fr.isika.cdi3.jee.dto.DtoCivilite;
import fr.isika.cdi3.jee.dto.DtoCodePostal;
import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoVille;
import fr.isika.cdi3.jee.dto.DtoVoie;

@ManagedBean
@SessionScoped
public class ManagedBeanEntreprise {

	private String pageAccueil="Accueil.xhtml";
	private DtoEntreprise ent = new DtoEntreprise();
	
	@PostConstruct
	public void init() {
		ent.setAdresseSiege(new DtoAdresse());
		ent.getAdresseSiege().setCodePostal(new DtoCodePostal());
		ent.getAdresseSiege().setVoie(new DtoVoie());
		ent.getAdresseSiege().setVille(new DtoVille());
		ent.setCivilite(new DtoCivilite());
		
	}
	
	@EJB
	private IEntrepriseBusiness proxyEntrepriseBusiness;
	
	
	public String creerCompteEntreprise() {
	
		proxyEntrepriseBusiness.creerUnCompteEntreprise(ent,null);
		return pageAccueil;
	}

	public DtoEntreprise getEnt() {
		return ent;
	}

	public void setEnt(DtoEntreprise ent) {
		this.ent = ent;
	}
	
}

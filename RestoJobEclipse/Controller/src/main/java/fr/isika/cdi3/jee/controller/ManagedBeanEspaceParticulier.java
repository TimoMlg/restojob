package fr.isika.cdi3.jee.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import fr.isika.cdi3.jee.business.api.ICoopteurProfilBusiness;
import fr.isika.cdi3.jee.business.api.IEntretienBusiness;
import fr.isika.cdi3.jee.business.api.ISuiviCooptationBusiness;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoEntretien;
import fr.isika.cdi3.jee.dto.DtoOffre;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

@ManagedBean
@ViewScoped
public class ManagedBeanEspaceParticulier {

	@ManagedProperty(value = "#{managedBeanPersonne}")
	private ManagedBeanPersonne mbp;
	private DtoCoopteurProfil dtoCoopteurProfil = new DtoCoopteurProfil();

	@EJB
	private ICoopteurProfilBusiness proxyCoopteurProfil;

	@EJB
	private ISuiviCooptationBusiness proxySuiviCooptation;

	@EJB
	private IEntretienBusiness proxyBusiness;

	@PostConstruct
	public void onPageLoad() {
		getCoopteurProfil(mbp.getDtoPersonne().getId());
	}

	public void getCoopteurProfil(int idParticulier) {
		dtoCoopteurProfil = proxyCoopteurProfil.getById(idParticulier);
	}

	public long recommandationsReussies(int idParticulier) {
		return proxyCoopteurProfil.recommandationsReussies(idParticulier);
	}

	public long personnesRecommandees(int idParticulier) {
		return proxyCoopteurProfil.personnesRecommandees(idParticulier);
	}

	public long cooptationsEnCours(int idParticulier) {
		return proxyCoopteurProfil.cooptationsEnCours(idParticulier);
	}

	public List<DtoSuiviCooptation> listeEntretiensByCoopteur(int idParticulier) {
		return proxySuiviCooptation.entretiensByCoopteur(idParticulier);
	}

	public List<DtoSuiviCooptation> listeRecrutementByCoopteur(int idParticulier) {
		return proxySuiviCooptation.recrutementsByCoopteurs(idParticulier);
	}

	public List<DtoSuiviCooptation> listeSelectionsByCoopteur(int idParticulier) {
		return proxySuiviCooptation.selectionsByCoopteurs(idParticulier);
	}

	public List<DtoEntretien> datesEntretiens(int idSuiviCooptation) {
		return proxyBusiness.datesEntretien(idSuiviCooptation);
	}

	public List<DtoCoopteurProfil> profilsAssocies(int idParticulier) {
		return proxyCoopteurProfil.profilsAssocies(idParticulier);
	}

	public List<DtoOffre> listeOffresFav(int idParticulier) {
		List<DtoOffre> liste = proxyCoopteurProfil.listeOffresFav(idParticulier);
		System.out.println(liste);
		return liste;
	}

	public String calculerSalaire(int idParticulier) {
		int salaire_min = 0, salaire_max = 0;
		double moyenne = 0;
		List<DtoOffre> recommandations = proxyCoopteurProfil
				.listeRecommandationsReussies(idParticulier);
		if (!recommandations.isEmpty()) {

			for (DtoOffre dtoOffre : recommandations) {
				salaire_min += dtoOffre.getSalaire().getSalaireMin() * 12;
				salaire_max += dtoOffre.getSalaire().getSalaireMax() * 12;
			}
			moyenne = ((((salaire_min + salaire_max) / 2) / recommandations.size()) * 0.03);
			return Double.toString(moyenne);
		}
		return "0";
	}

	public String formatDate(LocalDateTime date) {
		return date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
	}

	public DtoCoopteurProfil getDtoCoopteurProfil() {
		return dtoCoopteurProfil;
	}

	public void setDtoCoopteurProfil(DtoCoopteurProfil dtoCoopteurProfil) {
		this.dtoCoopteurProfil = dtoCoopteurProfil;
	}

	public ManagedBeanPersonne getMbp() {
		return mbp;
	}

	public void setMbp(ManagedBeanPersonne mbp) {
		this.mbp = mbp;
	}

}

package fr.isika.cdi3.jee.controller;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.isika.cdi3.jee.business.api.ICoopteurProfilBusiness;
import fr.isika.cdi3.jee.dto.DtoAdresse;
import fr.isika.cdi3.jee.dto.DtoCivilite;
import fr.isika.cdi3.jee.dto.DtoCodePostal;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoVille;
import fr.isika.cdi3.jee.dto.DtoVoie;

@ManagedBean
@SessionScoped
public class ManagedBeanCoopteurProfil {

	private String pageAccueil="Accueil.xhtml";
	private DtoCoopteurProfil particulier = new DtoCoopteurProfil();

	@PostConstruct
	public void init() {
		particulier.setAdresse(new DtoAdresse());
		particulier.setCivilite(new DtoCivilite());
		particulier.getAdresse().setVoie(new DtoVoie());
		particulier.getAdresse().setCodePostal(new DtoCodePostal());
		particulier.getAdresse().setVille(new DtoVille());
		
	}

	@EJB
	private ICoopteurProfilBusiness proxyCoopteurProfilBusiness;

	public String creerUnCompteParticulier() {
		proxyCoopteurProfilBusiness.creerCompteParticulier(particulier);
		return pageAccueil;
	}

	public DtoCoopteurProfil getParticulier() {
		return particulier;
	}

	public void setParticulier(DtoCoopteurProfil particulier) {
		this.particulier = particulier;
	}

}

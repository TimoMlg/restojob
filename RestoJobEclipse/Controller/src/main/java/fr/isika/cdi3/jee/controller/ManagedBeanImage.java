package fr.isika.cdi3.jee.controller;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fr.isika.cdi3.jee.business.api.IImageBusiness;
import fr.isika.cdi3.jee.dto.DtoImage;

@ManagedBean
@SessionScoped
public class ManagedBeanImage {
  @EJB
  private IImageBusiness proxy;
  
  private String imgBanEntreprise;

  public String getImgBanEntreprise() {
    return imgBanEntreprise;
  }

  public void setImgBanEntreprise(String imgBanEntreprise) {
    this.imgBanEntreprise = imgBanEntreprise;
  }
  
  @PostConstruct
  public void initialisation() {
    imgEntreprise();
  }
  
  public void imgEntreprise() {
    DtoImage img = proxy.getById(1);
    imgBanEntreprise = img.getUrl();
  }
}

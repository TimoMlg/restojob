package fr.isika.cdi3.jee.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.isika.cdi3.jee.business.api.ICiviliteBusiness;
import fr.isika.cdi3.jee.business.api.IOffreBusiness;
import fr.isika.cdi3.jee.business.api.IPersonneBusiness;
import fr.isika.cdi3.jee.business.api.IVoieBusiness;
import fr.isika.cdi3.jee.dto.DtoCivilite;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoOffre;
import fr.isika.cdi3.jee.dto.DtoPersonne;
import fr.isika.cdi3.jee.dto.DtoVoie;

@ManagedBean
@SessionScoped
public class ManagedBeanPersonne {

  private String message = "";
  private String mail;
  private String motPasse;
  private String pageEntreprise = "EspaceEntreprise.xhtml?faces-redirect=true";
  private String pageCoopteur = "EspaceParticulier.xhtml?faces-redirect=true";
  private String msgErreur = "";
  private DtoPersonne dtoPersonne;
  private DtoEntreprise dtoent;
  private List<DtoOffre> offres = new ArrayList<>();
  private Integer idCiv;
  private List<DtoCivilite> civilites;
  private Integer idVoie;
  private List<DtoVoie> voies;

  public List<DtoCivilite> getCivilites() {
    return civilites;
  }

  public void setCivilites(List<DtoCivilite> civilites) {
    this.civilites = civilites;
  }

  public Integer getIdCiv() {
    return idCiv;
  }

  public void setIdCiv(Integer idCiv) {
    this.idCiv = idCiv;
  }

  public String getMsgErreur() {
    return msgErreur;
  }

  public void setMsgErreur(String msgErreur) {
    this.msgErreur = msgErreur;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public String getMotPasse() {
    return motPasse;
  }

  public void setMotPasse(String motPasse) {
    this.motPasse = motPasse;
  }

  public List<DtoOffre> getOffres() {
    return offres;
  }

  public void setOffres(List<DtoOffre> offres) {
    this.offres = offres;
  }

  public DtoEntreprise getDtoent() {
    return dtoent;
  }

  public void setDtoent(DtoEntreprise dtoent) {
    this.dtoent = dtoent;
  }

  public void resetMessage() {
    this.message = "";
  }

  @EJB
  private IPersonneBusiness proxyBusiness;
  @EJB
  private IOffreBusiness proxyOffre;
  @EJB
  private ICiviliteBusiness proxyCiv;
  @EJB
  private IVoieBusiness proxyVoie;

  public String SeConnecter() {
    try {
      dtoPersonne = proxyBusiness.SeConnecter(mail, motPasse);
      if (dtoPersonne.getClass() == DtoEntreprise.class) {
        dtoent = (DtoEntreprise) dtoPersonne;
        listeOffre();
        listeCivilite();
        listeVoies();
        idCiv = dtoent.getCivilite().getId();
        idVoie = dtoent.getAdresseSiege().getVoie().getId();
      }
      return "Accueil.xhtml?faces-redirect=true";
    } catch (Exception e) {
      message = "Identifiant et/ou mot de passe incorrect(s) !";
      return "";
    }

  }

  public void deco() throws IOException {
    HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    session.invalidate();
    FacesContext.getCurrentInstance().getExternalContext().redirect("Accueil.xhtml");
  }

  public boolean fonctionnalitesCoopteur() {
    if (dtoPersonne == null || dtoPersonne.getClass() == DtoEntreprise.class) {
      return false;
    }
    return true;
  }

  public String nav() {
    if (dtoPersonne.getClass() == DtoCoopteurProfil.class) {
      return pageCoopteur;
    } else {

      return pageEntreprise;
    }

  }

  public void listeOffre() {
    offres = proxyOffre.GetByEntreprise(dtoPersonne.getId());
  }

  public void listeCivilite() {
    civilites = proxyCiv.getAll();
  }

  public void listeVoies() {
    voies = proxyVoie.getAll();
  }

  public DtoPersonne getDtoPersonne() {
    return dtoPersonne;
  }

  public void setDtoPersonne(DtoPersonne dtoPersonne) {
    this.dtoPersonne = dtoPersonne;
  }

  public String getPageEntreprise() {
    return pageEntreprise;
  }

  public void setPageEntreprise(String pageEntreprise) {
    this.pageEntreprise = pageEntreprise;
  }

  public String getPageCoopteur() {
    return pageCoopteur;
  }

  public void setPageCoopteur(String pageCoopteur) {
    this.pageCoopteur = pageCoopteur;
  }

  public IPersonneBusiness getProxyBusiness() {
    return proxyBusiness;
  }

  public void setProxyBusiness(IPersonneBusiness proxyBusiness) {
    this.proxyBusiness = proxyBusiness;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Integer getIdVoie() {
    return idVoie;
  }

  public void setIdVoie(Integer idVoie) {
    this.idVoie = idVoie;
  }

  public List<DtoVoie> getVoies() {
    return voies;
  }

  public void setVoies(List<DtoVoie> voies) {
    this.voies = voies;
  }

}

package fr.isika.cdi3.jee.entity;
import java.util.*;

import javax.interceptor.AroundInvoke;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="entreprise")
public class Entreprise extends Personne{

    /**
     * Default constructor
     */
    public Entreprise() {
    }

    /**
     * 
     */
    @Column(name="nom_entreprise", nullable=false, length=200)
    private String nomEntreprise;

    /**
     * 
     */
    @Column(name="numero_siret", nullable=false, length=200)
    private String numeroSiret;

    /**
     * 
     */
    //   
    @ManyToOne
    @JoinColumn(name="id_logo_entreprise", foreignKey=@ForeignKey(name="FK_ENTREPRISE_LOGO"))
    private Image logo;

    /**
     * 
     */
    @Column(name="description_entreprise", length=5000)
    private String description;

    /**
     * 
     */
    @Column(name="nombre_salaries")
    private Integer nombreSalaries;

    /**
     * 
     */
    @Id
    @Column(name="id_entreprise")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    /**
     * 
     */
    @OneToMany(mappedBy="entreprise")
    private List<Facture> factures;

    /**
     * 
     */
    @OneToMany(mappedBy="entreprise")
    private List<Abonnement> abonnements;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_compte_bancaire_entreprise", foreignKey=@ForeignKey(name="FK_ENTREPRISE_COMPTE_BANCAIRE"))
    private CompteBancaire compteBancaire;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_etat_compte_entreprise", columnDefinition = "int default 1", foreignKey=@ForeignKey(name="FK_ENTREPRISE_ETAT_COMPTE"))
    private EtatCompte etatCompte;

    /**
     *
     */
    @ManyToOne
    @JoinColumn(name="id_adresse_siege_entreprise", nullable=false , foreignKey=@ForeignKey(name="FK_ENTREPRISE_ADRESSE_SIEGE"))
    private Adresse adresseSiege;

    /**
     * 
     */
    @OneToMany(mappedBy="entreprise")
    private List<Adresse> adresseSite;

    /**
     * 
     */
    @ManyToMany
    @JoinTable(name="entreprise_tag", 
    joinColumns = @JoinColumn(name="id_entreprise_entreprise_tag"), 
    inverseJoinColumns=@JoinColumn(name="id_tag_entreprise_tag"))
    private List<Tag> tags;

    /**
     * 
     */
    @ManyToMany
    @JoinTable(name="entreprise_type_etablissement", 
    joinColumns = @JoinColumn(name="id_entreprise_entreprise_type_etablissement"),
    inverseJoinColumns = @JoinColumn (name = "id_type_etablissement_entreprise_type_etablissement"))
    private List<TypeEtablissement> typeEtablissements;
    
    /**
     * 
     */
    @OneToMany (mappedBy = "entreprise")
    private List<Offre> offres;

	public List<Offre> getOffres() {
      return offres;
    }

    public void setOffres(List<Offre> offres) {
      this.offres = offres;
    }

  public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nom) {
		this.nomEntreprise = nom;
	}

	public String getNumeroSiret() {
		return numeroSiret;
	}

	public void setNumeroSiret(String numeroSiret) {
		this.numeroSiret = numeroSiret;
	}

	public Image getLogo() {
		return logo;
	}

	public void setLogo(Image logo) {
		this.logo = logo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getNombreSalaries() {
		return nombreSalaries;
	}

	public void setNombreSalaries(Integer nombreSalaries) {
		this.nombreSalaries = nombreSalaries;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Facture> getFactures() {
		return factures;
	}

	public void setFactures(List<Facture> factures) {
		this.factures = factures;
	}

	public List<Abonnement> getAbonnements() {
		return abonnements;
	}

	public void setAbonnements(List<Abonnement> abonnements) {
		this.abonnements = abonnements;
	}

	public CompteBancaire getCompteBancaire() {
		return compteBancaire;
	}

	public void setCompteBancaire(CompteBancaire compteBancaire) {
		this.compteBancaire = compteBancaire;
	}

	public EtatCompte getEtatCompte() {
		return etatCompte;
	}

	public void setEtatCompte(EtatCompte etatCompte) {
		this.etatCompte = etatCompte;
	}

	public Adresse getAdresseSiege() {
		return adresseSiege;
	}

	public void setAdresseSiege(Adresse adresseSiege) {
		this.adresseSiege = adresseSiege;
	}

	public List<Adresse> getAdresseSite() {
		return adresseSite;
	}

	public void setAdresseSite(List<Adresse> adresseSite) {
		this.adresseSite = adresseSite;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<TypeEtablissement> getTypeEtablissements() {
		return typeEtablissements;
	}

	public void setTypeEtablissements(List<TypeEtablissement> typeEtablissements) {
		this.typeEtablissements = typeEtablissements;
	}

}
package fr.isika.cdi3.jee.entity;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="abonnement")
public class Abonnement {

    /**
     * Default constructor
     */
    public Abonnement() {
    }

    /**
     * 
     */
    @Column(name="type_abonnement", nullable=false, length=200)
     private String type;

    /**
     * 
     */
    @Column(name="numero_contrat", nullable=false, length=200)
    private String numeroContrat;

    /**
     * 
     */
    @Column(name="montant", nullable=false)
    private double montant;

    /**
     * 
     */
    @Column(name="date_souscription", nullable=false)
    private Date dateSouscription;

    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_abonnement")
    private Integer id;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_etat_abonnement_abonnement", nullable=false, foreignKey=@ForeignKey (name="FK_ABONNEMENT_ETAT_ABONNEMENT"))
    private EtatAbonnement etatAbonnement;
    
    /**
     * 
     */
    @ManyToOne
    @JoinColumn (name = "id_entreprise_abonnement", nullable = false,
        foreignKey = @ForeignKey (name = "FK_ABONNEMENT_ENTREPRISE"))
    private Entreprise entreprise;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumeroContrat() {
		return numeroContrat;
	}

	public void setNumeroContrat(String numeroContrat) {
		this.numeroContrat = numeroContrat;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public Date getDateSouscription() {
		return dateSouscription;
	}

	public void setDateSouscription(Date dateSouscription) {
		this.dateSouscription = dateSouscription;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EtatAbonnement getEtatAbonnement() {
		return etatAbonnement;
	}

	public void setEtatAbonnement(EtatAbonnement etatAbonnement) {
		this.etatAbonnement = etatAbonnement;
	}

  public Entreprise getEntreprise() {
    return entreprise;
  }

  public void setEntreprise(Entreprise entreprise) {
    this.entreprise = entreprise;
  }
    

}
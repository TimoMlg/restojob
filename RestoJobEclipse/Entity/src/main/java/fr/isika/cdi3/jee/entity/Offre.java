package fr.isika.cdi3.jee.entity;
import java.time.LocalDateTime;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="offre")
public class Offre {

  public Offre(String descritption, LocalDateTime publication, Date ouverturePoste, boolean estDansNewsletter, Integer id,
			TypeContrat typeContrat, TempsTravail tempsTravail, Poste poste, EtatOffre etatOffre, Salaire salaire,
			Experience experience, List<Competence> competences, List<SuiviCooptation> personnesRecommandees,
			Entreprise entreprise) {
		super();
		this.descritption = descritption;
		this.publication = publication;
		this.ouverturePoste = ouverturePoste;
		this.estDansNewsletter = estDansNewsletter;
		this.id = id;
		this.typeContrat = typeContrat;
		this.tempsTravail = tempsTravail;
		this.poste = poste;
		this.etatOffre = etatOffre;
		this.salaire = salaire;
		this.experience = experience;
		this.competences = competences;
		this.personnesRecommandees = personnesRecommandees;
		this.entreprise = entreprise;
	}

/**
   * Default constructor
   */
  public Offre() {
  }

  /**
   * 
   */
  @Column (name = "description_offre", nullable = false, length = 1000)
  private String descritption;

  /**
   * 
   */
  @Column
  private LocalDateTime publication;

  /**
   * 
   */
  @Column (name = "ouverture_poste", nullable = false)
  private Date ouverturePoste;

  /**
   * 
   */
  @Column (name = "est_dans_newsletter")
  private boolean estDansNewsletter;

  /**
   * 
   */
  @Id
  @GeneratedValue (strategy = GenerationType.IDENTITY)
  @Column (name = "id_offre")
  private Integer id;

  /**
   * 
   */
  @ManyToOne
  @JoinColumn (name = "id_type_contrat_offre", nullable = false,
  foreignKey = @ForeignKey (name = "FK_OFFRE_TYPE_CONTRAT"))
  private TypeContrat typeContrat;

  /**
   * 
   */
  @ManyToOne
  @JoinColumn (name = "id_temps_travail_offre", nullable = false,
  foreignKey = @ForeignKey(name = "FK_OFFRE_TEMPS_TRAVAIL"))    
  private TempsTravail tempsTravail;

  /**
   * 
   */
  @ManyToOne
  @JoinColumn (name = "id_poste_offre", nullable = false,
  foreignKey = @ForeignKey (name = "FK_OFFRE_POSTE"))
  private Poste poste;

  /**
   * 
   */
  @ManyToOne
  @JoinColumn (name = "id_etat_offre_offre",
  foreignKey = @ForeignKey (name = "FK_OFFRE_ETAT_OFFRE"))
  private EtatOffre etatOffre;

  /**
   * 
   */
  @ManyToOne
  @JoinColumn (name = "id_salaire_offre", nullable = false,
  foreignKey = @ForeignKey (name = "FK_OFFRE_SALAIRE"))
  private Salaire salaire;

  /**
   * 
   */
  @ManyToOne
  @JoinColumn (name = "id_experience_offre", nullable = false,
  foreignKey = @ForeignKey (name = "FK_OFFRE_EXPERIENCE"))
  private Experience experience;


  /**
   * 
   */
  @ManyToMany
  @JoinTable (name = "offre_competence",
  joinColumns = @JoinColumn(name = "id_offre_offre_competence"),
  foreignKey = @ForeignKey (name = "FK_OFFRE_COMPETENCE_OFFRE"),
  inverseJoinColumns = @JoinColumn(name = "id_competence_offre_competence"),
  inverseForeignKey = @ForeignKey (name = "FK_OFFRE_COMPETENCE_COMPETENCE"))
  private List<Competence> competences;

  /**
   *
   */
  @OneToMany(mappedBy = "offre")
  private List<SuiviCooptation> personnesRecommandees;
  
  /**
   * 
   */
  @ManyToOne
  @JoinColumn (name = "id_entreprise_offre", nullable = false,
  foreignKey = @ForeignKey (name = "FK_OFFRE_ENTREPRISE"))
  private Entreprise entreprise;
  
  public Entreprise getEntreprise() {
    return entreprise;
  }

  public void setEntreprise(Entreprise entreprise) {
    this.entreprise = entreprise;
  }

  public String getDescritption() {
    return descritption;
  }

  public void setDescritption(String descritption) {
    this.descritption = descritption;
  }

  public LocalDateTime getPublication() {
    return publication;
  }

  public void setPublication(LocalDateTime publication) {
    this.publication = publication;
  }

  public Date getOuverturePoste() {
    return ouverturePoste;
  }

  public void setOuverturePoste(Date ouverturePoste) {
    this.ouverturePoste = ouverturePoste;
  }

  public boolean isEstDansNewsletter() {
    return estDansNewsletter;
  }

  public void setEstDansNewsletter(boolean estDansNewsletter) {
    this.estDansNewsletter = estDansNewsletter;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public TypeContrat getTypeContrat() {
    return typeContrat;
  }

  public void setTypeContrat(TypeContrat typeContrat) {
    this.typeContrat = typeContrat;
  }

  public TempsTravail getTempsTravail() {
    return tempsTravail;
  }

  public void setTempsTravail(TempsTravail tempsTravail) {
    this.tempsTravail = tempsTravail;
  }

  public Poste getPoste() {
    return poste;
  }

  public void setPoste(Poste poste) {
    this.poste = poste;
  }

  public EtatOffre getEtatOffre() {
    return etatOffre;
  }

  public void setEtatOffre(EtatOffre etatOffre) {
    this.etatOffre = etatOffre;
  }

  public Salaire getSalaire() {
    return salaire;
  }

  public void setSalaire(Salaire salaire) {
    this.salaire = salaire;
  }

  public Experience getExperience() {
    return experience;
  }

  public void setExperience(Experience experience) {
    this.experience = experience;
  }

  public List<Competence> getCompetences() {
    return competences;
  }

  public void setCompetences(List<Competence> competences) {
    this.competences = competences;
  }

  public List<SuiviCooptation> getPersonnesRecommandees() {
    return personnesRecommandees;
  }

  public void setPersonnesRecommandees(List<SuiviCooptation> personnesRecommandees) {
    this.personnesRecommandees = personnesRecommandees;
  }


}
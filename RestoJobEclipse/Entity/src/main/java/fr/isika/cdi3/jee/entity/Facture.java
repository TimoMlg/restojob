package fr.isika.cdi3.jee.entity;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="facture")
public class Facture {

    /**
     * Default constructor
     */
    public Facture() {
    }

    /**
     * 
     */
    @Column (name = "numero_facture", nullable = false, length = 50)
    private String numero;

    /**
     * 
     */
    @Column (name = "creation_facture", nullable = false)
    private Date creation;

    /**
     * 
     */
    @Column (name = "libelle_facture", nullable = false, length = 50)
    private String libelle;

    /**
     * 
     */
    @Column (name = "delai_paiement", nullable = false)
    private Date delaiPaiement;

    /**
     * 
     */
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_facture")
    private Integer id;
    
    /**
     * 
     */
    @ManyToOne
    @JoinColumn (name = "id_entreprise_facture", nullable = false,
        foreignKey = @ForeignKey (name = "FK_FACTURE_ENTREPRISE"))
    private Entreprise entreprise;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Date getDelaiPaiement() {
		return delaiPaiement;
	}

	public void setDelaiPaiement(Date delaiPaiement) {
		this.delaiPaiement = delaiPaiement;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

  public Entreprise getEntreprise() {
    return entreprise;
  }

  public void setEntreprise(Entreprise entreprise) {
    this.entreprise = entreprise;
  }

}
package fr.isika.cdi3.jee.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="temps_travail")
public class TempsTravail {

    /**
     * Default constructor
     */
    public TempsTravail() {
    }

    /**
     * 
     */
    
    @Column (name = "libelle_Temps_Travail", nullable=false)
    private String libelle;

    /**
     * 
     */
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_Temps_travail")
    private Integer id;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
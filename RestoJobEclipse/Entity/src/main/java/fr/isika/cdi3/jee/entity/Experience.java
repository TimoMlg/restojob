package fr.isika.cdi3.jee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="experience")
public class Experience {

    /**
     * Default constructor
     */
    public Experience() {
    }

    /**
     * 
     */
    @Column (name = "annee_min", nullable = false, length = 50)
    private String anneeMin;

    /**
     * 
     */
    @Column (name = "annee_max", nullable = false, length = 50)
    private String anneeMax;

    /**
     * 
     */
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_experience")
    private Integer id;

	public String getAnneeMin() {
		return anneeMin;
	}

	public void setAnneeMin(String anneeMin) {
		this.anneeMin = anneeMin;
	}

	public String getAnneeMax() {
		return anneeMax;
	}

	public void setAnneeMax(String anneeMax) {
		this.anneeMax = anneeMax;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
package fr.isika.cdi3.jee.entity;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="civilite")
public class Civilite {

    /**
     * Default constructor
     */
    public Civilite() {
    }

    /**
     * 
     */
    @Column(name="libelle_civilite", nullable= false, length=200)
    private String libelle;

    /**
     * 
     */
    @Column(name="id_civilite")
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
package fr.isika.cdi3.jee.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name = "entretien")
public class Entretien {

  /**
   * Default constructor
   */
  public Entretien() {
  }

  public Entretien(LocalDateTime now, SuiviCooptation dtosc) {
    dateEntretien = now;
    suiviCooptation = dtosc;
  }

  /**
   * 
   */
  @Column(name = "date_entretien", nullable = false)
  private LocalDateTime dateEntretien;

  /**
   * 
   */
  @Id
  @Column(name = "id_entretien")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  /**
  *
  */
  @ManyToOne
  @JoinColumn(name = "id_suivi_cooptation_entretien", nullable = false, foreignKey = @ForeignKey(name = "FK_ENTREPRISE_SUIVI_COOPTATION"))
  private SuiviCooptation suiviCooptation;

  public LocalDateTime getDateEntretien() {
    return dateEntretien;
  }

  public void setDateEntretien(LocalDateTime dateEntretien) {
    this.dateEntretien = dateEntretien;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public SuiviCooptation getSuiviCooptation() {
    return suiviCooptation;
  }

  public void setSuiviCooptation(SuiviCooptation suiviCooptation) {
    this.suiviCooptation = suiviCooptation;
  }

}
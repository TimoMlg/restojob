package fr.isika.cdi3.jee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="niveau")
public class Niveau {

    /**
     * Default constructor
     */
    public Niveau() {
    }

    /**
     * 
     */
    @Column (nullable = false)
    private Integer etoiles;

    /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id_niveau")
    private Integer id;
    
    /**
     * 
     */
    @ManyToOne
    @JoinColumn (name = "id_coopteur_profil_niveau", nullable = false,
    foreignKey = @ForeignKey (name = "FK_NIVEAU_COOPTEUR_PROFIL"))
    private CoopteurProfil coopteurProfil;
    
    /**
     * 
     */
    @ManyToOne
    @JoinColumn (name = "id_competence_niveau", nullable = false,
    foreignKey = @ForeignKey (name = "FK_NIVEAU_COMPETENCE"))
    private Competence competence;

	public Integer getEtoiles() {
		return etoiles;
	}

	public void setEtoiles(Integer etoiles) {
		this.etoiles = etoiles;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

  public CoopteurProfil getCoopteurProfil() {
    return coopteurProfil;
  }

  public void setCoopteurProfil(CoopteurProfil coopteurProfil) {
    this.coopteurProfil = coopteurProfil;
  }

  public Competence getCompetence() {
    return competence;
  }

  public void setCompetence(Competence competence) {
    this.competence = competence;
  }

}
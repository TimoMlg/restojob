package fr.isika.cdi3.jee.entity;

import javax.persistence.ForeignKey;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="tag")
public class Tag {

    /**
     * Default constructor
     */
    public Tag() {
    }

    /**
     * 
     */
    @Column (name = "libelle_tag", nullable=false)
    private String libelle;

    /**
     * 
     */
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_tag")
    private Integer id;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn (name = "tag_parent",
    foreignKey = @ForeignKey(name = "FK_TAG_TAG_PARENT"))
    private Tag tagParent;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Tag getTagParent() {
		return tagParent;
	}

	public void setTagParent(Tag tagParent) {
		this.tagParent = tagParent;
	}

}
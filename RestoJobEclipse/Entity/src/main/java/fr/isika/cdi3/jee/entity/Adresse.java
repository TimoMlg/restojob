package fr.isika.cdi3.jee.entity;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="adresse")
public class Adresse {

    /**
     * Default constructor
     */
    public Adresse() {
    }

    /**
     * 
     */
    @Column(name="numero", nullable=false, length=200)
    private String numero;

    /**
     * 
     */
    @Column(name="nom_voie", nullable=false, length=200)
    private String nomVoie;

    /**
     * 
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id_adresse")
    private Integer id;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_voie_adresse", nullable=false, foreignKey=@ForeignKey(name= "FK_ADRESSE_VOIE"))
    private Voie voie;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_code_postal_adresse", nullable= false, foreignKey=@ForeignKey(name= "FK_ADRESSE_CODE_POSTALE"))
    private CodePostal codePostal;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_ville_adresse", nullable=false, foreignKey=@ForeignKey(name="FK_ADRESSE_VILLE"))
    private Ville ville;
    
    @ManyToOne
    @JoinColumn(name="id_entreprise_adresse", foreignKey=@ForeignKey(name="FK_ADRESSE_ENTREPRISE_ADRESSE"))
    private Entreprise entreprise;
    

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNomVoie() {
		return nomVoie;
	}

	public void setNomVoie(String nomVoie) {
		this.nomVoie = nomVoie;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Voie getVoie() {
		return voie;
	}

	public void setVoie(Voie voie) {
		this.voie = voie;
	}

	public CodePostal getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(CodePostal codePostal) {
		this.codePostal = codePostal;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}

	public Entreprise getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}

}
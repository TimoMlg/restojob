package fr.isika.cdi3.jee.entity;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="poste")
public class Poste {

    /**
     * Default constructor
     */
    public Poste() {
    }

    /**
     * 
     */
    @Column (name = "libelle_poste", nullable=false)
    private String libelle;

    /**
     * 
     */
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_poste")
    private Integer id;

    /**
     * 
     */
  
    @ManyToOne
    @JoinColumn(name = "type_metier_poste", nullable=false)
    private TypeMetier typeMetier;

    /**
    *
    */
    @OneToMany(mappedBy = "poste")
    private List<Image> images;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TypeMetier getTypeMetier() {
		return typeMetier;
	}

	public void setTypeMetier(TypeMetier typeMetier) {
		this.typeMetier = typeMetier;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}
    
}
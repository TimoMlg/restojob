package fr.isika.cdi3.jee.entity;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="compte_bancaire")
public class CompteBancaire {

    /**
     * Default constructor
     */
    public CompteBancaire() {
    }

    /**
     * 
     */
    @Column(name="titulaire", nullable= false, length=200)
    private String titulaire;

    /**
     * 
     */
    @Column(name="iban", nullable= false, length=200)
    private String IBAN;

    /**
     * 
     */
    @Column(name="bic", nullable= false, length=200)
    private String BIC;

    /**
     * 
     */
    @Id
    @Column(name="id_compte_bancaire")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

	public String getTitulaire() {
		return titulaire;
	}

	public void setTitulaire(String titulaire) {
		this.titulaire = titulaire;
	}

	public String getIBAN() {
		return IBAN;
	}

	public void setIBAN(String iBAN) {
		IBAN = iBAN;
	}

	public String getBIC() {
		return BIC;
	}

	public void setBIC(String bIC) {
		BIC = bIC;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
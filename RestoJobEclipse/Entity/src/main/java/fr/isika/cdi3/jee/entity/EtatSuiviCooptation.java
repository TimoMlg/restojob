package fr.isika.cdi3.jee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table (name = "etat_suivi_cooptation")
public class EtatSuiviCooptation {

    /**
     * Default constructor
     */
    public EtatSuiviCooptation() {
    }

    /**
     * 
     */
    @Column (name = "libelle_etat_suivi_cooptation", nullable = false, length = 50)
    private String libelle;

    /**
     * 
     */
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_etat_suivi_cooptation")
    private Integer id;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
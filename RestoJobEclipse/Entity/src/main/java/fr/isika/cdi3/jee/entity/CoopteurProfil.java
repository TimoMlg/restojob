package fr.isika.cdi3.jee.entity;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="coopteur_profil")
public class CoopteurProfil extends Personne {

    /**
     * Default constructor
     */
    public CoopteurProfil() {
    }

    /**
     * 
     */
    @Column(name="date_naissance", nullable= false)
    private Date dateNaissance;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_photo_coopteur_profil", foreignKey=@ForeignKey(name="FK_COOPTEUR_PROFIL_PHOTO"))
    private Image photo;

    /**
     * 
     */
    @Column(name="presentation", length=1000)
    private String presentation;

    /**
     * 
     */
    @Column(name="veut_etre_recommande", nullable=false)
    private boolean VeutEtreRecommande;

    /**
     * 
     */
    @ManyToMany
    @JoinTable(name="profils_associes", 
    joinColumns = @JoinColumn(name="id_coopteur_profils_associes"),
    inverseJoinColumns = @JoinColumn(name = "id_profil_profils_associes"))
   
    private List<CoopteurProfil> profilsAssocies;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_compte_bancaire_coopteur_profil", foreignKey=@ForeignKey(name="FK_COOPTEUR_PROFIL_COMPTE_BANCAIRE"))
    private CompteBancaire compteBancaire;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_etat_compte_coopteur_profil",
    columnDefinition="int default 1",  foreignKey=@ForeignKey(name="FK_COOPTEUR_PROFIL_ETAT_COMPTE"))
    private EtatCompte etatCompte;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_adresse_coopteur_profil", nullable=false, foreignKey=@ForeignKey(name="FK_COOPTEUR_PROFIL_ADRESSE"))
    private Adresse adresse;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_pretentions_salariales_coopteur_profil", foreignKey=@ForeignKey(name="FK_COOPTEUR_PROFIL_PRETENTIONS_SALARIALES"))
    private Salaire pretentionsSalariales;

    /**
     * 
     */ 
    @OneToMany(mappedBy = "coopteurProfil")
    private List<Niveau> niveaux;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_experience_coopteur_profil", foreignKey=@ForeignKey(name="FK_COOPTEUR_PROFIL_EXPERIENCE"))
    private Experience experience;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn(name="id_ancien_employeur_coopteur_profil", foreignKey=@ForeignKey(name="FK_COOPTEUR_PROFIL_ANCIEN_EMPLOYEUR"))
    private Personne ancienEmployeur;

    /**
     * 
     */
    @OneToMany(mappedBy="profil")
    private List<SuiviCooptation> suiviCooptation;
    
    @ManyToMany
    @JoinTable(name="coopteur_profil_offre",joinColumns=@JoinColumn (name="id_coopteur_profil_coopteur_profil_offre"),
    inverseJoinColumns=@JoinColumn(name="id_offre_coopteur_profil_offre"))
    private List<Offre> offresEnregistrees;

	public List<Niveau> getNiveaux() {
		return niveaux;
	}

	public void setNiveaux(List<Niveau> niveaux) {
		this.niveaux = niveaux;
	}

	public List<Offre> getOffresEnregistrees() {
		return offresEnregistrees;
	}

	public void setOffresEnregistrees(List<Offre> offresEnregistrees) {
		this.offresEnregistrees = offresEnregistrees;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public Image getPhoto() {
		return photo;
	}

	public void setPhoto(Image photo) {
		this.photo = photo;
	}

	public String getPresentation() {
		return presentation;
	}

	public void setPresentation(String presentation) {
		this.presentation = presentation;
	}

	public boolean isVeutEtreRecommande() {
		return VeutEtreRecommande;
	}

	public void setVeutEtreRecommande(boolean veutEtreRecommande) {
		VeutEtreRecommande = veutEtreRecommande;
	}

	public List<CoopteurProfil> getProfilsAssocies() {
		return profilsAssocies;
	}

	public void setProfilsAssocies(List<CoopteurProfil> profilsAssocies) {
		this.profilsAssocies = profilsAssocies;
	}

	public CompteBancaire getCompteBancaire() {
		return compteBancaire;
	}

	public void setCompteBancaire(CompteBancaire compteBancaire) {
		this.compteBancaire = compteBancaire;
	}

	public EtatCompte getEtatCompte() {
		return etatCompte;
	}

	public void setEtatCompte(EtatCompte etatCompte) {
		this.etatCompte = etatCompte;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public Salaire getPretentionsSalariales() {
		return pretentionsSalariales;
	}

	public void setPretentionsSalariales(Salaire pretentionsSalariales) {
		this.pretentionsSalariales = pretentionsSalariales;
	}

	public List<Niveau> getCompetences() {
		return niveaux;
	}

	public void setCompetences(List<Niveau> competences) {
		this.niveaux = competences;
	}

	public Experience getExperience() {
		return experience;
	}

	public void setExperience(Experience experience) {
		this.experience = experience;
	}

	public Personne getAncienEmployeur() {
		return ancienEmployeur;
	}

	public void setAncienEmployeur(Personne ancienEmployeur) {
		this.ancienEmployeur = ancienEmployeur;
	}

	public List<SuiviCooptation> getSuiviCooptation() {
		return suiviCooptation;
	}

	public void setSuiviCooptation(List<SuiviCooptation> suiviCooptation) {
		this.suiviCooptation = suiviCooptation;
	}


}
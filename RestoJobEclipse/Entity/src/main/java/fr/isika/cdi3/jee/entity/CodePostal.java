package fr.isika.cdi3.jee.entity;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="code_postal")
public class CodePostal {

    /**
     * Default constructor
     */
    public CodePostal() {
    }

    /**
     * 
     */
    @Column(name="libelle_code_postal" ,nullable= false, length=200)
    private String libelle;

    /**
     * 
     */
    @Id
    @Column(name="id_code_postal")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
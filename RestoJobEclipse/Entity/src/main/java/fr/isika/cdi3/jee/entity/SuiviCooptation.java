package fr.isika.cdi3.jee.entity;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 */
@Entity
@Table(name="suivi_cooptation")
public class SuiviCooptation {

    /**
     * Default constructor
     */
    public SuiviCooptation() {
    }

    /**
     *
     */
    @Column (name = "date_selection")
    private LocalDateTime dateSelection;

    /**
     *
     */
    @Column (name = "date_recrutement")
    private LocalDateTime dateRecrutement;

    /**
     *
     */
    @Column (name = "fin_periode_essai")
    private LocalDateTime dateFinPeriodeEssai;

    /**
     *
     */
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_suivi_cooptation")
    private Integer id;

    /**
     *
     */
    @ManyToOne
    @JoinColumn (name = "etat_cooptation", nullable = false,
    foreignKey = @ForeignKey(name = "FK_SUIVI_COOPTATION_ETAT"))
    private EtatSuiviCooptation etatCooptation;


    /**
     *
     */
    @ManyToOne
    @JoinColumn (name = "profil", nullable = false,
    foreignKey = @ForeignKey(name = "FK_suivi_cooptation_coopteur_profil"))
    private CoopteurProfil profil;

    /**
     *
     */
    @ManyToOne
    @JoinColumn (name = "coopteur", nullable = false,
    foreignKey = @ForeignKey(name = "FK_suivi_cooptation_coopteur"))
    private CoopteurProfil coopteur;

    /**
     *
     */
    @ManyToOne
    @JoinColumn (name = "offre", nullable = false,
    foreignKey = @ForeignKey(name = "FK_suivi_cooptation_offre"))
    private Offre offre;

    /**
     *
     */
    @OneToMany (mappedBy="suiviCooptation")
    private List<Entretien> entretiens;
    
    @Column(name="date_creation", nullable=false)
    private LocalDateTime dateCreation;    

	public LocalDateTime getDateSelection() {
		return dateSelection;
	}

	public void setDateSelection(LocalDateTime dateSelection) {
		this.dateSelection = dateSelection;
	}

	public LocalDateTime getDateRecrutement() {
		return dateRecrutement;
	}

	public void setDateRecrutement(LocalDateTime dateRecrutement) {
		this.dateRecrutement = dateRecrutement;
	}

	public LocalDateTime getDateFinPeriodeEssai() {
		return dateFinPeriodeEssai;
	}

	public void setDateFinPeriodeEssai(LocalDateTime dateFinPeriodeEssai) {
		this.dateFinPeriodeEssai = dateFinPeriodeEssai;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EtatSuiviCooptation getEtatCooptation() {
		return etatCooptation;
	}

	public void setEtatCooptation(EtatSuiviCooptation etatCooptation) {
		this.etatCooptation = etatCooptation;
	}

	public CoopteurProfil getProfil() {
		return profil;
	}

	public void setProfil(CoopteurProfil profil) {
		this.profil = profil;
	}

	public CoopteurProfil getCoopteur() {
		return coopteur;
	}

	public void setCoopteur(CoopteurProfil coopteur) {
		this.coopteur = coopteur;
	}

	public Offre getOffre() {
		return offre;
	}

	public void setOffre(Offre offre) {
		this.offre = offre;
	}

	public List<Entretien> getEntretiens() {
		return entretiens;
	}

	public void setEntretiens(List<Entretien> entretiens) {
		this.entretiens = entretiens;
	}

	public LocalDateTime getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDateTime dateCreation) {
		this.dateCreation = dateCreation;
	}


}
package fr.isika.cdi3.jee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="message")
public class Message {

    /**
     * Default constructor
     */
    public Message() {
    }

    /**
     * 
     */
    @Column (nullable = false, length = 50)
    private String objet;

    /**
     * 
     */
    @Column (nullable = false, length = 1000)
    private String contenu;

    /**
     * 
     */
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_message")
    private Integer id;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn (name = "id_etat_message_message", nullable = false,
    foreignKey = @ForeignKey (name = "FK_MESSAGE_ETAT_MESSAGE"))
    private EtatMessage etatMessage;
    
    /**
     * 
     */
    @ManyToOne
    @JoinColumn (name = "id_personne_emetteur", nullable = false,
    foreignKey = @ForeignKey (name = "FK_MESSAGE_PERSONNE"))
    private Personne emetteur;
    
    /**
     * 
     */
    @ManyToOne
    @JoinColumn (name = "id_personne_destinataire", nullable = false,
    foreignKey = @ForeignKey (name = "FK_MESSAGE_PERSONNE_DESTINATAIRE"))
    private Personne destinataire;

	public String getObjet() {
		return objet;
	}

	public void setObjet(String objet) {
		this.objet = objet;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EtatMessage getEtatMessage() {
		return etatMessage;
	}

	public void setEtatMessage(EtatMessage etatMessage) {
		this.etatMessage = etatMessage;
	}

}
package fr.isika.cdi3.jee.entity;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="competence")
public class Competence {

  /**
   * Default constructor
   */
  public Competence() {
  }

  /**
   * 
   */
  @Column(name="libelle_competences", nullable=false, length=200)
  private String libelle;

  /**
   * 
   */
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @Column(name="id_competences")
  private Integer id;

  /**
   * 
   */
  @ManyToOne
  @JoinColumn(name="id_competence_parent", foreignKey=@ForeignKey(name="FK_COMPETENCES_COMPETENCE_PARENT"))
  private Competence competenceParent;

  @ManyToMany(mappedBy = "competences")
  private List<Offre> offres;

  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Competence getCompetenceParent() {
    return competenceParent;
  }

  public void setCompetenceParent(Competence competenceParent) {
    this.competenceParent = competenceParent;
  }

  public List<Offre> getOffres() {
    return offres;
  }

  public void setOffres(List<Offre> offres) {
    this.offres = offres;
  }

}
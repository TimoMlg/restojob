package fr.isika.cdi3.jee.entity;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * 
 */
@Entity
@Inheritance (strategy = InheritanceType.JOINED)
public class Personne {

    /**
     * Default constructor
     */
    public Personne() {
    }

    /**
     * 
     */
    @Column (name = "nom_personne", nullable=false)
    private String nom;

    /**
     * 
     */
    @Column (name = "prenom", nullable=false)
    private String prenom;

    /**
     * 
     */
    @Column (name = "fonction", nullable=false)
    private String fonction;

    /**
     * 
     */
    @Column (name = "numero_telephone", nullable=false)
    private String numeroTelephone;

    /**
     * 
     */
    @Column (name = "mail_personne", nullable = false)
    private String mail;

    /**
     * 
     */
    @Column (name = "profil_linkedin")
    private String profilLinkedIn;


    /**
     * 
     */
    @Column (name = "mot_de_passe_personne", nullable = false)
    private String motPasse;

    /**
     * 
     */
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_personne")
    private Integer id;

    /**
     * 
     */
    
    @ManyToOne
    @JoinColumn(name="civilite_personne", nullable=false)
    private Civilite civilite;

    /**
     * 
     */
    @OneToMany (mappedBy = "emetteur")
    private List<Message> messages;
    
    /**
     * 
     * @return
     */
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	public String getNumeroTelephone() {
		return numeroTelephone;
	}

	public void setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getProfilLinkedIn() {
		return profilLinkedIn;
	}

	public void setProfilLinkedIn(String profilLinkedIn) {
		this.profilLinkedIn = profilLinkedIn;
	}

	public String getMotPasse() {
		return motPasse;
	}

	public void setMotPasse(String motPasse) {
		this.motPasse = motPasse;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

}
package fr.isika.cdi3.jee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="salaire")
public class Salaire {

    /**
     * Default constructor
     */
    public Salaire() {
    }

    /**
     * 
     */
    @Column (name = "salaire_min", nullable = false)
    private Integer salaireMin;

    /**
     * 
     */
    @Column (name = "salaire_max", nullable = false)
    private Integer salaireMax;

    /**
     * 
     */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column (name = "id_salaire")
    private Integer id;

	public Integer getSalaireMin() {
		return salaireMin;
	}

	public void setSalaireMin(Integer salaireMin) {
		this.salaireMin = salaireMin;
	}

	public Integer getSalaireMax() {
		return salaireMax;
	}

	public void setSalaireMax(Integer salaireMax) {
		this.salaireMax = salaireMax;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
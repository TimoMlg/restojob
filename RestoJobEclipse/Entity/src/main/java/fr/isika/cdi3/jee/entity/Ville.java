package fr.isika.cdi3.jee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 */
@Entity
@Table(name="ville")
public class Ville {

    /**
     * Default constructor
     */
    public Ville() {
    }


    /**
     * 
     */
    @Column (name = "libelle_ville",nullable=false, length = 50)
    private String libelle;

    /**
     * 
     */
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id_ville")
    private Integer id;

    /**
     * 
     */
    @ManyToOne
    @JoinColumn (name = "id_pays_ville", columnDefinition = "int default 1",
    foreignKey = @ForeignKey (name = "FK_VILLE_PAYS"))
    private Pays pays;

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}

}
package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoCivilite;

public interface ICiviliteDao {

  List<DtoCivilite> getAll();

}

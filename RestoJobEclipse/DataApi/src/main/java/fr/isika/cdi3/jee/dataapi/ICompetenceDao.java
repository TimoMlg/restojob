package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoCompetence;

public interface ICompetenceDao {
  List<DtoCompetence> getAll();

  DtoCompetence getById(int idCompetence);
}

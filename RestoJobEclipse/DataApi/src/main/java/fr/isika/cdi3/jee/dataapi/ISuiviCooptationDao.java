package fr.isika.cdi3.jee.dataapi;

import java.time.LocalDateTime;
import java.util.List;

import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

public interface ISuiviCooptationDao {
  void addSuiviCooptation(LocalDateTime dateCreation, int idCoopteur, int idEtatCooptation, int idOffre, int idProfil);

  boolean isDejaCoopte(int idOffre, String mail);

  boolean countLimiteCooptation(int idCoopteur, int idOffre);

  void updateEtatSuiviCooptationValid(DtoSuiviCooptation dtosc);

  void updateEtatSuiviCooptationRefus(DtoSuiviCooptation dtosc);

  List<DtoSuiviCooptation> getByOffre(int idOffre);

  long getSuiviSelonEtat(int idPersonne, int idEtat);

  DtoSuiviCooptation updateEtatSuiviCooptationSelection(DtoSuiviCooptation dtosc);

  DtoSuiviCooptation updateEtatSuiviCooptationEntretien(DtoSuiviCooptation dtosc);

  DtoSuiviCooptation updateEtatSuiviCooptationRecrutement(DtoSuiviCooptation dtosc);

  DtoSuiviCooptation updateEtatSuiviCooptationPeriodeEssai(DtoSuiviCooptation dtosc);

  Long nbProfilsRecrutes(int idEntreprise);

  Long nbValidationRecrutementCoopteur(int idCoopteur);

  Long nbRecommandationsCoopteur(int idCoopteur);

  List<DtoSuiviCooptation> entretiensByCoopteur(int idParticulier);

  List<DtoSuiviCooptation> recrutementsByCoopteur(int idParticulier);

  List<DtoSuiviCooptation> selectionsByCoopteur(int idParticulier);
}

package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoImage;
import fr.isika.cdi3.jee.dto.DtoPoste;

public interface IImageDao {
  List<DtoImage> getByPoste(int idPoste);
  
  DtoImage getById(int id);
}

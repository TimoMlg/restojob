package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoTypeMetier;

public interface ITypeMetierDao {
  List<DtoTypeMetier> getAll();
}

package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoNiveau;

public interface INiveauDao {
  List<DtoNiveau> getByIdCoopteurProfil(int idCP);

  List<DtoNiveau> getByIdEntreprise(int idEntreprise);

}

package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoPoste;

public interface IPosteDao {
	List<DtoPoste> getAll();
	DtoPoste getById(int idPoste);
}

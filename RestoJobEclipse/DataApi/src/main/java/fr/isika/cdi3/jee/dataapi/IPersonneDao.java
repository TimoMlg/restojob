package fr.isika.cdi3.jee.dataapi;

import fr.isika.cdi3.jee.dto.DtoPersonne;

public interface IPersonneDao {

	public DtoPersonne getConnexion(String mail, String motPasse) throws Exception;

	boolean isEnregistre(String mail);

	int getIdByMail(String mail);
}

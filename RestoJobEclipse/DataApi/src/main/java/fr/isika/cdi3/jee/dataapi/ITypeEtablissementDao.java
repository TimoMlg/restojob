package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoTypeEtablissement;

public interface ITypeEtablissementDao {
  List<DtoTypeEtablissement> getAll();
}

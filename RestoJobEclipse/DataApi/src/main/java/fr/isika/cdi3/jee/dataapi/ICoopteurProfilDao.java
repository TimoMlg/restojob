package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoOffre;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;


public interface ICoopteurProfilDao {

	boolean checkVeutRecommandation(int idProfil);

	boolean isCoopteurProfil(int idProfil);
	
	public void creerCompteParticulier(DtoCoopteurProfil particulier);

	DtoCoopteurProfil getById(int idDtoPersonne);

	long recommandationsReussies(int idParticulier);

	long personnesRecommandees(int idParticulier);

	long cooptationsEnCours(int idParticulier);

	List<DtoCoopteurProfil> profilsAssocies(int idParticulier);

	List<DtoOffre> listeOffresFav(int idParticulier);

	List<DtoOffre> listeRecommandationsReussies(int idParticulier);

}

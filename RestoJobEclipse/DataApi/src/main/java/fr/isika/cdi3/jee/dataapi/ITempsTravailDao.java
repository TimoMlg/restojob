package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoTempsTravail;

public interface ITempsTravailDao {
	List<DtoTempsTravail> getAll();
	DtoTempsTravail getById(int idTT);
}

package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoVoie;

public interface IVoieDao {
  List<DtoVoie> getall();
}

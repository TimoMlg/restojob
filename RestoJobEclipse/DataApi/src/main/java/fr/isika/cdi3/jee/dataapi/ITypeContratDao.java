package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoTypeContrat;

public interface ITypeContratDao {
  List<DtoTypeContrat> getAll();
  DtoTypeContrat getById(int idTC);
}

package fr.isika.cdi3.jee.dataapi;

import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoPersonne;

public interface IEntrepriseDao {
	
	public void creerCompteEntreprise(DtoEntreprise ent, DtoPersonne pers);
	public DtoEntreprise getById(int idEntreprise);

}

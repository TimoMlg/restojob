package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoOffre;

public interface IOffreDao {
	
	List<DtoOffre> GetAll(String recherche, String gastronomie, String bistronomie, String patisserie, String traiteur,
	    String boulangerie, String cuisineRegionale, String grandService, String petitService, String fruitsMer,
	    String cuisineMesure, String experienceCulinaire, String troisHuit, String cuisineThematique, String cuisineModerne,
	    String CDI, String CDD, String vacation, String cuisine, String salle);

     void bookmark(DtoOffre dtoOffreBookmark, DtoCoopteurProfil dtoCoopteurProfil);

	DtoOffre GetById(int IdOffre); 

	void createOffer(DtoOffre dtoOffre);
	
	Long nbOffresPostees(int idEntreprise);

	List<DtoOffre> GetByEntreprise(int idEntreprise);


}

package fr.isika.cdi3.jee.dataapi;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoEntretien;

public interface IEntretienDao {

  List<DtoEntretien> datesEntretien(int idSuiviCooptation);

}

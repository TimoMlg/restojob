package fr.isika.cdi3.jee.dataapi;

import java.util.List;


import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;


public interface ISuiviRecommandationDAO {

	 List<DtoSuiviCooptation>  getRecommandation(int idCoopteur);

	 
}

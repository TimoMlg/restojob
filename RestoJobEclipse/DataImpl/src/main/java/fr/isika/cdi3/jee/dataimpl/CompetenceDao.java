package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.ICompetenceDao;
import fr.isika.cdi3.jee.dto.DtoCompetence;
import fr.isika.cdi3.jee.entity.Competence;

@Remote(ICompetenceDao.class)
@Stateless
public class CompetenceDao extends Dao implements ICompetenceDao {

  @Override
  public List<DtoCompetence> getAll() {
    List<Competence> competences = entityManager.createQuery("SELECT com FROM Competence com", Competence.class)
        .getResultList();
    List<DtoCompetence> result = new ArrayList<>();
    for (Competence c : competences) {
      result.add(EntityUtils.transformEntityToDto(c));
    }
    return result;
  }

  @Override
  public DtoCompetence getById(int idCompetence) {
    Competence competence = entityManager.find(Competence.class, idCompetence);

    return EntityUtils.transformEntityToDto(competence);
  }

}

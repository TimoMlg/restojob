package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.ITypeMetierDao;
import fr.isika.cdi3.jee.dto.DtoTypeMetier;
import fr.isika.cdi3.jee.entity.TypeMetier;

@Remote(ITypeMetierDao.class)
@Stateless
public class TypeMetierDao extends Dao implements ITypeMetierDao {

  @Override
  public List<DtoTypeMetier> getAll() {
    List<TypeMetier> typeMetiers = entityManager.createQuery("SELECT tm "
        + "FROM TypeMetier tm", TypeMetier.class).getResultList();
    
    List<DtoTypeMetier> result = new ArrayList<>();
    for (TypeMetier typeMetier : typeMetiers) {
      result.add(EntityUtils.transformEntityToDto(typeMetier));
    }
    return result;
  }

}

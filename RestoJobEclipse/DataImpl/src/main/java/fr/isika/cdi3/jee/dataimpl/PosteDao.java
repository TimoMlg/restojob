package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import fr.isika.cdi3.jee.dataapi.IPosteDao;
import fr.isika.cdi3.jee.dto.DtoPoste;
import fr.isika.cdi3.jee.entity.Poste;

@Remote(IPosteDao.class)
@Stateless
public class PosteDao extends Dao implements IPosteDao{

	@Override
	public List<DtoPoste> getAll() {
		List<Poste> postes = entityManager.createQuery("SELECT poste FROM Poste poste",Poste.class).getResultList();
		List<DtoPoste> result = new ArrayList<>();
		for(Poste p:postes) {
			result.add(EntityUtils.transformEntityToDto(p));
		}
		return result;
	}

	@Override
	public DtoPoste getById(int idPoste) {
		DtoPoste dtoPoste=null;
		String requete="SELECT poste FROM Poste poste WHERE id like :paramId";
		Poste poste=entityManager.createQuery(requete,Poste.class).setParameter("paramId", idPoste).getSingleResult();
		dtoPoste=EntityUtils.transformEntityToDto(poste);
		return dtoPoste;
	}
	
}

package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.ISuiviRecommandationDAO;

import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

import fr.isika.cdi3.jee.entity.SuiviCooptation;

@Remote(ISuiviRecommandationDAO.class)
@Stateless 
public class SuiviRecommandationDAO extends Dao implements ISuiviRecommandationDAO {
	
	@Override
	public List<DtoSuiviCooptation> getRecommandation(int idCoopteur) {
		List<SuiviCooptation> validations = entityManager.createQuery("SELECT sc "
															+ "FROM SuiviCooptation sc "
															+ "WHERE sc.etatCooptation.id = 1 "
															+"AND sc.profil.id = :tartampion", SuiviCooptation.class)
				.setParameter("tartampion", idCoopteur)
				.getResultList();
		List<DtoSuiviCooptation> valid = new ArrayList<DtoSuiviCooptation>();
		
		for (SuiviCooptation SuiviCooptation : validations) {
		valid.add(EntityUtils.transformEntityToDto(SuiviCooptation));
				
		}
		return valid;
	}
	

}

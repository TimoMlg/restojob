package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.IVoieDao;
import fr.isika.cdi3.jee.dto.DtoVoie;
import fr.isika.cdi3.jee.entity.Voie;

@Remote(IVoieDao.class)
@Stateless
public class VoieDao extends Dao implements IVoieDao {

  @Override
  public List<DtoVoie> getall() {
    List<Voie> voies = entityManager.createQuery("SELECT v FROM Voie v",
        Voie.class).getResultList();
    List<DtoVoie> result = new ArrayList<>();
    for (Voie voie : voies) {
      result.add(EntityUtils.transformEntityToDto(voie));
    }
    return result;
  }

}

package fr.isika.cdi3.jee.dataimpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.INiveauDao;
import fr.isika.cdi3.jee.dataapi.ISuiviCooptationDao;
import fr.isika.cdi3.jee.dto.DtoNiveau;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;
import fr.isika.cdi3.jee.entity.Entretien;
import fr.isika.cdi3.jee.entity.EtatSuiviCooptation;
import fr.isika.cdi3.jee.entity.SuiviCooptation;

@Stateless
@Remote(ISuiviCooptationDao.class)
public class SuiviCooptationDao extends Dao implements ISuiviCooptationDao {

  @EJB
  private INiveauDao proxyNiv;

  @Override
  public void addSuiviCooptation(LocalDateTime dateCreation, int idCoopteur, int idEtatCooptation, int idOffre,
      int idProfil) {
    entityManager
        .createNativeQuery("INSERT INTO suivi_cooptation (date_creation, coopteur, etat_cooptation, offre, profil) "
            + "VALUES (?,?,?,?,?)")
        .setParameter(1, dateCreation).setParameter(2, idCoopteur).setParameter(3, idEtatCooptation)
        .setParameter(4, idOffre).setParameter(5, idProfil).executeUpdate();

  }

  @Override
  public boolean isDejaCoopte(int idOffre, String mail) {
    long count = entityManager.createQuery("SELECT COUNT(*) FROM SuiviCooptation sc "
        + "INNER JOIN CoopteurProfil cp ON sc.profil.id = cp.id " + "WHERE cp.mail = ?1 AND sc.offre.id = ?2",
        Long.class).setParameter(1, mail).setParameter(2, idOffre).getSingleResult();
    if (count != 0) {
      return true;
    }
    return false;
  }

  @Override
  public boolean countLimiteCooptation(int idCoopteur, int idOffre) {
    long count = entityManager
        .createQuery("SELECT COUNT(*) FROM SuiviCooptation sc " + "WHERE sc.offre.id = ?1 AND sc.coopteur.id = ?2",
            Long.class)
        .setParameter(1, idOffre).setParameter(2, idCoopteur).getSingleResult();
    if (count >= 3) {
      return true;
    }
    return false;
  }

  @Override
  public void updateEtatSuiviCooptationValid(DtoSuiviCooptation dtosc) {

    SuiviCooptation sc = entityManager.find(SuiviCooptation.class, dtosc.getId());
    EtatSuiviCooptation esc = new EtatSuiviCooptation();
    esc.setId(2);
    sc.setEtatCooptation(esc);
    entityManager.merge(sc);
  }

  @Override
  public void updateEtatSuiviCooptationRefus(DtoSuiviCooptation dtosc) {
    SuiviCooptation sc = entityManager.find(SuiviCooptation.class, dtosc.getId());
    EtatSuiviCooptation esc = new EtatSuiviCooptation();
    esc.setId(7);
    sc.setEtatCooptation(esc);
    entityManager.merge(sc);

  }

  @Override
  public DtoSuiviCooptation updateEtatSuiviCooptationSelection(DtoSuiviCooptation dtosc) {

    SuiviCooptation sc = entityManager.find(SuiviCooptation.class, dtosc.getId());
    EtatSuiviCooptation esc = new EtatSuiviCooptation();
    esc.setId(3);
    sc.setEtatCooptation(esc);
    sc.setDateSelection(LocalDateTime.now());
    entityManager.merge(sc);
    return EntityUtils.transformEntityToDto(sc);
  }

  @Override
  public DtoSuiviCooptation updateEtatSuiviCooptationEntretien(DtoSuiviCooptation dtosc) {

    SuiviCooptation sc = entityManager.find(SuiviCooptation.class, dtosc.getId());
    EtatSuiviCooptation esc = new EtatSuiviCooptation();
    Entretien entretien = new Entretien(LocalDateTime.now(), EntityUtils.transformDtoToEntity(dtosc));
    entityManager.persist(entretien);
    esc.setId(4);
    sc.setEtatCooptation(esc);
    sc.setDateSelection(LocalDateTime.now());
    entityManager.merge(sc);
    return EntityUtils.transformEntityToDto(sc);
  }

  @Override
  public DtoSuiviCooptation updateEtatSuiviCooptationRecrutement(DtoSuiviCooptation dtosc) {

    SuiviCooptation sc = entityManager.find(SuiviCooptation.class, dtosc.getId());
    EtatSuiviCooptation esc = new EtatSuiviCooptation();
    esc.setId(5);
    sc.setEtatCooptation(esc);
    sc.setDateRecrutement(LocalDateTime.now());
    entityManager.merge(sc);
    return EntityUtils.transformEntityToDto(sc);
  }

  @Override
  public DtoSuiviCooptation updateEtatSuiviCooptationPeriodeEssai(DtoSuiviCooptation dtosc) {

    SuiviCooptation sc = entityManager.find(SuiviCooptation.class, dtosc.getId());
    sc.setEtatCooptation(entityManager.find(EtatSuiviCooptation.class, 6));
    sc.setDateFinPeriodeEssai(LocalDateTime.now());
    entityManager.merge(sc);
    return EntityUtils.transformEntityToDto(sc);
  }

  public Long nbProfilsRecrutes(int idEntreprise) {
    long count = entityManager
        .createQuery("SELECT COUNT(*) FROM SuiviCooptation sc WHERE sc.dateFinPeriodeEssai != null "
            + "AND sc.offre.entreprise.id = :pidEntreprise", Long.class)
        .setParameter("pidEntreprise", idEntreprise).getSingleResult();
    return count;

  }

  public Long nbRecommandationsCoopteur(int idCoopteur) {
    long count = entityManager
        .createQuery("SELECT COUNT(*) FROM SuiviCooptation sc WHERE sc.coopteur.id = :pidCoopteurProfil", Long.class)
        .setParameter("pidCoopteurProfil", idCoopteur).getSingleResult();
    return count;

  }

  public Long nbValidationRecrutementCoopteur(int idCoopteur) {
    Long count = entityManager
        .createQuery("SELECT COUNT(*) FROM SuiviCooptation sc WHERE sc.coopteur.id = :pidCoopteurProfil"
            + " AND sc.dateFinPeriodeEssai != null", Long.class)
        .setParameter("pidCoopteurProfil", idCoopteur).getSingleResult();
    return count;

  }

  @Override
  public List<DtoSuiviCooptation> getByOffre(int idOffre) {
    List<SuiviCooptation> suivis = entityManager
        .createQuery("SELECT sc FROM SuiviCooptation sc " + "WHERE sc.offre.id = :pid AND sc.etatCooptation.id != 1",
            SuiviCooptation.class)
        .setParameter("pid", idOffre).getResultList();
    List<DtoSuiviCooptation> result = new ArrayList<>();
    for (SuiviCooptation suiviCooptation : suivis) {
      List<DtoNiveau> niveaux = proxyNiv.getByIdCoopteurProfil(suiviCooptation.getProfil().getId());
      DtoSuiviCooptation suivi = EntityUtils.transformEntityToDto(suiviCooptation);
      suivi.getProfil().setNiveaux(niveaux);
      result.add(suivi);
    }
    return result;
  }

  @Override
  public long getSuiviSelonEtat(int idPersonne, int idEtat) {
    return entityManager.createQuery(
        "SELECT COUNT(*) FROM SuiviCooptation sc WHERE sc.etatCompte.id = :petat "
            + "AND (sc.coopteur = :pid OR sc.offre.entreprise.id = :pid) " + "AND sc.offre.etatOffre.id != 3",
        Long.class).setParameter("petat", idEtat).setParameter("pid", idPersonne).getSingleResult();
  }

  @Override
  public List<DtoSuiviCooptation> entretiensByCoopteur(int idParticulier) {
    List<SuiviCooptation> suivisCooptation = entityManager
        .createQuery("SELECT sc FROM SuiviCooptation sc JOIN FETCH sc.profil p " + " JOIN FETCH sc.offre o "
            + " WHERE sc.coopteur.id = ?1 AND sc.etatCooptation.id >= 4", SuiviCooptation.class)
        .setParameter(1, idParticulier).getResultList();
    List<DtoSuiviCooptation> dtoSuivisCooptation = new ArrayList<DtoSuiviCooptation>();
    for (SuiviCooptation sC : suivisCooptation) {
      dtoSuivisCooptation.add(EntityUtils.transformEntityToDto(sC));
    }
    return dtoSuivisCooptation;
  }

  @Override
  public List<DtoSuiviCooptation> recrutementsByCoopteur(int idParticulier) {
    List<SuiviCooptation> suivisCooptation = entityManager
        .createQuery("SELECT sc FROM SuiviCooptation sc JOIN FETCH sc.profil p " + "JOIN FETCH sc.offre o "
            + "WHERE sc.coopteur.id = ?1 AND sc.etatCooptation.id >= 5", SuiviCooptation.class)
        .setParameter(1, idParticulier).getResultList();
    List<DtoSuiviCooptation> dtoSuivisCooptation = new ArrayList<DtoSuiviCooptation>();
    for (SuiviCooptation sC : suivisCooptation) {
      dtoSuivisCooptation.add(EntityUtils.transformEntityToDto(sC));
    }
    return dtoSuivisCooptation;
  }

  @Override
  public List<DtoSuiviCooptation> selectionsByCoopteur(int idParticulier) {
    List<SuiviCooptation> suivisCooptation = entityManager
        .createQuery("SELECT sc FROM SuiviCooptation sc JOIN FETCH sc.profil p " + "JOIN FETCH sc.offre o "
            + "WHERE sc.coopteur.id = ?1 AND sc.etatCooptation.id >= 3", SuiviCooptation.class)
        .setParameter(1, idParticulier).getResultList();
    List<DtoSuiviCooptation> dtoSuivisCooptation = new ArrayList<DtoSuiviCooptation>();
    for (SuiviCooptation sC : suivisCooptation) {
      dtoSuivisCooptation.add(EntityUtils.transformEntityToDto(sC));
    }
    return dtoSuivisCooptation;
  }
}

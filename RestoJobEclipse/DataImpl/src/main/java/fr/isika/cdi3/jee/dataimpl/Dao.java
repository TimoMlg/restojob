package fr.isika.cdi3.jee.dataimpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class Dao {
	@PersistenceContext(unitName = "ConnexionUser")
	public EntityManager entityManager;
}

package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.IOffreDao;
import fr.isika.cdi3.jee.dto.DtoCompetence;
import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.entity.CoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoImage;
import fr.isika.cdi3.jee.dto.DtoOffre;
import fr.isika.cdi3.jee.entity.Image;
import fr.isika.cdi3.jee.entity.Offre;
import fr.isika.cdi3.jee.entity.TypeEtablissement;


@Stateless @Remote(IOffreDao.class)
public class OffreDao extends Dao implements IOffreDao {

	@Override
	public List<DtoOffre> GetAll(String recherche, String gastronomie, String bistronomie, String patisserie,
			String traiteur, String boulangerie, String cuisineRegionale, String grandService, String petitService,
			String fruitsMer, String cuisineMesure, String experienceCulinaire, String troisHuit, String cuisineThematique,
			String cuisineModerne, String CDI, String CDD, String vacation, String cuisine, String salle) {

		/********************
		 * PARTIE DE DAPHNE *
		 ********************/
		// Condition pour mettre une valeur si au moins l'une des checkbox est cochée

		/*
		 * TYPE DE CONTRAT
		 */
		if (CDI == "" && CDD == "" && vacation == "") { }
		else
		{
			CDI = (CDI == "" ? "undefined" : CDI);
			CDD = (CDD == "" ? "undefined" : CDD);
			vacation = (vacation == "" ? "undefined" : vacation);
		}

		/*
		 * TYPE DE METIER
		 */
		if (salle =="" && cuisine == "") { }
		else {
			salle = (salle == "" ? "undefined" : salle);
			cuisine = (cuisine == "" ? "undefined" : cuisine);
		}

		/*
		 * TYPE D'ETABLISSEMENT
		 */
		if (gastronomie == "" && bistronomie == "" && patisserie == "" && traiteur == "" && boulangerie == ""
				&& cuisineRegionale == "" && grandService == "" && petitService == "" && fruitsMer == "" && cuisineMesure == ""
				&& experienceCulinaire == "" && troisHuit == "" && cuisineThematique == "" && cuisineModerne == "") { }
		else {
			gastronomie = (gastronomie == "" ? "undefined" : gastronomie);
			bistronomie = (bistronomie == "" ? "undefined" : bistronomie);
			patisserie = (patisserie == "" ? "undefined" : patisserie);
			traiteur = (traiteur == "" ? "undefined" : traiteur);
			boulangerie = (boulangerie == "" ? "undefined" : boulangerie);
			cuisineRegionale = (cuisineRegionale == "" ? "undefined" : cuisineRegionale);
			grandService = (grandService == "" ? "undefined" : grandService);
			petitService = (petitService == "" ? "undefined" : petitService);
			fruitsMer = (fruitsMer == "" ? "undefined" : fruitsMer);
			cuisineMesure = (cuisineMesure == "" ? "undefined" : cuisineMesure);
			experienceCulinaire = (experienceCulinaire == "" ? "undefined" : experienceCulinaire);
			troisHuit = (troisHuit == "" ? "undefined" : troisHuit);
			cuisineThematique = (cuisineThematique == "" ? "undefined" : cuisineThematique);
			cuisineModerne = (cuisineModerne == "" ? "undefined" : cuisineModerne);
		}

		/********************
		 * PARTIE D'HELOISE
		 ********************/
		List<Offre> offres = entityManager.createQuery("SELECT offre "
				+ "FROM Offre offre "
				+ "WHERE (offre.poste.libelle LIKE :parametre_recherche "
				+ "OR offre.entreprise.adresseSiege.ville.libelle LIKE :parametre_recherche) "
				+ "AND (offre.poste.typeMetier.libelle LIKE :pcuisine "
				+ "OR offre.poste.typeMetier.libelle LIKE :psalle) "
				+ "AND (offre.typeContrat.libelle LIKE :pcdi "
				+ "OR offre.typeContrat.libelle LIKE :pcdd "
				+ "OR offre.typeContrat.libelle LIKE :pvacation)", Offre.class)
				.setParameter("parametre_recherche", "%" + recherche + "%")
				.setParameter("pcuisine", "%" + cuisine + "%")
				.setParameter("psalle", "%" + salle + "%")
				.setParameter("pcdi", "%" + CDI + "%")
				.setParameter("pcdd", "%" + CDD + "%")
				.setParameter("pvacation", "%" + vacation + "%")
				.getResultList();

		List<DtoOffre> result = new ArrayList<DtoOffre>();


		for(Offre o : offres) {      
			List<TypeEtablissement> typeEtablissements = o.getEntreprise().getTypeEtablissements();
			for (TypeEtablissement etablissement : typeEtablissements) {
				String theme = etablissement.getLibelle();
				if (isUnchecked(gastronomie, bistronomie, patisserie, traiteur, boulangerie, cuisineRegionale, grandService,
						petitService, fruitsMer, cuisineMesure, experienceCulinaire, troisHuit, cuisineThematique, cuisineModerne)
						|| (theme.equals(cuisineModerne) || theme.equals(cuisineThematique) || theme.equals(troisHuit)
								|| theme.equals(experienceCulinaire) || theme.equals(cuisineMesure) || theme.equals(fruitsMer)
								|| theme.equals(petitService) || theme.equals(grandService) || theme.equals(cuisineRegionale)
								|| theme.equals(boulangerie) || theme.equals(traiteur) || theme.equals(patisserie) || theme.equals(bistronomie)
								|| theme.equals(gastronomie))) {
					if (result.isEmpty()) {
						List<Image> imgs = entityManager.createQuery("SELECT img FROM Image img WHERE img.poste.id = :pposte", Image.class)
								.setParameter("pposte", o.getPoste().getId()).getResultList();
						List<DtoImage> dtoimgs = new ArrayList<>();
						for (Image img : imgs) {
							dtoimgs.add(EntityUtils.transformEntityToDto(img));
						}
						DtoOffre offredto = EntityUtils.transformEntityToDto(o);
						offredto.getPoste().setImages(dtoimgs);            
						result.add(offredto);
						//on rentre que la
					}
					else
					{
						boolean trouve = false;
						//on rentre pas la
						for (DtoOffre offredto : result) {
							if (offredto.getDescription().equals(o.getDescritption())) {          
								trouve = true;
							}
						}
						if(!trouve) {
							List<Image> imgs = entityManager.createQuery("SELECT img FROM Image img WHERE img.poste.id = :pposte", Image.class)
									.setParameter("pposte", o.getPoste().getId()).getResultList();
							List<DtoImage> dtoimgs = new ArrayList<>();
							for (Image img : imgs) {
								dtoimgs.add(EntityUtils.transformEntityToDto(img));
							}
							DtoOffre offredto = EntityUtils.transformEntityToDto(o);
							offredto.getPoste().setImages(dtoimgs); 
							result.add(offredto);
						}
					}
				}
			}
		}       
		return result;
	}

	public boolean isUnchecked(String gastronomie, String bistronomie, String patisserie,
			String traiteur, String boulangerie, String cuisineRégionale, String grandService, String petitService,
			String fruitsMer, String cuisineMesure, String experienceCulinaire, String troisHuit, String cuisineThematique,
			String cuisineModerne) {
		boolean unchecked = false;

		if (gastronomie.equals("") && bistronomie.equals("") && patisserie.equals("") && traiteur.equals("")
				&& boulangerie.equals("") && cuisineRégionale.equals("") && grandService.equals("") && petitService.equals("")
				&& fruitsMer.equals("") && cuisineMesure.equals("") && experienceCulinaire.equals("") && troisHuit.equals("")
				&& cuisineThematique.equals("") && cuisineModerne.equals("")) {
			unchecked = true;
		}

		return unchecked;
	}

	/********************
	 * PARTIE DE Titi *
	 ********************/
@Override
public DtoOffre GetById(int IdOffre) {
	return EntityUtils.transformEntityToDto(entityManager.find(Offre.class, IdOffre));
}

/*public Long nbOffresPostees(int idEntreprise) {
	long count = entityManager.createQuery("SELECT COUNT(*) FROM Offre o WHERE o.entreprise.id = :pidEntreprise", Long.class)
			.setParameter("pidEntreprise", idEntreprise).getSingleResult();	
	return count;
}*/
	
@Override
public void createOffer(DtoOffre dtoOffre) {
	Offre offre=EntityUtils.transformDtoToEntity(dtoOffre);
	
	entityManager.persist(offre.getSalaire());
	entityManager.persist(offre.getExperience());
	entityManager.persist(offre);
}

public Long nbOffresPostees(int idEntreprise) {
	long count = entityManager.createQuery("SELECT COUNT(*) FROM Offre o WHERE o.entreprise.id = :pidEntreprise", Long.class)
			.setParameter("pidEntreprise", idEntreprise).getSingleResult();	
	return count;
}
	

	
@Override
public void bookmark(DtoOffre dtoOffreBookmark, DtoCoopteurProfil dtoCoopteurProfil) {
	
	CoopteurProfil entity = entityManager.find(CoopteurProfil.class, dtoCoopteurProfil.getId());
	List<Offre> bookmark = entity.getOffresEnregistrees();
	int position = -1;
	//je parcours la liste des offres favorites
	//Si lors de mon parcours l'id de l'offre favorite correspond à l'id de l'offre dto 
	//que je souhaite mettre en fav, alors je récupère la position de l'offre dans la liste. 
	for (int i = 0 ; i < bookmark.size() ; i++) {
		if (bookmark.get(i).getId() == dtoOffreBookmark.getId()) {
			position = i;
		}
	}
	//Si je ne trouve pas l'offre, je l'ajoute à la liste, si je la trouve je la supprime
	//sinon je l'ajoute puis je mets à jour la liste des entités offre
	if (position == -1) {
		Offre off = entityManager.find(Offre.class, dtoOffreBookmark.getId());
		entity.getOffresEnregistrees().add(off);
	} else {
		entity.getOffresEnregistrees().remove(position);
	}
	entityManager.merge(entity);
}

	@Override
	public List<DtoOffre> GetByEntreprise(int idEntreprise) {
		List<Offre> offres = entityManager.createQuery("SELECT o FROM Offre o "
				+ "WHERE o.entreprise.id = :pid", Offre.class)
				.setParameter("pid", idEntreprise)
				.getResultList();
		List<DtoOffre> result = new ArrayList<>();
		for (Offre offre : offres) {
			result.add(EntityUtils.transformEntityToDto(offre));
		}
		return result;
	}

}

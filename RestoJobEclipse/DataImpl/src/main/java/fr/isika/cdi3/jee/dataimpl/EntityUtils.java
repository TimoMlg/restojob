package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;

import fr.isika.cdi3.jee.dto.DtoAdresse;
import fr.isika.cdi3.jee.dto.DtoCivilite;
import fr.isika.cdi3.jee.dto.DtoCodePostal;
import fr.isika.cdi3.jee.dto.DtoCompetence;
import fr.isika.cdi3.jee.dto.DtoCompteBancaire;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoEntretien;
import fr.isika.cdi3.jee.dto.DtoEtatCompte;
import fr.isika.cdi3.jee.dto.DtoEtatSuiviCooptation;
import fr.isika.cdi3.jee.dto.DtoExperience;
import fr.isika.cdi3.jee.dto.DtoImage;
import fr.isika.cdi3.jee.dto.DtoNiveau;
import fr.isika.cdi3.jee.dto.DtoOffre;
import fr.isika.cdi3.jee.dto.DtoPays;
import fr.isika.cdi3.jee.dto.DtoPersonne;
import fr.isika.cdi3.jee.dto.DtoPoste;
import fr.isika.cdi3.jee.dto.DtoSalaire;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;
import fr.isika.cdi3.jee.dto.DtoTag;
import fr.isika.cdi3.jee.dto.DtoTempsTravail;
import fr.isika.cdi3.jee.dto.DtoTypeContrat;
import fr.isika.cdi3.jee.dto.DtoTypeEtablissement;
import fr.isika.cdi3.jee.dto.DtoTypeMetier;
import fr.isika.cdi3.jee.dto.DtoVille;
import fr.isika.cdi3.jee.dto.DtoVoie;
import fr.isika.cdi3.jee.entity.Adresse;
import fr.isika.cdi3.jee.entity.Civilite;
import fr.isika.cdi3.jee.entity.CodePostal;
import fr.isika.cdi3.jee.entity.Competence;
import fr.isika.cdi3.jee.entity.CompteBancaire;
import fr.isika.cdi3.jee.entity.CoopteurProfil;
import fr.isika.cdi3.jee.entity.Entreprise;
import fr.isika.cdi3.jee.entity.Entretien;
import fr.isika.cdi3.jee.entity.EtatCompte;
import fr.isika.cdi3.jee.entity.EtatSuiviCooptation;
import fr.isika.cdi3.jee.entity.Experience;
import fr.isika.cdi3.jee.entity.Image;
import fr.isika.cdi3.jee.entity.Niveau;
import fr.isika.cdi3.jee.entity.Offre;
import fr.isika.cdi3.jee.entity.Pays;
import fr.isika.cdi3.jee.entity.Personne;
import fr.isika.cdi3.jee.entity.Poste;
import fr.isika.cdi3.jee.entity.Salaire;
import fr.isika.cdi3.jee.entity.SuiviCooptation;
import fr.isika.cdi3.jee.entity.Tag;
import fr.isika.cdi3.jee.entity.TempsTravail;
import fr.isika.cdi3.jee.entity.TypeContrat;
import fr.isika.cdi3.jee.entity.TypeEtablissement;
import fr.isika.cdi3.jee.entity.TypeMetier;
import fr.isika.cdi3.jee.entity.Ville;
import fr.isika.cdi3.jee.entity.Voie;

public class EntityUtils {
  // Transformation d'une personne
  public static DtoPersonne transformEntityToDto(Personne pers) {
    DtoPersonne personneDto = new DtoPersonne();
    personneDto.setId(pers.getId());
    personneDto.setMail(pers.getMail());
    personneDto.setMotPasse(pers.getMotPasse());
    personneDto.setCivilite(transformEntityToDto(pers.getCivilite()));
    personneDto.setFonction(pers.getFonction());
    personneDto.setNumeroTelephone(pers.getNumeroTelephone());
    personneDto.setNom(pers.getNom());
    personneDto.setPrenom(pers.getPrenom());
    personneDto.setProfilLinkedIn(pers.getProfilLinkedIn());
    return personneDto;
  }

  public static Personne transformDtoToEntity(DtoPersonne dtoPersonne) {
    Personne personne = new Personne();
    personne.setId(dtoPersonne.getId());
    personne.setMail(dtoPersonne.getMail());
    personne.setMotPasse(dtoPersonne.getMotPasse());
    personne.setCivilite(transformDtoToEntity(dtoPersonne.getCivilite()));
    personne.setFonction(dtoPersonne.getFonction());
    personne.setNumeroTelephone(dtoPersonne.getNumeroTelephone());
    personne.setNom(dtoPersonne.getNom());
    personne.setPrenom(dtoPersonne.getPrenom());
    personne.setProfilLinkedIn(dtoPersonne.getProfilLinkedIn());
    return personne;
  }

  // Transformation d'une Civilité
  public static DtoCivilite transformEntityToDto(Civilite civilite) {
    if (civilite != null) {
      DtoCivilite dtoCivilite = new DtoCivilite();
      dtoCivilite.setLibelle(civilite.getLibelle());
      dtoCivilite.setId(civilite.getId());
      return dtoCivilite;
    }
    return null;
  }

  public static Civilite transformDtoToEntity(DtoCivilite civilite) {
    Civilite civ = new Civilite();
    civ.setLibelle(civilite.getLibelle());
    civ.setId(civilite.getId());
    return civ;
  }

  // Transformation d'un Coopteur Profil
  public static DtoCoopteurProfil transformEntityToDto(CoopteurProfil coopteurProfil) {
    if (coopteurProfil != null) {
      DtoCoopteurProfil dtoCoopteurProfil = new DtoCoopteurProfil();
      dtoCoopteurProfil.setId(coopteurProfil.getId());
      dtoCoopteurProfil.setMail(coopteurProfil.getMail());
      dtoCoopteurProfil.setMotPasse(coopteurProfil.getMotPasse());
      dtoCoopteurProfil.setCivilite(transformEntityToDto(coopteurProfil.getCivilite()));
      dtoCoopteurProfil.setFonction(coopteurProfil.getFonction());
      dtoCoopteurProfil.setNumeroTelephone(coopteurProfil.getNumeroTelephone());
      dtoCoopteurProfil.setNom(coopteurProfil.getNom());
      dtoCoopteurProfil.setPrenom(coopteurProfil.getPrenom());
      dtoCoopteurProfil.setProfilLinkedIn(coopteurProfil.getProfilLinkedIn());
      dtoCoopteurProfil.setDateNaissance(coopteurProfil.getDateNaissance());
      dtoCoopteurProfil.setPhoto(transformEntityToDto(coopteurProfil.getPhoto()));
      dtoCoopteurProfil.setPresentation(coopteurProfil.getPresentation());
      dtoCoopteurProfil.setVeutEtreRecommande(coopteurProfil.isVeutEtreRecommande());
      dtoCoopteurProfil.setCompteBancaire(transformEntityToDto(coopteurProfil.getCompteBancaire()));
      dtoCoopteurProfil.setEtatCompte(transformEntityToDto(coopteurProfil.getEtatCompte()));
      dtoCoopteurProfil.setAdresse(transformEntityToDto(coopteurProfil.getAdresse()));
      dtoCoopteurProfil.setPretentionsSalariales(transformEntityToDto(coopteurProfil.getPretentionsSalariales()));
      dtoCoopteurProfil.setExperience(transformEntityToDto(coopteurProfil.getExperience()));
      return dtoCoopteurProfil;
    }
    return null;
  }

  public static CoopteurProfil transformDtoToEntity(DtoCoopteurProfil dtoCoopteurProfil) {
    CoopteurProfil coopteurProfil = new CoopteurProfil();
    coopteurProfil.setId(dtoCoopteurProfil.getId());
    coopteurProfil.setMail(dtoCoopteurProfil.getMail());
    coopteurProfil.setMotPasse(dtoCoopteurProfil.getMotPasse());
    coopteurProfil.setCivilite(transformDtoToEntity(dtoCoopteurProfil.getCivilite()));
    coopteurProfil.setFonction(dtoCoopteurProfil.getFonction());
    coopteurProfil.setNumeroTelephone(dtoCoopteurProfil.getNumeroTelephone());
    coopteurProfil.setNom(dtoCoopteurProfil.getNom());
    coopteurProfil.setPrenom(dtoCoopteurProfil.getPrenom());
    coopteurProfil.setProfilLinkedIn(dtoCoopteurProfil.getProfilLinkedIn());
    coopteurProfil.setDateNaissance(dtoCoopteurProfil.getDateNaissance());
    coopteurProfil.setPhoto(transformDtoToEntity(dtoCoopteurProfil.getPhoto()));
    coopteurProfil.setPresentation(dtoCoopteurProfil.getPresentation());
    coopteurProfil.setVeutEtreRecommande(dtoCoopteurProfil.isVeutEtreRecommande());
    coopteurProfil.setCompteBancaire(transformDtoToEntity(dtoCoopteurProfil.getCompteBancaire()));
    coopteurProfil.setEtatCompte(transformDtoToEntity(dtoCoopteurProfil.getEtatCompte()));
    coopteurProfil.setAdresse(transformDtoToEntity(dtoCoopteurProfil.getAdresse()));
    coopteurProfil.setPretentionsSalariales(transformDtoToEntity(dtoCoopteurProfil.getPretentionsSalariales()));
    coopteurProfil.setExperience(transformDtoToEntity(dtoCoopteurProfil.getExperience()));
    return coopteurProfil;
  }

  /// Transformation d'une Image
  public static DtoImage transformEntityToDto(Image photo) {
    if (photo != null) {
      DtoImage dtoImage = new DtoImage();
      dtoImage.setId(photo.getId());
      dtoImage.setUrl(photo.getUrl());
      return dtoImage;
    }
    return null;

  }

  public static Image transformDtoToEntity(DtoImage photo) {
    if (photo != null) {
      Image image = new Image();
      image.setId(photo.getId());
      image.setUrl(photo.getUrl());
      return image;
    }
    return null;
  }

  // Transformation d'un Salaire
  public static DtoSalaire transformEntityToDto(Salaire pretentionsSalariales) {
    if (pretentionsSalariales != null) {
      DtoSalaire dtoSalaire = new DtoSalaire();
      dtoSalaire.setId(pretentionsSalariales.getId());
      dtoSalaire.setSalaireMax(pretentionsSalariales.getSalaireMax());
      dtoSalaire.setSalaireMin(pretentionsSalariales.getSalaireMin());
      return dtoSalaire;
    }
    return null;
  }

  public static Salaire transformDtoToEntity(DtoSalaire pretentionsSalariales) {
    if (pretentionsSalariales != null) {
      Salaire entity = new Salaire();
      entity.setId(pretentionsSalariales.getId());
      entity.setSalaireMax(pretentionsSalariales.getSalaireMax());
      entity.setSalaireMin(pretentionsSalariales.getSalaireMin());
      return entity;
    }
    return null;
  }

  // Transformation d'une Adresse
  public static DtoAdresse transformEntityToDto(Adresse adresse) {
    if (adresse != null) {
      DtoAdresse dtoAdresse = new DtoAdresse();
      dtoAdresse.setId(adresse.getId());
      dtoAdresse.setNomVoie(adresse.getNomVoie());
      dtoAdresse.setVoie(transformEntityToDto(adresse.getVoie()));
      dtoAdresse.setNumero(adresse.getNumero());
      dtoAdresse.setCodePostal(transformEntityToDto(adresse.getCodePostal()));
      dtoAdresse.setVille(transformEntityToDto(adresse.getVille()));
      return dtoAdresse;
    }
    return null;
  }

  public static Adresse transformDtoToEntity(DtoAdresse adresse) {
    if (adresse != null) {
      Adresse adr = new Adresse();
      adr.setId(adresse.getId());
      adr.setNomVoie(adresse.getNomVoie());
      adr.setVoie(transformDtoToEntity(adresse.getVoie()));
      adr.setNumero(adresse.getNumero());
      adr.setCodePostal(transformDtoToEntity(adresse.getCodePostal()));
      adr.setVille(transformDtoToEntity(adresse.getVille()));
      return adr;
    }
    return null;
  }

  // Transformation d'un Code Postal
  public static DtoCodePostal transformEntityToDto(CodePostal codePostal) {
    DtoCodePostal dtoCodePostal = new DtoCodePostal();
    dtoCodePostal.setId(codePostal.getId());
    dtoCodePostal.setLibelle(codePostal.getLibelle());
    return dtoCodePostal;
  }

  public static CodePostal transformDtoToEntity(DtoCodePostal codePostal) {
    if (codePostal != null) {
      CodePostal cp = new CodePostal();
      cp.setId(codePostal.getId());
      cp.setLibelle(codePostal.getLibelle());
      return cp;
    }
    return null;
  }

  // Transformation d'une Ville
  public static DtoVille transformEntityToDto(Ville ville) {
    DtoVille dtoVille = new DtoVille();
    dtoVille.setId(ville.getId());
    dtoVille.setLibelle(ville.getLibelle());
    dtoVille.setPays(transformDtoToEntity(ville.getPays()));
    return dtoVille;
  }

  public static Ville transformDtoToEntity(DtoVille ville) {
    if (ville != null) {
      Ville vil = new Ville();
      vil.setId(ville.getId());
      vil.setLibelle(ville.getLibelle());
      vil.setPays(transformDtoToEntity(ville.getPays()));
      return vil;
    }
    return null;
  }

  public static DtoPays transformDtoToEntity(Pays pays) {
    DtoPays p = new DtoPays();
    p.setId(pays.getId());
    p.setLibelle(pays.getLibelle());
    return p;
  }

  public static Pays transformDtoToEntity(DtoPays pays) {
    Pays p = new Pays();
    p.setId(pays.getId());
    p.setLibelle(pays.getLibelle());
    return p;
  }

  // Transformation d'un Etat de Compte
  public static DtoEtatCompte transformEntityToDto(EtatCompte etatCompte) {
    DtoEtatCompte dtoEtatCompte = new DtoEtatCompte();
    dtoEtatCompte.setId(etatCompte.getId());
    dtoEtatCompte.setLibelle(etatCompte.getLibelle());
    return dtoEtatCompte;
  }

  public static EtatCompte transformDtoToEntity(DtoEtatCompte etatCompte) {
    if (etatCompte != null) {
      EtatCompte etatCmpte = new EtatCompte();
      etatCmpte.setId(etatCompte.getId());
      etatCmpte.setLibelle(etatCompte.getLibelle());
      return etatCmpte;
    }
    return null;
  }

  // Transformation d'un Compte Bancaire
  public static DtoCompteBancaire transformEntityToDto(CompteBancaire compteBancaire) {
    if (compteBancaire != null) {
      DtoCompteBancaire dtoCompteBancaire = new DtoCompteBancaire();
      dtoCompteBancaire.setBIC(compteBancaire.getBIC());
      dtoCompteBancaire.setIBAN(compteBancaire.getIBAN());
      dtoCompteBancaire.setId(compteBancaire.getId());
      dtoCompteBancaire.setTitulaire(compteBancaire.getTitulaire());
      return dtoCompteBancaire;
    }
    return null;
  }

  public static CompteBancaire transformDtoToEntity(DtoCompteBancaire compteBancaire) {
    if (compteBancaire != null) {
      CompteBancaire dtoCompteBancaire = new CompteBancaire();
      dtoCompteBancaire.setBIC(compteBancaire.getBIC());
      dtoCompteBancaire.setIBAN(compteBancaire.getIBAN());
      dtoCompteBancaire.setId(compteBancaire.getId());
      dtoCompteBancaire.setTitulaire(compteBancaire.getTitulaire());
      return dtoCompteBancaire;
    }
    return null;
  }

  // Transformation d'une Voie
  public static Voie transformDtoToEntity(DtoVoie voie) {
    if (voie != null) {
      Voie voi = new Voie();
      voi.setType(voie.getType());
      voi.setId(voie.getId());
      return voi;
    }
    return null;
  }

  public static DtoVoie transformEntityToDto(Voie voie) {
    DtoVoie v = new DtoVoie();
    v.setType(voie.getType());
    v.setId(voie.getId());
    return v;
  }

  // Transformation d'un Poste
  public static DtoPoste transformEntityToDto(Poste poste) {
    if (poste != null) {
      DtoPoste dtoposte = new DtoPoste();
      dtoposte.setLibelle(poste.getLibelle());
      dtoposte.setId(poste.getId());
      dtoposte.setTypeMetier(transformEntityToDto(poste.getTypeMetier()));
      if (poste.getImages() != null) {
        dtoposte.setImages(new ArrayList<>());
        for (Image omg : poste.getImages()) {
          dtoposte.getImages().add(transformEntityToDto(omg));
        }
      }
      return dtoposte;
    }
    return null;
  }

  public static Poste transformDtoToEntity(DtoPoste poste) {
    if (poste != null) {
      Poste dtoposte = new Poste();
      dtoposte.setLibelle(poste.getLibelle());
      dtoposte.setId(poste.getId());
      dtoposte.setTypeMetier(transformDtoToEntity(poste.getTypeMetier()));
      return dtoposte;
    }
    return null;
  }

  public static TypeMetier transformDtoToEntity(DtoTypeMetier typeMetier) {
    TypeMetier entity = new TypeMetier();
    entity.setLibelle(typeMetier.getLibelle());
    entity.setId(typeMetier.getId());
    return entity;
  }

  public static DtoTypeMetier transformEntityToDto(TypeMetier typeMetier) {
    DtoTypeMetier dto = new DtoTypeMetier();
    dto.setLibelle(typeMetier.getLibelle());
    dto.setId(typeMetier.getId());
    return dto;
  }

  // Transformation d'un Type de Contrat
  public static DtoTypeContrat transformEntityToDto(TypeContrat typeContrat) {
    DtoTypeContrat dtotypecontrat = new DtoTypeContrat();
    dtotypecontrat.setLibelle(typeContrat.getLibelle());
    dtotypecontrat.setId(typeContrat.getId());
    return dtotypecontrat;
  }

  public static TypeContrat transformDtoToEntity(DtoTypeContrat dtypeContrat) {
    TypeContrat totc = new TypeContrat();
    totc.setLibelle(dtypeContrat.getLibelle());
    totc.setId(dtypeContrat.getId());
    return totc;
  }

  // Transformation d'une Entreprise
  public static DtoEntreprise transformEntityToDto(Entreprise entreprise) {
    DtoEntreprise dtoentreprise = new DtoEntreprise();
    dtoentreprise.setId(entreprise.getId());
    dtoentreprise.setMail(entreprise.getMail());
    dtoentreprise.setMotPasse(entreprise.getMotPasse());
    dtoentreprise.setCivilite(transformEntityToDto(entreprise.getCivilite()));
    dtoentreprise.setFonction(entreprise.getFonction());
    dtoentreprise.setNumeroTelephone(entreprise.getNumeroTelephone());
    dtoentreprise.setNom(entreprise.getNom());
    dtoentreprise.setPrenom(entreprise.getPrenom());
    dtoentreprise.setProfilLinkedIn(entreprise.getProfilLinkedIn());
    dtoentreprise.setNomEntreprise(entreprise.getNomEntreprise());
    dtoentreprise.setAdresseSiege(transformEntityToDto(entreprise.getAdresseSiege()));
    dtoentreprise.setCompteBancaire(transformEntityToDto(entreprise.getCompteBancaire()));
    dtoentreprise.setDescription(entreprise.getDescription());
    dtoentreprise.setEtatCompte(transformEntityToDto(entreprise.getEtatCompte()));
    dtoentreprise.setId(entreprise.getId());
    dtoentreprise.setLogo(transformEntityToDto(entreprise.getLogo()));
    dtoentreprise.setNombreSalaries(entreprise.getNombreSalaries());
    dtoentreprise.setNumeroSiret(entreprise.getNumeroSiret());
    return dtoentreprise;
  }

  public static Entreprise transformDtoToEntity(DtoEntreprise entreprise) {
    Entreprise entity = new Entreprise();
    entity.setId(entreprise.getId());
    entity.setMail(entreprise.getMail());
    entity.setMotPasse(entreprise.getMotPasse());
    entity.setCivilite(transformDtoToEntity(entreprise.getCivilite()));
    entity.setFonction(entreprise.getFonction());
    entity.setNumeroTelephone(entreprise.getNumeroTelephone());
    entity.setNomEntreprise(entreprise.getNomEntreprise());
    entity.setPrenom(entreprise.getPrenom());
    entity.setProfilLinkedIn(entreprise.getProfilLinkedIn());
    entity.setNom(entreprise.getNom());
    entity.setAdresseSiege(transformDtoToEntity(entreprise.getAdresseSiege()));
    entity.setCompteBancaire(transformDtoToEntity(entreprise.getCompteBancaire()));
    entity.setDescription(entreprise.getDescription());
    entity.setEtatCompte(transformDtoToEntity(entreprise.getEtatCompte()));
    entity.setId(entreprise.getId());
    entity.setLogo(transformDtoToEntity(entreprise.getLogo()));
    entity.setNombreSalaries(entreprise.getNombreSalaries());
    entity.setNumeroSiret(entreprise.getNumeroSiret());
    return entity;
  }

  // Transformation d'une Competence
  public static DtoCompetence transformEntityToDto(Competence entity) {
    DtoCompetence dtocompetences = new DtoCompetence();
    dtocompetences.setId(entity.getId());
    dtocompetences.setLibelle(entity.getLibelle());
    if (entity.getCompetenceParent() != null) {
      dtocompetences.setCompetenceParent(transformEntityToDto(entity.getCompetenceParent()));
    }
    return dtocompetences;
  }

  public static Competence transformDtoToEntity(DtoCompetence dto) {
    Competence entity = new Competence();
    entity.setId(dto.getId());
    entity.setLibelle(dto.getLibelle());
    if (entity.getCompetenceParent() != null) {
      entity.setCompetenceParent(transformDtoToEntity(dto.getCompetenceParent()));
    }
    return entity;
  }

  // Transformation d'un Tag
  public static DtoTag transformEntityToDto(Tag entity) {
    DtoTag dto = new DtoTag();
    dto.setId(entity.getId());
    dto.setLibelle(entity.getLibelle());
    if (entity.getTagParent() != null) {
      dto.setTagParent(transformEntityToDto(entity.getTagParent()));
    }
    return dto;
  }

  public static Tag transformDtoToEntity(DtoTag dto) {
    Tag entity = new Tag();
    entity.setId(dto.getId());
    entity.setLibelle(dto.getLibelle());
    if (entity.getTagParent() != null) {
      dto.setTagParent(transformEntityToDto(entity.getTagParent()));
    }
    return entity;
  }

  public static DtoSuiviCooptation transformEntityToDto(SuiviCooptation sc) {
    DtoSuiviCooptation dto = new DtoSuiviCooptation();
    dto.setId(sc.getId());
    dto.setDateSelection(sc.getDateSelection());
    dto.setDateCreation(sc.getDateCreation());
    dto.setDateRecrutement(sc.getDateRecrutement());
    dto.setDateFinPeriodeEssai(sc.getDateFinPeriodeEssai());
    dto.setEtatCooptation(transformEntityToDto(sc.getEtatCooptation()));
    dto.setCoopteur(transformEntityToDto(sc.getCoopteur()));
    dto.setProfil(transformEntityToDto(sc.getProfil()));
    dto.setOffre(transformEntityToDto(sc.getOffre()));

    // List<DtoEntretien> entretiens = new ArrayList<DtoEntretien>();
    // if(sc.getEntretiens()!=null)
    // {
    // for (Entretien e : sc.getEntretiens()) {
    // DtoEntretien entretien = transformEntityToDto(e);
    // entretiens.add(entretien);
    // }
    // dto.setEntretiens(entretiens);
    // }

    return dto;
  }

  public static SuiviCooptation transformDtoToEntity(DtoSuiviCooptation dtosuiviCooptation) {
    SuiviCooptation sc = new SuiviCooptation();
    sc.setId(dtosuiviCooptation.getId());
    sc.setDateSelection(dtosuiviCooptation.getDateSelection());
    sc.setDateCreation(dtosuiviCooptation.getDateCreation());
    sc.setDateRecrutement(dtosuiviCooptation.getDateRecrutement());
    sc.setDateFinPeriodeEssai(dtosuiviCooptation.getDateFinPeriodeEssai());
    sc.setEtatCooptation(transformDtoToEntity(dtosuiviCooptation.getEtatCooptation()));
    sc.setCoopteur(transformDtoToEntity(dtosuiviCooptation.getCoopteur()));
    sc.setProfil(transformDtoToEntity(dtosuiviCooptation.getProfil()));
    sc.setOffre(transformDtoToEntity(dtosuiviCooptation.getOffre()));
    return sc;
  }

  public static DtoEntretien transformEntityToDto(Entretien e) {
    DtoEntretien dtoEntretien = new DtoEntretien();

    dtoEntretien.setId(e.getId());
    dtoEntretien.setDateEntretien(e.getDateEntretien());
    dtoEntretien.setSuiviCooptation(transformEntityToDto(e.getSuiviCooptation()));
    return dtoEntretien;

  }

  public static Entretien transformDtoToEntity(DtoEntretien dtoEntretien) {
    Entretien e = new Entretien();
    e.setId(dtoEntretien.getId());
    e.setDateEntretien(dtoEntretien.getDateEntretien());
    e.setSuiviCooptation(transformDtoToEntity(dtoEntretien.getSuiviCooptation()));
    return e;
  }

  public static DtoEtatSuiviCooptation transformEntityToDto(EtatSuiviCooptation etatCooptation) {
    DtoEtatSuiviCooptation dto = new DtoEtatSuiviCooptation();
    dto.setId(etatCooptation.getId());
    dto.setLibelle(etatCooptation.getLibelle());
    return dto;
  }

  public static EtatSuiviCooptation transformDtoToEntity(DtoEtatSuiviCooptation dto) {
    EtatSuiviCooptation etatSuiviCooptation = new EtatSuiviCooptation();
    etatSuiviCooptation.setId(dto.getId());
    etatSuiviCooptation.setLibelle(dto.getLibelle());
    return etatSuiviCooptation;
  }

  public static DtoExperience transformEntityToDto(Experience experience) {
    if (experience != null) {
      DtoExperience dto = new DtoExperience();
      dto.setAnneeMax(experience.getAnneeMax());
      dto.setAnneeMin(experience.getAnneeMin());
      dto.setId(experience.getId());
      return dto;
    }
    return null;
  }

  public static Experience transformDtoToEntity(DtoExperience dtoExperience) {
    if (dtoExperience != null) {
      Experience experience = new Experience();
      experience.setAnneeMax(dtoExperience.getAnneeMax());
      experience.setAnneeMin(dtoExperience.getAnneeMin());
      experience.setId(dtoExperience.getId());
      return experience;
    }
    return null;
  }

  public static DtoOffre transformEntityToDto(Offre offre) {
    DtoOffre dto = new DtoOffre();
    if (offre != null) {
      dto.setDescritption(offre.getDescritption());
      dto.setEntreprise(transformEntityToDto(offre.getEntreprise()));
      dto.setExperience(transformEntityToDto(offre.getExperience()));
      dto.setId(offre.getId());
      dto.setOuverturePoste(offre.getOuverturePoste());
      dto.setPoste(transformEntityToDto(offre.getPoste()));
      dto.setPublication(offre.getPublication());
      dto.setSalaire(transformEntityToDto(offre.getSalaire()));
      dto.setTypeContrat(transformEntityToDto(offre.getTypeContrat()));
      dto.setTempsTravail(transformEntityToDto(offre.getTempsTravail()));
      dto.setImage(dto.chooseImg());
      if (offre.getCompetences() != null) {
        dto.setCompetences((new ArrayList<>()));
        for (Competence c : offre.getCompetences()) {
          dto.getCompetences().add(transformEntityToDto(c));
        }
      }
      return dto;
    }
    return null;
  }

  public static Offre transformDtoToEntity(DtoOffre dtoOffre) {
    Offre offre = new Offre();
    offre.setDescritption(dtoOffre.getDescription());
    offre.setEntreprise(transformDtoToEntity(dtoOffre.getEntreprise()));
    offre.setExperience(transformDtoToEntity(dtoOffre.getExperience()));
    offre.setId(dtoOffre.getId());
    offre.setOuverturePoste(dtoOffre.getOuverturePoste());
    offre.setPoste(transformDtoToEntity(dtoOffre.getPoste()));
    offre.setPublication(dtoOffre.getPublication());
    offre.setSalaire(transformDtoToEntity(dtoOffre.getSalaire()));
    offre.setTypeContrat(transformDtoToEntity(dtoOffre.getTypeContrat()));
    offre.setTempsTravail(transformDtoToEntity(dtoOffre.getTempsTravail()));

    return offre;
  }

  public static DtoTempsTravail transformEntityToDto(TempsTravail tempsTravail) {
    DtoTempsTravail dtott = new DtoTempsTravail();
    dtott.setId(tempsTravail.getId());
    dtott.setLibelle(tempsTravail.getLibelle());
    return dtott;
  }

  public static TempsTravail transformDtoToEntity(DtoTempsTravail dtotpsTravail) {
    TempsTravail tt = new TempsTravail();
    tt.setId(dtotpsTravail.getId());
    tt.setLibelle(dtotpsTravail.getLibelle());
    return tt;
  }

  public static DtoTypeEtablissement transformEntityToDto(TypeEtablissement entity) {
    DtoTypeEtablissement dto = new DtoTypeEtablissement();
    dto.setId(entity.getId());
    dto.setLibelle(entity.getLibelle());
    return dto;
  }

  public static DtoNiveau transformEntityToDto(Niveau niveau) {
    DtoNiveau n = new DtoNiveau();
    n.setCompetence(transformEntityToDto(niveau.getCompetence()));
    n.setCoopteurProfil(transformEntityToDto(niveau.getCoopteurProfil()));
    n.setEtoiles(niveau.getEtoiles());
    n.setId(niveau.getId());
    return n;
  }

  public static Niveau transformDtoToEntity(DtoNiveau niveau) {
    Niveau n = new Niveau();
    n.setCompetence(transformDtoToEntity(niveau.getCompetence()));
    n.setCoopteurProfil(transformDtoToEntity(niveau.getCoopteurProfil()));
    n.setEtoiles(niveau.getEtoiles());
    n.setId(niveau.getId());
    return n;
  }

}

package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.ICiviliteDao;
import fr.isika.cdi3.jee.dto.DtoCivilite;
import fr.isika.cdi3.jee.entity.Civilite;

@Remote(ICiviliteDao.class)
@Stateless
public class CiviliteDao extends Dao implements ICiviliteDao {

  @Override
  public List<DtoCivilite> getAll() {
    List<Civilite> civilites = entityManager.createQuery("SELECT c FROM Civilite c",
        Civilite.class).getResultList();
    List<DtoCivilite> result = new ArrayList<>();
    for (Civilite civilite : civilites) {
      result.add(EntityUtils.transformEntityToDto(civilite));
    }
    return result;
  }

}

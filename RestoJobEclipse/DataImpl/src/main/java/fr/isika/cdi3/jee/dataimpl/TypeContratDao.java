package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.ITypeContratDao;
import fr.isika.cdi3.jee.dto.DtoTypeContrat;
import fr.isika.cdi3.jee.entity.TypeContrat;

@Remote(ITypeContratDao.class)
@Stateless
public class TypeContratDao extends Dao implements ITypeContratDao {

  @Override
  public List<DtoTypeContrat> getAll() {
    List<TypeContrat> tclist = entityManager.createQuery("SELECT tc FROM TypeContrat tc", TypeContrat.class)
        .getResultList();
    List<DtoTypeContrat> result = new ArrayList<>();
    for (TypeContrat typeContrat : tclist) {
      result.add(EntityUtils.transformEntityToDto(typeContrat));
    }
    return result;
  }

@Override
public DtoTypeContrat getById(int idTC) {
	TypeContrat typeContrat=entityManager.createQuery("SELECT tc FROM TypeContrat tc WHERE id like :paramId", TypeContrat.class)
			.setParameter("paramId", idTC).getSingleResult();
	return EntityUtils.transformEntityToDto(typeContrat);
}

}

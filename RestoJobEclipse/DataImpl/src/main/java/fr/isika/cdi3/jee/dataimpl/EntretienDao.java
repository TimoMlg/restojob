package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.IEntretienDao;
import fr.isika.cdi3.jee.dto.DtoEntretien;
import fr.isika.cdi3.jee.entity.Entretien;

@Remote(IEntretienDao.class)
@Stateless
public class EntretienDao extends Dao implements IEntretienDao {

	@Override
	public List<DtoEntretien> datesEntretien(int idSuiviCooptation) {
		List<Entretien> entretiens = entityManager
				.createQuery("SELECT e FROM Entretien e WHERE e.suiviCooptation.id = ?1",
						Entretien.class)
				.setParameter(1, idSuiviCooptation).getResultList();
		List<DtoEntretien> dtoEntretiens = new ArrayList<DtoEntretien>();
		for (Entretien e : entretiens) {
			dtoEntretiens.add(EntityUtils.transformEntityToDto(e));
		}
		return dtoEntretiens;
	}

}

package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.ITypeEtablissementDao;
import fr.isika.cdi3.jee.dto.DtoTypeEtablissement;
import fr.isika.cdi3.jee.entity.TypeEtablissement;

@Remote(ITypeEtablissementDao.class)
@Stateless
public class TypeEtablissementDao extends Dao implements ITypeEtablissementDao {

  @Override
  public List<DtoTypeEtablissement> getAll() {
    List<TypeEtablissement> telist = entityManager.createQuery("SELECT te FROM TypeEtablissement te",
        TypeEtablissement.class).getResultList();
    List<DtoTypeEtablissement> result = new ArrayList<>();
    for (TypeEtablissement te : telist) {
      result.add(EntityUtils.transformEntityToDto(te));
    }
    return result;
  }

}

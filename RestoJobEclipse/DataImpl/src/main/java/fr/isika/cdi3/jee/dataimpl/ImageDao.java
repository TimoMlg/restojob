package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.IImageDao;
import fr.isika.cdi3.jee.dto.DtoImage;
import fr.isika.cdi3.jee.entity.Image;

@Remote(IImageDao.class)
@Stateless
public class ImageDao extends Dao implements IImageDao {

  @Override
  public List<DtoImage> getByPoste(int idPoste) {
    List<Image> imgs = entityManager.createQuery("SELECT img FROM Image img WHERE img.poste.id = :pposte", Image.class)
        .setParameter("pposte", idPoste).getResultList();
    List<DtoImage> result = new ArrayList<>();
    for (Image img : imgs) {
      result.add(EntityUtils.transformEntityToDto(img));
    }
    return result;
  }

  @Override
  public DtoImage getById(int id) {
    Image img = entityManager.createQuery("SELECT img FROM Image img WHERE img.id = :pimg", Image.class)
        .setParameter("pimg", id).getSingleResult();
    return EntityUtils.transformEntityToDto(img);
  }

}

package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.ITempsTravailDao;
import fr.isika.cdi3.jee.dto.DtoTempsTravail;
import fr.isika.cdi3.jee.entity.TempsTravail;

@Remote(ITempsTravailDao.class)
@Stateless
public class TempsTravailDao extends Dao implements ITempsTravailDao{

	@Override
	public List<DtoTempsTravail> getAll() {
		List<TempsTravail> ttlist=entityManager.createQuery("SELECT tt FROM TempsTravail tt",TempsTravail.class).getResultList();
		List<DtoTempsTravail> result=new ArrayList<>();
		for(TempsTravail temps:ttlist) {
			result.add(EntityUtils.transformEntityToDto(temps));
		}
		return result;
	}

	@Override
	public DtoTempsTravail getById(int idTT) {
		TempsTravail tempsTravail=entityManager.createQuery("SELECT tt FROM TempsTravail tt WHERE id like :paramId",TempsTravail.class)
				.setParameter("paramId", idTT).getSingleResult();
		return EntityUtils.transformEntityToDto(tempsTravail);
	}

}

package fr.isika.cdi3.jee.dataimpl;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import fr.isika.cdi3.jee.dataapi.IEntrepriseDao;
import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoPersonne;
import fr.isika.cdi3.jee.entity.Entreprise;


@Stateless
@Remote(IEntrepriseDao.class)
public class EntrepriseDao extends Dao implements IEntrepriseDao {
	
	@Override
	public void creerCompteEntreprise(DtoEntreprise ent, DtoPersonne pers) {
		
		Entreprise entreprise = EntityUtils.transformDtoToEntity(ent);
		entityManager.persist(entreprise.getAdresseSiege().getVille());
		entityManager.persist(entreprise.getAdresseSiege().getVoie());
		entityManager.persist(entreprise.getAdresseSiege().getCodePostal());
		entityManager.persist(entreprise.getAdresseSiege());
		entityManager.persist(entreprise);
	}

	@Override
	public DtoEntreprise getById(int idEntreprise) {
		Entreprise entreprise=entityManager.find(Entreprise.class, idEntreprise);
		return EntityUtils.transformEntityToDto(entreprise);
	}

}

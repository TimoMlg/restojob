package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import fr.isika.cdi3.jee.dataapi.INiveauDao;
import fr.isika.cdi3.jee.dataapi.IPersonneDao;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoNiveau;
import fr.isika.cdi3.jee.dto.DtoPersonne;
import fr.isika.cdi3.jee.entity.CoopteurProfil;
import fr.isika.cdi3.jee.entity.Entreprise;
import fr.isika.cdi3.jee.entity.Niveau;
import fr.isika.cdi3.jee.entity.Personne;

@Remote(IPersonneDao.class)
@Stateless
public class PersonneDao extends Dao implements IPersonneDao {

  @EJB
  private INiveauDao proxyNiv;
  
  public DtoPersonne getConnexion(String mail, String motPasse) throws Exception {
    DtoPersonne dtopersonne = EntityUtils.transformEntityToDto((Personne) entityManager
        .createQuery(
            "SELECT p FROM Personne p WHERE p.mail like :pmail AND p.motPasse like :pmotPasse")
        .setParameter("pmail", mail).setParameter("pmotPasse", motPasse).getSingleResult());

    if (entityManager.find(CoopteurProfil.class, dtopersonne.getId()) != null) {
      DtoCoopteurProfil cp = EntityUtils.transformEntityToDto(
          entityManager.find(CoopteurProfil.class, dtopersonne.getId()));
      List<DtoNiveau> niveaux = proxyNiv.getByIdCoopteurProfil(cp.getId());
      cp.setNiveaux(niveaux);
      dtopersonne = cp;
      return dtopersonne;
    }
    return dtopersonne = EntityUtils
        .transformEntityToDto(entityManager.find(Entreprise.class, dtopersonne.getId()));
  }

  @Override
  public boolean isEnregistre(String mail) {
    return entityManager.createQuery(
        "SELECT CASE WHEN (COUNT(*) >0) THEN true ELSE false end FROM Personne p WHERE p.mail = ?1",
        Boolean.class).setParameter(1, mail).getSingleResult();
  }

  @Override
  public int getIdByMail(String mail) {

    return entityManager
        .createQuery("SELECT p.id FROM Personne p WHERE p.mail = ?1", Integer.class)
        .setParameter(1, mail).getSingleResult();
  }
}

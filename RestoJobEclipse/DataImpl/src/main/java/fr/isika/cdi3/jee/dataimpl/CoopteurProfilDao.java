package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.ICoopteurProfilDao;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoOffre;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;
import fr.isika.cdi3.jee.entity.CoopteurProfil;
import fr.isika.cdi3.jee.entity.Offre;
import fr.isika.cdi3.jee.entity.SuiviCooptation;

@Stateless
@Remote(ICoopteurProfilDao.class)
public class CoopteurProfilDao extends Dao implements ICoopteurProfilDao {

	@Override
	public boolean checkVeutRecommandation(int idProfil) {
		return entityManager
				.createQuery("SELECT cp.VeutEtreRecommande FROM CoopteurProfil cp WHERE cp.id = ?1",
						Boolean.class)
				.setParameter(1, idProfil).getSingleResult();
	}

	@Override
	public boolean isCoopteurProfil(int idProfil) {

		if (entityManager.find(CoopteurProfil.class, idProfil) != null) {
			return true;
		}
		return false;
	}

	@Override
	public void creerCompteParticulier(DtoCoopteurProfil particulier) {

		CoopteurProfil coopteurProfil = EntityUtils.transformDtoToEntity(particulier);
		entityManager.persist(coopteurProfil.getAdresse().getVille());
		entityManager.persist(coopteurProfil.getAdresse().getCodePostal());
		entityManager.persist(coopteurProfil.getAdresse().getVoie());
		entityManager.persist(coopteurProfil.getAdresse());
		entityManager.persist(coopteurProfil);

	}

	@Override
	public DtoCoopteurProfil getById(int idDtoPersonne) {
		return EntityUtils
				.transformEntityToDto(entityManager
						.createQuery("SELECT cp FROM CoopteurProfil cp WHERE cp.id = ?1",
								CoopteurProfil.class)
						.setParameter(1, idDtoPersonne).getSingleResult());
	}

	@Override
	public long recommandationsReussies(int idParticulier) {
		return entityManager.createQuery(
				"SELECT COUNT (*) FROM SuiviCooptation sc WHERE sc.coopteur.id = ?1 AND sc.etatCooptation = 6",
				Long.class).setParameter(1, idParticulier).getSingleResult();
	}

	@Override
	public long personnesRecommandees(int idParticulier) {
		return entityManager.createQuery(
				"SELECT COUNT (*) FROM SuiviCooptation sc WHERE sc.coopteur.id = ?1 AND sc.etatCooptation.id > 1",
				Long.class).setParameter(1, idParticulier).getSingleResult();
	}

	@Override
	public long cooptationsEnCours(int idParticulier) {
		return entityManager.createQuery(
				"SELECT COUNT(*) FROM SuiviCooptation sc WHERE sc.coopteur.id = ?1 AND sc.etatCooptation.id > 1 AND sc.etatCooptation.id < 6",
				Long.class).setParameter(1, idParticulier).getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DtoCoopteurProfil> profilsAssocies(int idParticulier) {
		List<CoopteurProfil> cp = entityManager.createNativeQuery("SELECT * FROM profils_associes "
				+ "INNER JOIN coopteur_profil cp ON profils_associes.id_profil_profils_associes = cp.id_personne "
				+ "INNER JOIN personne p ON cp.id_personne = p.id_personne "
				+ "WHERE id_coopteur_profils_associes = ?1", CoopteurProfil.class)
				.setParameter(1, idParticulier).getResultList();
		List<DtoCoopteurProfil> dtocp = new ArrayList<DtoCoopteurProfil>();
		for (CoopteurProfil c : cp) {
			dtocp.add(EntityUtils.transformEntityToDto(c));
		}
		return dtocp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DtoOffre> listeOffresFav(int idParticulier) {
		List<Offre> liste = entityManager
				.createQuery("SELECT cp.offresEnregistrees FROM CoopteurProfil cp WHERE cp.id = ?1")
				.setParameter(1, idParticulier).getResultList();
		List<DtoOffre> retour = new ArrayList<>();
		for (Offre off : liste) {
			retour.add(EntityUtils.transformEntityToDto(off));
		}
		return retour;
	}

	@Override
	public List<DtoOffre> listeRecommandationsReussies(int idParticulier) {
		List<Offre> offre = entityManager
				.createQuery(
						"SELECT sc.offre FROM SuiviCooptation sc "
								+ "WHERE sc.coopteur.id = ?1 AND sc.etatCooptation.id = 6",
						Offre.class)
				.setParameter(1, idParticulier).getResultList();
		List<DtoOffre> dtoOffre = new ArrayList<DtoOffre>();
		for (Offre o : offre) {
			dtoOffre.add(EntityUtils.transformEntityToDto(o));
		}
		return dtoOffre;
	}

}

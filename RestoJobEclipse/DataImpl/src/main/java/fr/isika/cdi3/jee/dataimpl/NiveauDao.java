package fr.isika.cdi3.jee.dataimpl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.isika.cdi3.jee.dataapi.INiveauDao;
import fr.isika.cdi3.jee.dto.DtoNiveau;
import fr.isika.cdi3.jee.entity.Niveau;

@Remote(INiveauDao.class)
@Stateless
public class NiveauDao extends Dao implements INiveauDao {

  @Override
  public List<DtoNiveau> getByIdCoopteurProfil(int idCP) {
    List<Niveau> niv = entityManager
        .createQuery("SELECT n FROM Niveau n " + "WHERE n.coopteurProfil.id = :pid", Niveau.class)
        .setParameter("pid", idCP).getResultList();
    List<DtoNiveau> niveaux = new ArrayList<>();
    for (Niveau niveau : niv) {
      niveaux.add(EntityUtils.transformEntityToDto(niveau));
    }
    return niveaux;
  }

  @SuppressWarnings("unchecked")
  public List<DtoNiveau> getByIdEntreprise(int idEntreprise) {
    List<Niveau> niveaux = entityManager
        .createQuery("SELECT sc.profil.niveaux FROM SuiviCooptation sc WHERE sc.offre.id = :pid")
        .setParameter("pid", idEntreprise).getResultList();
    List<DtoNiveau> result = new ArrayList<>();
    for (Niveau niveau : niveaux) {
      result.add(EntityUtils.transformEntityToDto(niveau));
    }
    return result;
  }

}

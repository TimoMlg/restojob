package fr.isika.cdi3.jee.dto;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 */
public class DtoSuiviCooptation implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoSuiviCooptation() {
  }

  /**
   *
   */
  private LocalDateTime dateSelection;

  /**
   *
   */
  private LocalDateTime dateRecrutement;

  /**
   *
   */
  private LocalDateTime dateFinPeriodeEssai;

  /**
   *
   */
  private Integer id;

  /**
   *
   */
  private DtoEtatSuiviCooptation etatCooptation;


  /**
   *
   */
  private DtoCoopteurProfil profil;

  /**
   *
   */
  private DtoCoopteurProfil coopteur;

  /**
   *
   */
  private DtoOffre offre;
  /**
   *
   */
  private LocalDateTime dateCreation;
  /**
   *
   */
  private List<DtoEntretien> entretiens;

  public LocalDateTime getDateSelection() {
    return dateSelection;
  }

  public void setDateSelection(LocalDateTime dateSelection) {
    this.dateSelection = dateSelection;
  }

  public LocalDateTime getDateRecrutement() {
    return dateRecrutement;
  }

  public void setDateRecrutement(LocalDateTime dateRecrutement) {
    this.dateRecrutement = dateRecrutement;
  }

  public LocalDateTime getDateFinPeriodeEssai() {
    return dateFinPeriodeEssai;
  }

  public void setDateFinPeriodeEssai(LocalDateTime dateFinPeriodeEssai) {
    this.dateFinPeriodeEssai = dateFinPeriodeEssai;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoEtatSuiviCooptation getEtatCooptation() {
    return etatCooptation;
  }

  public void setEtatCooptation(DtoEtatSuiviCooptation etatCooptation) {
    this.etatCooptation = etatCooptation;
  }

  public DtoCoopteurProfil getProfil() {
    return profil;
  }

  public void setProfil(DtoCoopteurProfil profil) {
    this.profil = profil;
  }

  public DtoCoopteurProfil getCoopteur() {
    return coopteur;
  }

  public void setCoopteur(DtoCoopteurProfil coopteur) {
    this.coopteur = coopteur;
  }

  public DtoOffre getOffre() {
    return offre;
  }

  public void setOffre(DtoOffre offre) {
    this.offre = offre;
  }

  public List<DtoEntretien> getEntretiens() {
    return entretiens;
  }

  public void setEntretiens(List<DtoEntretien> entretiens) {
    this.entretiens = entretiens;
  }

public LocalDateTime getDateCreation() {
	return dateCreation;
}

public void setDateCreation(LocalDateTime dateCreation) {
	this.dateCreation = dateCreation;
}

}
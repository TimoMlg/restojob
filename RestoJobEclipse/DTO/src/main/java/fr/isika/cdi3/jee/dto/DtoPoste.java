package fr.isika.cdi3.jee.dto;
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class DtoPoste implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoPoste() {
  }

  /**
   * 
   */
  private String libelle;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private DtoTypeMetier typeMetier;

  /**
   *
   */
  private List<DtoImage> images;

  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoTypeMetier getTypeMetier() {
    return typeMetier;
  }

  public void setTypeMetier(DtoTypeMetier typeMetier) {
    this.typeMetier = typeMetier;
  }

  public List<DtoImage> getImages() {
    return images;
  }

  public void setImages(List<DtoImage> images) {
    this.images = images;
  }

}
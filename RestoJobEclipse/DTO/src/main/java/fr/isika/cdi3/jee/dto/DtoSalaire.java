package fr.isika.cdi3.jee.dto;
import java.io.Serializable;

/**
 * 
 */
public class DtoSalaire implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoSalaire() {
  }

  /**
   * 
   */
  private Integer salaireMin;

  /**
   * 
   */
  private Integer salaireMax;

  /**
   * 
   */
  private Integer id;
  
public Integer getSalaireMin() {
    return salaireMin;
  }

  public void setSalaireMin(Integer salaireMin) {
    this.salaireMin = salaireMin;
  }

  public Integer getSalaireMax() {
    return salaireMax;
  }

  public void setSalaireMax(Integer salaireMax) {
    this.salaireMax = salaireMax;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

}
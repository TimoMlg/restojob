package fr.isika.cdi3.jee.dto;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

/**
 * 
 */
public class DtoEntretien implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoEntretien() {
  }
  /**
   * 
   */
  private LocalDateTime dateEntretien;

  /**
   * 
   */
  private Integer id;

  /**
   *
   */
  private DtoSuiviCooptation suiviCooptation;


  public LocalDateTime getDateEntretien() {
    return dateEntretien;
  }

  public void setDateEntretien(LocalDateTime dateEntretien) {
    this.dateEntretien = dateEntretien;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoSuiviCooptation getSuiviCooptation() {
    return suiviCooptation;
  }

  public void setSuiviCooptation(DtoSuiviCooptation suiviCooptation) {
    this.suiviCooptation = suiviCooptation;
  }

}
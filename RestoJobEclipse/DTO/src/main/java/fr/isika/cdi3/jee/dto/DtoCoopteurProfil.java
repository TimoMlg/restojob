package fr.isika.cdi3.jee.dto;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 
 */
public class DtoCoopteurProfil extends DtoPersonne implements Serializable {

  public DtoCoopteurProfil(Date dateNaissance, DtoImage photo, String presentation, boolean veutEtreRecommande,
			Integer id, List<DtoCoopteurProfil> profilsAssocies, DtoCompteBancaire compteBancaire,
			DtoEtatCompte etatCompte, DtoAdresse adresse, DtoSalaire pretentionsSalariales, DtoExperience experience, DtoPersonne ancienEmployeur,
			List<DtoSuiviCooptation> suiviCooptation) {
		super();
		this.dateNaissance = dateNaissance;
		this.photo = photo;
		this.presentation = presentation;
		this.veutEtreRecommande = veutEtreRecommande;
		this.id = id;
		this.profilsAssocies = profilsAssocies;
		this.compteBancaire = compteBancaire;
		this.etatCompte = etatCompte;
		this.adresse = adresse;
		this.pretentionsSalariales = pretentionsSalariales;
		this.experience = experience;
		this.ancienEmployeur = ancienEmployeur;
		this.suiviCooptation = suiviCooptation;
	}

/**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoCoopteurProfil() {
  }

  /**
   * 
   */
  private Date dateNaissance;

  /**
   * 
   */
  private DtoImage photo;

  /**
   * 
   */
  private String presentation;

  /**
   * 
   */
  private boolean veutEtreRecommande;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private List<DtoCoopteurProfil> profilsAssocies;

  /**
   * 
   */
  private DtoCompteBancaire compteBancaire;

  /**
   * 
   */
  private DtoEtatCompte etatCompte;

  /**
   * 
   */
  private DtoAdresse adresse;

  /**
   * 
   */
  private DtoSalaire pretentionsSalariales;

  /**
   * 
   */
  private List<DtoNiveau> niveaux;

  /**
   * 
   */
  private DtoExperience experience;

  /**
   * 
   */
  private DtoPersonne ancienEmployeur;

  /**
   * 
   */
  private List<DtoSuiviCooptation> suiviCooptation;
  
  private List<DtoOffre>bookmark;

  public List<DtoOffre> getBookmark() {
	return bookmark;
}

public void setBookmark(List<DtoOffre> bookmark) {
	this.bookmark = bookmark;
}

public Date getDateNaissance() {
    return dateNaissance;
  }

  public void setDateNaissance(Date dateNaissance) {
    this.dateNaissance = dateNaissance;
  }

  public DtoImage getPhoto() {
    return photo;
  }

  public void setPhoto(DtoImage photo) {
    this.photo = photo;
  }

  public String getPresentation() {
    return presentation;
  }

  public void setPresentation(String presentation) {
    this.presentation = presentation;
  }

  public boolean isVeutEtreRecommande() {
    return veutEtreRecommande;
  }

  public void setVeutEtreRecommande(boolean veutEtreRecommande) {
    this.veutEtreRecommande = veutEtreRecommande;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public List<DtoCoopteurProfil> getProfilsAssocies() {
    return profilsAssocies;
  }

  public void setProfilsAssocies(List<DtoCoopteurProfil> profilsAssocies) {
    this.profilsAssocies = profilsAssocies;
  }

  public DtoCompteBancaire getCompteBancaire() {
    return compteBancaire;
  }

  public void setCompteBancaire(DtoCompteBancaire compteBancaire) {
    this.compteBancaire = compteBancaire;
  }

  public DtoEtatCompte getEtatCompte() {
    return etatCompte;
  }

  public void setEtatCompte(DtoEtatCompte etatCompte) {
    this.etatCompte = etatCompte;
  }

  public DtoAdresse getAdresse() {
    return adresse;
  }

  public void setAdresse(DtoAdresse adresse) {
    this.adresse = adresse;
  }

  public DtoSalaire getPretentionsSalariales() {
    return pretentionsSalariales;
  }

  public void setPretentionsSalariales(DtoSalaire pretentionsSalariales) {
    this.pretentionsSalariales = pretentionsSalariales;
  }

  public List<DtoNiveau> getNiveaux() {
    return niveaux;
  }

  public void setNiveaux(List<DtoNiveau> niveaux) {
    this.niveaux = niveaux;
  }

  public DtoExperience getExperience() {
    return experience;
  }

  public void setExperience(DtoExperience experience) {
    this.experience = experience;
  }

  public DtoPersonne getAncienEmployeur() {
    return ancienEmployeur;
  }

  public void setAncienEmployeur(DtoPersonne ancienEmployeur) {
    this.ancienEmployeur = ancienEmployeur;
  }

  public List<DtoSuiviCooptation> getSuiviCooptation() {
    return suiviCooptation;
  }

  public void setSuiviCooptation(List<DtoSuiviCooptation> suiviCooptation) {
    this.suiviCooptation = suiviCooptation;
  }


}
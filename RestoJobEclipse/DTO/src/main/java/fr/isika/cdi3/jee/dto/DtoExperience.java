package fr.isika.cdi3.jee.dto;

import java.io.Serializable;

/**
 * 
 */
public class DtoExperience implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoExperience() {
  }

  /**
   * 
   */
  private String anneeMin;

  /**
   * 
   */
  private String anneeMax;

  /**
   * 
   */
  private Integer id;

  public String getAnneeMin() {
    return anneeMin;
  }

  public void setAnneeMin(String anneeMin) {
    this.anneeMin = anneeMin;
  }

  public String getAnneeMax() {
    return anneeMax;
  }

  public void setAnneeMax(String anneeMax) {
    this.anneeMax = anneeMax;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

}
package fr.isika.cdi3.jee.dto;
import java.io.Serializable;

/**
 * 
 */
public class DtoCompteBancaire implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoCompteBancaire() {
  }

  /**
   * 
   */
  private String titulaire;

  /**
   * 
   */
  private String IBAN;

  /**
   * 
   */
  private String BIC;

  /**
   * 
   */
  private Integer id;

  public String getTitulaire() {
    return titulaire;
  }

  public void setTitulaire(String titulaire) {
    this.titulaire = titulaire;
  }

  public String getIBAN() {
    return IBAN;
  }

  public void setIBAN(String iBAN) {
    IBAN = iBAN;
  }

  public String getBIC() {
    return BIC;
  }

  public void setBIC(String bIC) {
    BIC = bIC;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

}
package fr.isika.cdi3.jee.dto;

import java.io.Serializable;

/**
 * 
 */
public class DtoVoie implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

    /**
     * Default constructor
     */
    public DtoVoie() {
    }

    /**
     * 
     */
    private String type;

    /**
     * 
     */
    private Integer id;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
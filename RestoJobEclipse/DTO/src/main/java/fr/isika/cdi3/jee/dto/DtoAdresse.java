package fr.isika.cdi3.jee.dto;
import java.io.Serializable;

/**
 * 
 */
public class DtoAdresse implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoAdresse() {
  }

  /**
   * 
   */
  private String numero;

  /**
   * 
   */
  private String nomVoie;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private DtoVoie voie;

  /**
   * 
   */
  private DtoCodePostal codePostal;

  /**
   * 
   */
  private DtoVille ville;

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public String getNomVoie() {
    return nomVoie;
  }

  public void setNomVoie(String nomVoie) {
    this.nomVoie = nomVoie;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoVoie getVoie() {
    return voie;
  }

  public void setVoie(DtoVoie voie) {
    this.voie = voie;
  }

  public DtoCodePostal getCodePostal() {
    return codePostal;
  }

  public void setCodePostal(DtoCodePostal codePostal) {
    this.codePostal = codePostal;
  }

  public DtoVille getVille() {
    return ville;
  }

  public void setVille(DtoVille ville) {
    this.ville = ville;
  }

}
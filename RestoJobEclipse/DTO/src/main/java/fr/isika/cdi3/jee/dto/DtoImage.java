package fr.isika.cdi3.jee.dto;

import java.io.Serializable;

/**
 * 
 */
public class DtoImage implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoImage() {
  }

  /**
   * 
   */
  private String url;

  /**
   * 
   */
  private Integer id;
  
  /**
   * 
   */
  private DtoPoste poste;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

public DtoPoste getPoste() {
	return poste;
}

public void setPoste(DtoPoste poste) {
	this.poste = poste;
}

}
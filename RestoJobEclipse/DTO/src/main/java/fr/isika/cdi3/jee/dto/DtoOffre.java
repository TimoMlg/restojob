package fr.isika.cdi3.jee.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 
 */
public class DtoOffre implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoOffre() {
  }

  /**
   * 
   */
  private String description;

  /**
   * 
   */
  private LocalDateTime publication;

  // AFFICHAGE DE LA DATE SANS LES HEURES
  public String toStringDatePubli() {
    return publication.toString().substring(0, 10);
  }

  public String toStringMiniDescri() {
    return description.substring(0, 140) + "[...]";
  }

  /**
   * 
   */
  private Date ouverturePoste;

  /**
   * 
   */
  private boolean estDansNewsletter;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private DtoTypeContrat typeContrat;

  /**
   * 
   */
  private DtoTempsTravail tempsTravail;

  /**
   * 
   */
  private DtoPoste poste;

  /**
   * 
   */
  private DtoEtatOffre etatOffre;

  /**
   * 
   */
  private DtoSalaire salaire;

  /**
   * 
   */
  private DtoExperience experience;

  /**
   * 
   */
  private List<DtoCompetence> competences;

  public DtoOffre(String description, LocalDateTime publication, Date ouverturePoste, boolean estDansNewsletter,
      Integer id, DtoTypeContrat typeContrat, DtoTempsTravail tempsTravail, DtoPoste poste, DtoEtatOffre etatOffre,
      DtoSalaire salaire, DtoExperience experience, List<DtoCompetence> competences,
      List<DtoSuiviCooptation> personnesRecommandees, DtoEntreprise entreprise) {
    super();
    this.description = description;
    this.publication = publication;
    this.ouverturePoste = ouverturePoste;
    this.estDansNewsletter = estDansNewsletter;
    this.id = id;
    this.typeContrat = typeContrat;
    this.tempsTravail = tempsTravail;
    this.poste = poste;
    this.etatOffre = etatOffre;
    this.salaire = salaire;
    this.experience = experience;
    this.competences = competences;
    this.personnesRecommandees = personnesRecommandees;
    this.entreprise = entreprise;
  }

  /**
   *
   */
  private List<DtoSuiviCooptation> personnesRecommandees;

  /**
   * 
   */
  private DtoEntreprise entreprise;

  /**
   * 
   */
  private DtoImage image;

  public DtoImage getImage() {
    return image;
  }

  public void setImage(DtoImage image) {
    this.image = image;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public DtoEntreprise getEntreprise() {
    return entreprise;
  }

  public void setEntreprise(DtoEntreprise entreprise) {
    this.entreprise = entreprise;
  }

  public String getDescription() {
    return description;
  }

  public void setDescritption(String description) {
    this.description = description;
  }

  public LocalDateTime getPublication() {
    return publication;
  }

  public void setPublication(LocalDateTime publication) {
    this.publication = publication;
  }

  public Date getOuverturePoste() {
    return ouverturePoste;
  }

  public void setOuverturePoste(Date ouverturePoste) {
    this.ouverturePoste = ouverturePoste;
  }

  public boolean isEstDansNewsletter() {
    return estDansNewsletter;
  }

  public void setEstDansNewsletter(boolean estDansNewsletter) {
    this.estDansNewsletter = estDansNewsletter;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoTypeContrat getTypeContrat() {
    return typeContrat;
  }

  public void setTypeContrat(DtoTypeContrat typeContrat) {
    this.typeContrat = typeContrat;
  }

  public DtoTempsTravail getTempsTravail() {
    return tempsTravail;
  }

  public void setTempsTravail(DtoTempsTravail tempsTravail) {
    this.tempsTravail = tempsTravail;
  }

  public DtoPoste getPoste() {
    return poste;
  }

  public void setPoste(DtoPoste poste) {
    this.poste = poste;
  }

  public DtoEtatOffre getEtatOffre() {
    return etatOffre;
  }

  public void setEtatOffre(DtoEtatOffre etatOffre) {
    this.etatOffre = etatOffre;
  }

  public DtoSalaire getSalaire() {
    return salaire;
  }

  public void setSalaire(DtoSalaire salaire) {
    this.salaire = salaire;
  }

  public DtoExperience getExperience() {
    return experience;
  }

  public void setExperience(DtoExperience experience) {
    this.experience = experience;
  }

  public List<DtoCompetence> getCompetences() {
    return competences;
  }

  public void setCompetences(List<DtoCompetence> competences) {
    this.competences = competences;
  }

  public List<DtoSuiviCooptation> getPersonnesRecommandees() {
    return personnesRecommandees;
  }

  public void setPersonnesRecommandees(List<DtoSuiviCooptation> personnesRecommandees) {
    this.personnesRecommandees = personnesRecommandees;
  }

  public DtoImage chooseImg() {
    Random rand = new Random();
    return poste.getImages().get(rand.nextInt(poste.getImages().size()));
  }
}
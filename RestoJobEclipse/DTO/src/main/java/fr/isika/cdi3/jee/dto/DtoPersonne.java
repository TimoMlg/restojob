package fr.isika.cdi3.jee.dto;
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class DtoPersonne implements Serializable {

  public DtoPersonne(String nom, String prenom, String fonction, String numeroTelephone, String mail,
			String profilLinkedIn, String motPasse, Integer id, DtoCivilite civilite, List<DtoMessage> messages) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.fonction = fonction;
		this.numeroTelephone = numeroTelephone;
		this.mail = mail;
		this.profilLinkedIn = profilLinkedIn;
		this.motPasse = motPasse;
		this.id = id;
		this.civilite = civilite;
		this.messages = messages;
	}

/**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoPersonne() {
  }

  /**
   * 
   */
  private String nom;

  /**
   * 
   */
  private String prenom;

  /**
   * 
   */
  private String fonction;

  /**
   * 
   */
  private String numeroTelephone;

  /**
   * 
   */
  private String mail;

  /**
   * 
   */
  private String profilLinkedIn;

  /**
   * 
   */
  private String motPasse;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private DtoCivilite civilite;

  /**
   * 
   */
  private List<DtoMessage> messages;

  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getFonction() {
    return fonction;
  }

  public void setFonction(String fonction) {
    this.fonction = fonction;
  }

  public String getNumeroTelephone() {
    return numeroTelephone;
  }

  public void setNumeroTelephone(String numeroTelephone) {
    this.numeroTelephone = numeroTelephone;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public String getProfilLinkedIn() {
    return profilLinkedIn;
  }

  public void setProfilLinkedIn(String profilLinkedIn) {
    this.profilLinkedIn = profilLinkedIn;
  }


  public String getMotPasse() {
    return motPasse;
  }

  public void setMotPasse(String motPasse) {
    this.motPasse = motPasse;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoCivilite getCivilite() {
    return civilite;
  }

  public void setCivilite(DtoCivilite civilite) {
    this.civilite = civilite;
  }

  public List<DtoMessage> getMessages() {
    return messages;
  }

  public void setMessages(List<DtoMessage> messages) {
    this.messages = messages;
  }

}
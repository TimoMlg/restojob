package fr.isika.cdi3.jee.dto;

import java.io.Serializable;

/**
 * 
 */
public class DtoVille implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoVille() {
  }


  /**
   * 
   */
  private String libelle;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private DtoPays pays;

  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoPays getPays() {
    return pays;
  }

  public void setPays(DtoPays pays) {
    this.pays = pays;
  }

}
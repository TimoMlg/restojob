package fr.isika.cdi3.jee.dto;
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class DtoFacture implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoFacture() {
  }

  /**
   * 
   */
  private String numero;

  /**
   * 
   */
  private Date creation;

  /**
   * 
   */
  private String libelle;

  /**
   * 
   */
  private Date delaiPaiement;

  /**
   * 
   */
  private Integer id;
  
  /**
   * 
   */
  private DtoEntreprise entreprise;

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public Date getCreation() {
    return creation;
  }

  public void setCreation(Date creation) {
    this.creation = creation;
  }

  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public Date getDelaiPaiement() {
    return delaiPaiement;
  }

  public void setDelaiPaiement(Date delaiPaiement) {
    this.delaiPaiement = delaiPaiement;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoEntreprise getEntreprise() {
    return entreprise;
  }

  public void setEntreprise(DtoEntreprise entreprise) {
    this.entreprise = entreprise;
  }

}
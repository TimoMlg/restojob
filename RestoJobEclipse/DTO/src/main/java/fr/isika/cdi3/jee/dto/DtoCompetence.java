package fr.isika.cdi3.jee.dto;
import java.io.Serializable;

/**
 * 
 */
public class DtoCompetence implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoCompetence() {
  }

  /**
   * 
   */
  private String libelle;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private DtoCompetence competenceParent;

  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoCompetence getCompetenceParent() {
    return competenceParent;
  }

  public void setCompetenceParent(DtoCompetence competenceParent) {
    this.competenceParent = competenceParent;
  }

}
package fr.isika.cdi3.jee.dto;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * 
 */
public class DtoEntreprise extends DtoPersonne implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoEntreprise() {
  }

  /**
   * 
   */
  private String nomEntreprise;

  /**
   * 
   */
  private String numeroSiret;

  /**
   * 
   */
  private DtoImage logo;

  /**
   * 
   */
  private String description;

  /**
   * 
   */
  private Integer nombreSalaries;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private List<DtoFacture> factures;

  /**
   * 
   */
  private List<DtoAbonnement> abonnements;

  /**
   * 
   */
  private DtoCompteBancaire compteBancaire;

  /**
   * 
   */
  private DtoEtatCompte etatCompte;

  /**
   * 
   */
  private DtoPersonne referent;

  /**
   * 
   */
  private DtoAdresse adresseSiege;

  /**
   * 
   */
  private List<DtoAdresse> adresseSite;

  /**
   * 
   */
  private List<DtoTag> tags;

  /**
   * 
   */
  private List<DtoTypeEtablissement> typeEtablissements;
  
  /**
   * 
   */
  private List<DtoOffre> offres;

public List<DtoOffre> getOffres() {
    return offres;
  }

  public void setOffres(List<DtoOffre> offres) {
    this.offres = offres;
  }

  public String getNomEntreprise() {
    return nomEntreprise;
  }

  public void setNomEntreprise(String nom) {
    this.nomEntreprise = nom;
  }

  public String getNumeroSiret() {
    return numeroSiret;
  }

  public void setNumeroSiret(String numeroSiret) {
    this.numeroSiret = numeroSiret;
  }

  public DtoImage getLogo() {
    return logo;
  }

  public void setLogo(DtoImage logo) {
    this.logo = logo;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getNombreSalaries() {
    return nombreSalaries;
  }

  public void setNombreSalaries(Integer nombreSalaries) {
    this.nombreSalaries = nombreSalaries;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public List<DtoFacture> getFactures() {
    return factures;
  }

  public void setFactures(List<DtoFacture> factures) {
    this.factures = factures;
  }

  public List<DtoAbonnement> getAbonnements() {
    return abonnements;
  }

  public void setAbonnements(List<DtoAbonnement> abonnements) {
    this.abonnements = abonnements;
  }

  public DtoCompteBancaire getCompteBancaire() {
    return compteBancaire;
  }

  public void setCompteBancaire(DtoCompteBancaire compteBancaire) {
    this.compteBancaire = compteBancaire;
  }

  public DtoEtatCompte getEtatCompte() {
    return etatCompte;
  }

  public void setEtatCompte(DtoEtatCompte etatCompte) {
    this.etatCompte = etatCompte;
  }

  public DtoPersonne getReferent() {
    return referent;
  }

  public void setReferent(DtoPersonne referent) {
    this.referent = referent;
  }

  public DtoAdresse getAdresseSiege() {
    return adresseSiege;
  }

  public void setAdresseSiege(DtoAdresse adresseSiege) {
    this.adresseSiege = adresseSiege;
  }

  public List<DtoAdresse> getAdresseSite() {
    return adresseSite;
  }

  public void setAdresseSite(List<DtoAdresse> adresseSite) {
    this.adresseSite = adresseSite;
  }

  public List<DtoTag> getTags() {
    return tags;
  }

  public void setTags(List<DtoTag> tags) {
    this.tags = tags;
  }

  public List<DtoTypeEtablissement> getTypeEtablissements() {
    return typeEtablissements;
  }

  public void setTypeEtablissements(List<DtoTypeEtablissement> typeEtablissements) {
    this.typeEtablissements = typeEtablissements;
  }

}
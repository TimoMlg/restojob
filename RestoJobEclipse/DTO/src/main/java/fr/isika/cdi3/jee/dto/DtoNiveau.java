package fr.isika.cdi3.jee.dto;

import java.io.Serializable;

/**
 * 
 */
public class DtoNiveau implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoNiveau() {
  }

  /**
   * 
   */
  private Integer etoiles;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private DtoCoopteurProfil coopteurProfil;
  
  /**
   * 
   */
  private DtoCompetence competence;

  public Integer getEtoiles() {
    return etoiles;
  }

  public void setEtoiles(Integer etoiles) {
    this.etoiles = etoiles;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoCoopteurProfil getCoopteurProfil() {
    return coopteurProfil;
  }

  public void setCoopteurProfil(DtoCoopteurProfil coopteurProfil) {
    this.coopteurProfil = coopteurProfil;
  }

  public DtoCompetence getCompetence() {
    return competence;
  }

  public void setCompetence(DtoCompetence competence) {
    this.competence = competence;
  }

}
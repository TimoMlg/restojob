package fr.isika.cdi3.jee.dto;

import java.io.Serializable;

/**
 * 
 */
public class DtoMessage implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoMessage() {
  }

  /**
   * 
   */
  private String objet;

  /**
   * 
   */
  private String contenu;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private DtoEtatMessage etatMessage;
  
  /**
   * 
   */
  private DtoPersonne emetteur;
  
  /**
   * 
   */
  private DtoPersonne destinataire;

  public String getObjet() {
    return objet;
  }

  public void setObjet(String objet) {
    this.objet = objet;
  }

  public String getContenu() {
    return contenu;
  }

  public void setContenu(String contenu) {
    this.contenu = contenu;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoEtatMessage getEtatMessage() {
    return etatMessage;
  }

  public void setEtatMessage(DtoEtatMessage etatMessage) {
    this.etatMessage = etatMessage;
  }

public DtoPersonne getEmetteur() {
	return emetteur;
}

public void setEmetteur(DtoPersonne emetteur) {
	this.emetteur = emetteur;
}

public DtoPersonne getDestinataire() {
	return destinataire;
}

public void setDestinataire(DtoPersonne destinataire) {
	this.destinataire = destinataire;
}

}
package fr.isika.cdi3.jee.dto;
import java.io.Serializable;

/**
 * 
 */
public class DtoTypeEtablissement implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoTypeEtablissement() {
  }

  /**
   * 
   */
  private String libelle;

  /**
   * 
   */
  private Integer id;

  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


}
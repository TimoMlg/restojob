package fr.isika.cdi3.jee.dto;
import java.io.Serializable;

/**
 * 
 */
public class DtoTag implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoTag() {
  }

  /**
   * 
   */
  private String libelle;

  /**
   * 
   */
  private Integer id;

  /**
   * 
   */
  private DtoTag tagParent;

  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoTag getTagParent() {
    return tagParent;
  }

  public void setTagParent(DtoTag tagParent) {
    this.tagParent = tagParent;
  }

}
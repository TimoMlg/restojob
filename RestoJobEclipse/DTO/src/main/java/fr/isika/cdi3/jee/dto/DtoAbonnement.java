package fr.isika.cdi3.jee.dto;
import java.io.Serializable;
import java.util.*;

/**
 * 
 */
public class DtoAbonnement implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Default constructor
   */
  public DtoAbonnement() {
  }

  /**
   * 
   */
  private String type;

  /**
   * 
   */
  private String numeroContrat;

  /**
   * 
   */
  private double montant;

  /**
   * 
   */
  private Date dateSouscription;

  /**
   * 
   */
  private Integer id;
  
  /**
   * 
   */
  private DtoEntreprise entreprise;

  /**
   * 
   */
  private DtoEtatAbonnement etatAbonnement;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getNumeroContrat() {
    return numeroContrat;
  }

  public void setNumeroContrat(String numeroContrat) {
    this.numeroContrat = numeroContrat;
  }

  public double getMontant() {
    return montant;
  }

  public void setMontant(double montant) {
    this.montant = montant;
  }

  public Date getDateSouscription() {
    return dateSouscription;
  }

  public void setDateSouscription(Date dateSouscription) {
    this.dateSouscription = dateSouscription;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DtoEtatAbonnement getEtatAbonnement() {
    return etatAbonnement;
  }

  public void setEtatAbonnement(DtoEtatAbonnement etatAbonnement) {
    this.etatAbonnement = etatAbonnement;
  }

  public DtoEntreprise getEntreprise() {
    return entreprise;
  }

  public void setEntreprise(DtoEntreprise entreprise) {
    this.entreprise = entreprise;
  }


}
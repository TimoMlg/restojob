package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoTypeEtablissement;

public interface ITypeEtablissementBusiness {
  List<DtoTypeEtablissement> getAll();
}

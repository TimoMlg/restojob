package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoCompetence;

public interface ICompetenceBusiness {
  List<DtoCompetence> getAll();

  DtoCompetence getById(int idCompetence);
}

package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

public interface ISuiviRecommandationBusiness {

	 List<DtoSuiviCooptation> getRecommandation(int idCoopteur);
}

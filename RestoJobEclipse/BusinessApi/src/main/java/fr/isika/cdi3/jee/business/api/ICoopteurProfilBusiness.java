package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoOffre;
import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

public interface ICoopteurProfilBusiness {

	public void creerCompteParticulier(DtoCoopteurProfil particulier);

	public DtoCoopteurProfil getById(int idDtoPersonne);

	public long recommandationsReussies(int idParticulier);

	public long personnesRecommandees(int idParticulier);

	public long cooptationsEnCours(int idParticulier);

	public List<DtoCoopteurProfil> profilsAssocies(int idParticulier);

	public List<DtoOffre> listeOffresFav(int idParticulier);

	public List<DtoOffre> listeRecommandationsReussies(int idParticulier);
}

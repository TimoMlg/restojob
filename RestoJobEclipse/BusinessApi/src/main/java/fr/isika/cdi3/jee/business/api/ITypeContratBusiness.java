package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoTypeContrat;

public interface ITypeContratBusiness {
  List<DtoTypeContrat> getAll();
  DtoTypeContrat getById(int idTC);
}

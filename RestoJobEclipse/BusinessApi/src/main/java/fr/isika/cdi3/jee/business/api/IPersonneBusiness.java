package fr.isika.cdi3.jee.business.api;

import fr.isika.cdi3.jee.dto.DtoPersonne;

public interface IPersonneBusiness {

	DtoPersonne SeConnecter(String mail, String motPasse) throws Exception;

}

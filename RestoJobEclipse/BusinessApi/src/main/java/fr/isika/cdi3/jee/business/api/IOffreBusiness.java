package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoCoopteurProfil;
import fr.isika.cdi3.jee.dto.DtoOffre;

public interface IOffreBusiness {

	List<DtoOffre> GetAll(String recherche, String gastronomie, String bistronomie, String patisserie, String traiteur,
		    String boulangerie, String cuisineRegionale, String grandService, String petitService, String fruitsMer,
		    String cuisineMesure, String experienceCulinaire, String troisHuit, String cuisineThematique, String cuisineModerne,
		    String CDI, String CDD, String vacation, String cuisine, String salle);

	DtoOffre GetById(int IdOffre);
	void createOffer(DtoOffre dtoOffre);
	
	public Long nbOffresPostees(int idEntreprise);
	public void bookmark(DtoOffre dtoOffreBookmark, DtoCoopteurProfil dtoCoopteurProfil);

	List<DtoOffre> GetByEntreprise(int idEntreprise);
	
	
}

package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoTempsTravail;

public interface ITempsTravailBusiness {
	List<DtoTempsTravail> getAll();
	DtoTempsTravail getById(int idTT);
}

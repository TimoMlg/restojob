package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoPoste;

public interface IPosteBusiness {
	List<DtoPoste> getAll();
	DtoPoste getById(int idPoste);
}

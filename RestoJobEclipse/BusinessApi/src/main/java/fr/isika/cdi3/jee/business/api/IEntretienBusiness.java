package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoEntretien;

public interface IEntretienBusiness {

	List<DtoEntretien> datesEntretien(int idSuiviCooptation);

}

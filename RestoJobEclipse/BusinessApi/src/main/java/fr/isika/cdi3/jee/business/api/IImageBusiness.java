package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoImage;

public interface IImageBusiness {
  List<DtoImage> getByPoste(int idPoste);
  
  DtoImage getById(int id);
  
}

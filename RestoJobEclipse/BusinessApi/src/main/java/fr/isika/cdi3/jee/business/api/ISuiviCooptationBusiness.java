package fr.isika.cdi3.jee.business.api;

import java.time.LocalDateTime;
import java.util.List;

import fr.isika.cdi3.jee.dto.DtoSuiviCooptation;

public interface ISuiviCooptationBusiness {

  String addSuiviCooptation(LocalDateTime dateCreation, int idCoopteur, int idEtatCooptation, int idOffre, String mail);

  void updateEtatSuiviCooptationValid(DtoSuiviCooptation sc);

  void updateEtatSuiviCooptationRefus(DtoSuiviCooptation sc);

  DtoSuiviCooptation updateEtatSuiviCooptationSelection(DtoSuiviCooptation dtosc);

  DtoSuiviCooptation updateEtatSuiviCooptationEntretien(DtoSuiviCooptation dtosc);

  DtoSuiviCooptation updateEtatSuiviCooptationRecrutement(DtoSuiviCooptation dtosc);

  DtoSuiviCooptation updateEtatSuiviCooptationPeriodeEssai(DtoSuiviCooptation dtosc);

  Long nbProfilsRecrutes(int idEntreprise);

  Long nbValidationRecrutementCoopteur(int idCoopteur);

  List<DtoSuiviCooptation> getByOffre(int idOffre);

  List<DtoSuiviCooptation> entretiensByCoopteur(int idParticulier);

  List<DtoSuiviCooptation> recrutementsByCoopteurs(int idParticulier);

  List<DtoSuiviCooptation> selectionsByCoopteurs(int idParticulier);

  public long getSuiviSelonEtat(int idPersonne, int idEtat);

}

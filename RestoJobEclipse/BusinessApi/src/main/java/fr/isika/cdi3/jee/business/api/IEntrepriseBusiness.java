package fr.isika.cdi3.jee.business.api;

import fr.isika.cdi3.jee.dto.DtoEntreprise;
import fr.isika.cdi3.jee.dto.DtoPersonne;

public interface IEntrepriseBusiness {

	public void creerUnCompteEntreprise(DtoEntreprise LULULAND, DtoPersonne LULU);

	public DtoEntreprise getById(int idEntreprise);
	}

package fr.isika.cdi3.jee.business.api;

import java.util.List;

import fr.isika.cdi3.jee.dto.DtoCivilite;

public interface ICiviliteBusiness {
  List<DtoCivilite> getAll();
}
